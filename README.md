# Setting up the Project

```
    npm install
    npm run start
```

# Container vs Component
 Feature                | Containers                               | Components _(presentational)_            |
------------------------|------------------------------------------|------------------------------------------|
 Purpose                | _how it works_                           | _how it looks_                           |
 Redux                  | knows                                    | doesn't know                             |
 Data (API)             | do fetch, provide to others              | receives via props only                  |
 Actions                | provide to others                        | receives via props only                  |
 State                  | stateful                                 | UI only                                  |
 DOM Markup             | no own markup (usually)                  | _no restrictions_                        |

# Container/Component split rules
- When you notice that some components don’t use the props they receive but merely forward them down and you have to rewire all those intermediate components any time the children need more data, it’s a good time to introduce some container components.

# Q&A
- Component isn't rerendering on props (via store.state) changes
    - Check if store.state isn't mutated in reducer (the reason to use ImmutableJS)
- IDE doesn't complete attributes for custom components
    - Give them a name (`const Something = React.createClass..; export default Something;`)

# Docs
- https://github.com/reactjs/react-router-tutorial
- http://courses.reactjsprogram.com/courses/reactjsfundamentals
- https://maxfarseer.gitbooks.io/redux-course-ru/content/sozdanie_actions.html

# Blogs
- https://medium.com/@dan_abramov (Redux author)
- http://tylermcginnis.com/ (ReactJS Fundamentals course author)

# Bookmarks
- https://github.com/erikras/redux-form