var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var rootApi;
if (process.argv.indexOf("--root_api") != -1) {
  rootApi = process.argv[process.argv.indexOf("--root_api") + 1];
} else {
  rootApi = "http://localhost:8000";
  process.env.DEBUG = true;
}

rootApi = rootApi + "/api";

var devFlagPlugin = new webpack.DefinePlugin({
  API_ROOT: JSON.stringify(rootApi),
  __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
});

var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
});

// Hack for Ubuntu on Windows: interface enumeration fails with EINVAL, so return empty.
// https://github.com/Microsoft/BashOnWindows/issues/468#issuecomment-247684647
try {
  require('os').networkInterfaces()
} catch (e) {
  require('os').networkInterfaces = () => ({})
}

module.exports = {
  entry: [
    './app/index.js'
  ],

  devServer: {
    historyApiFallback: true,
  },

  output: {
    path: __dirname + '/dist',
    filename: "/index_bundle.js"
  },

  module: {
    loaders: [
      {test: /\.css$/, loader: 'style-loader!css-loader'},
      {test: /\.scss$/, loaders: ["style", "css", "sass"]},
      {test: /\.less$/, loaders: ["style", "css", "less"]},
      {test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"},
      {test: /\.json$/, exclude: /node_modules/, loader: "json-loader"}
    ]
  },

  watchOptions: {
    aggregateTimeout: 300, // Delay the rebuilt after the first change. Value is a time in ms.
    //poll: true // bool - enable / disable polling or number - polling delay
    poll: 1000
  },

  plugins: [
    new webpack.NoErrorsPlugin(),
    devFlagPlugin,
    HTMLWebpackPluginConfig
  ]
};