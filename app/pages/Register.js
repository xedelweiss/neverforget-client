import React from 'react';
import {Panel, Row, Col} from 'react-bootstrap';
import RegisterForm from '../containers/RegisterForm';

export default React.createClass({
    render() {
        return (
            <div className="container">
                <Row>
                    <Col md={8} mdOffset={2}>
                        <Panel header="Register">
                            <RegisterForm />
                        </Panel>
                    </Col>
                </Row>
            </div>
        )
    }
});