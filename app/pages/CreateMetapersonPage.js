import React from 'react';
import MetapersonContainer from '../containers/Metaperson/MetapersonContainer';

export default React.createClass({
  render() {
    return (<div className="container">
      <MetapersonContainer />
    </div>)
  }
});