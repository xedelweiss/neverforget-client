import React from 'react';
import MetapersonContainer from '../containers/Metaperson/MetapersonContainer';

export default React.createClass({
  render() {
    let metapersonId = parseInt(this.props.params.metapersonId);

    return (<div className="container">
      <MetapersonContainer metapersonId={metapersonId} />
    </div>)
  }
});