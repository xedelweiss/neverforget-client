import React from 'react';
import {Row, Col} from 'react-bootstrap';
import {FormattedMessage} from 'react-intl';

export default React.createClass({
  render() {
    let year = parseInt(this.props.params.year) || (new Date()).getFullYear();

    return (
      <div className="container spark-screen">
        <Row>
          <Col md={4}><h2>Year { year }</h2></Col>
        </Row>
        <Row>
          <Col md={12}>
            <FormattedMessage
              id="yourad"
              defaultMessage="Your advertisment could be here" />
          </Col>
        </Row>
      </div>

    )
  }
});