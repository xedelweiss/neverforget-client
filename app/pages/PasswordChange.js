import React from "react";
import {connect} from "react-redux";
import update from "react-addons-update";
import {
  Row,
  Col,
  Panel,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Button
} from "react-bootstrap";
import {
  FormattedMessage,
  injectIntl
} from "react-intl";
import {log} from "../utils/debug";
import {
  newPasswordRequest,
  changePasswordPage
} from "../actions/auth";
import messages from "../i18n/definedCommonMessages";
import queryString from "query-string";

const PasswordChange = React.createClass({

  getInitialState() {
    return {
      password:         '',
      passwordConfirm:  '',
      loading:          false
    };
  },

  componentWillMount() {
    this.props.changePasswordPage();
  },

  componentWillReceiveProps(nextProps) {

  },

  updateField(field, e) {
    this.setState(update(this.state, {
      [field]: {
        $set: e.target.value
      }
    }));
  },

  setNewPassword() {
    if ( this.state.password === this.state.passwordConfirm ) {
      this.setState(update(this.state, {
        loading: {$set: true}
      }));

      let location    = window.location;
      let parameters  = location.search;
      let parsedStr   = queryString.parse(parameters);

      this.props.newPasswordRequest(parsedStr.token, this.state.password);
    }
  },

  renderChangePassword() {
    const intl  = this.props.intl;

    return (
      <div>
        <Panel header={intl.formatMessage(messages.set_new_password)}>
          <Form>
            <FormGroup>
              <ControlLabel>
                <FormattedMessage id="Enter_New_Password" defaultMessage="Enter New Password" />
              </ControlLabel>
              <FormControl
                type="password"
                value={this.state.password}
                onChange={this.updateField.bind(this, 'password')}
              />
            </FormGroup>

            <FormGroup>
              <ControlLabel>
                <FormattedMessage id="repeat_password" defaultMessage="Repeat New Password" />
              </ControlLabel>
              <FormControl
                type="password"
                value={this.state.passwordConfirm}
                onChange={this.updateField.bind(this, 'passwordConfirm')}
              />
            </FormGroup>

            <FormGroup>
              <Button
                disabled={this.state.loading}
                bsStyle="primary"
                onClick={this.setNewPassword}>
                  {intl.formatMessage(messages.set_new_password)}
              </Button>
            </FormGroup>
          </Form>
        </Panel>
      </div>
    );
  },

  renderPasswordChanged() {
    const intl  = this.props.intl;

    return (
      <div>
        <Panel header={intl.formatMessage(messages.congratulations)}>
          <div>
            <FormattedMessage id="password_changed" defaultMessage="The password has been changed" />
          </div>
          <div>
            <FormattedMessage
              id="login_to_account_with_new_creds"
              defaultMessage="Please login into your account with new credentials" />
          </div>
        </Panel>
      </div>
    );
  },

  render() {
    return (
      <div className="container">
        <Row>
          <Col md={6} mdOffset={3}>
            { !this.props.isPasswordChanged ? this.renderChangePassword() : this.renderPasswordChanged() }
          </Col>
        </Row>
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  let authState = state.auth.toJS();

  return {
    isPasswordChanged:  authState.resetPassword.passwordChanged
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    newPasswordRequest: (token, newPassword) => {
      dispatch(newPasswordRequest(token, newPassword));
    },
    changePasswordPage: () => {
      dispatch(changePasswordPage());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
) (injectIntl(PasswordChange));

