import React from "react";
import {log} from "../utils/debug";
import {Row, Col} from "react-bootstrap";
import {connect} from 'react-redux';
import ContactInfo from "../containers/Settings/ContactInformation";
import PasswordInfo from "../containers/Settings/PasswordInformation";
import SocialsInfo from "../containers/Settings/SocialsInformation";

const SettingsPage = React.createClass({
  render() {
    return (
      <div className="container">
        <Row>
          <Col md={12}>
            <ContactInfo token={this.props.authToken} />
            <hr />
            <PasswordInfo />
            <hr />
            <SocialsInfo />
          </Col>
        </Row>

      </div>
    );
  }
});

const mapStateToProps = (state) => {
  let auth      = state.auth.toJS();

  return {
    authToken:  auth.token
  }
};

export default connect(mapStateToProps) (SettingsPage);
