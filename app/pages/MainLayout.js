import React from 'react';
import MainMenu from '../components/navigation/MainMenu';
import SocialWatcher from '../containers/Social/Watcher';

export default React.createClass({
  render() {
    return (
      <div>
        <MainMenu />
        <SocialWatcher socials={this.props.location.query.socials} jwt={this.props.location.query.jwt} />
        {this.props.children}
      </div>
    )
  }
});