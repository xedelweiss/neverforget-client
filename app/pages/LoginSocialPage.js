import React from 'react';
import {Panel, Row, Col} from 'react-bootstrap';
import LoginSocialForm from '../containers/LoginSocialForm';

const LoginSocialPage = React.createClass({
  render() {
    let provider = this.props.params.provider;
    let code = this.props.location.query.code;

    return (
      <div className="container">
        <Row>
          <Col xs={12} sm={8} smOffset={2} lg={6} lgOffset={3}>
            <Panel>
              <LoginSocialForm provider={provider} code={code}/>
            </Panel>
          </Col>
        </Row>
      </div>
    )
  }
});

export default LoginSocialPage;