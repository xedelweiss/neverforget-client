import React from "react";
import MetapersonsContainer from '../containers/Metaperson/MetapersonsContainer';
import MetapersonsTextFilter from '../containers/Metaperson/MetapersonsTextFilter';
import MetapersonsGroupFilter from '../containers/Metaperson/MetapersonsGroupFilter';
import MetapersonsDestinationFilter from '../containers/Metaperson/MetapersonsDestinationFilter';
import MetapersonsToolbar from '../containers/Metaperson/MetapersonsToolbar';
import {
  Row,
  Col,
  Button,
  DropdownButton,
  MenuItem,
  ButtonGroup,
  ButtonToolbar,
  Glyphicon
} from "react-bootstrap";

export default React.createClass({
  render() {
    return (<div className="container">
      <Row>
        <Col md={12}>
          <p>You have: 30900 contacts in 18 groups</p>
        </Col>
      </Row>
      <Row>
        <Col md={5}>
          <MetapersonsTextFilter />
        </Col>
        <Col md={7}>
          <MetapersonsToolbar />
        </Col>
      </Row>
      <Row>
        <Col md={3}>
          <MetapersonsGroupFilter />
          <MetapersonsDestinationFilter />
        </Col>
        <Col md={9}>
          <MetapersonsContainer />
        </Col>
      </Row>
    </div>);
  }
});