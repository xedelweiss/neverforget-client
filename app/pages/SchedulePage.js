import * as axios from 'axios';
import {log} from '../utils/debug';
import update from "react-addons-update";
import {connect} from 'react-redux';
import React from "react";
import {Row, Col} from "react-bootstrap";
import EventContainer from "../containers/Event/EventContainer";
import NewEventContainer from "../containers/Event/NewEventContainer";
import moment from "moment";
import {fetchUserContacts} from '../actions/data/default';

const SchedulePage =  React.createClass({
  getInitialState() {
    this.props.fetchUserContacts();

    return {
      timezones: []
    }
  },

  componentWillMount() {
    axios
      .get(`${API_ROOT}/timezones`, {})
      .then((response) => {
        this.setState(update(this.state, {
          timezones: {$set: response.data.result.data}
        }));
      })
      .catch((response) => {
        alert("Error in Schedule page");
      });
  },

  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps);
  // },

  render() {
    let period      = this.props.location.query.period || 'year';
    let activeDate  = this.props.location.query.activeDate || moment();

    return (
      <div className="container schedule-wrapper">
        <Row className="new-event-row">
          <Col md={6}>

          </Col>
          <Col md={1} mdOffset={5}>
            <NewEventContainer
              period={period}
              userContacts={this.props.userContacts}
              timezones={this.state.timezones}
            />
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <EventContainer
              period={period}
              userContacts={this.props.userContacts}
              timezones={this.state.timezones}
              activeDate={activeDate}
              location={this.props.location}
            />
          </Col>

        </Row>
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  const userContacts = state.data.toJS().userContacts;
  return {
    userContacts
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserContacts: () => {
      dispatch(fetchUserContacts());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SchedulePage);