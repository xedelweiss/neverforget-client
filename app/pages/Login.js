import React from 'react';
import {Panel, Row, Col} from 'react-bootstrap';
import LoginForm from '../containers/LoginForm';

export default React.createClass({
  render() {
    return (
      <div className="container">
        <Row>
          <Col xs={12} sm={8} smOffset={2} lg={6} lgOffset={3}>
            <Panel header="Login">
              <LoginForm />
            </Panel>
          </Col>
        </Row>
      </div>
    )
  }
});