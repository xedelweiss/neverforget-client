import React from "react";
import {connect} from "react-redux";
import update from "react-addons-update";
import {
  Row,
  Col,
  Panel,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Button
} from "react-bootstrap";
import {
  FormattedMessage,
  injectIntl
} from "react-intl";

import {log} from "../utils/debug";
import ValidationErrors from '../components/ValidationErrors';

import {
  resetPasswordRequest,
  resetPasswordPage
} from "../actions/auth";

import messages from "../i18n/definedCommonMessages";

const PasswordReset = React.createClass({
  getInitialState() {
    return {
      restoreEmail: '',
      loading:      false
    }
  },

  componentWillMount() {
    this.props.resetPasswordPage();
  },

  componentWillReceiveProps(nextProps) {
    if ( 'loading' in nextProps ) {
      this.setState(update(this.state, {
        loading: {
          $set: nextProps.loading
        }
      }));
    }
  },

  updateEmailVal(e) {
    this.setState(update(this.state, {
      restoreEmail: {
        $set: e.target.value
      }
    }));
  },

  resetPasswordStart() {
    this.setState(update(this.state, {
      loading: {$set: true}
    }));
    
    this.props.resetPasswordRequest(this.state.restoreEmail);
  },

  renderResetForm() {
    const intl  = this.props.intl;
    const errors  = this.props.errors.errors ? this.props.errors.errors.resetEmail || [] : [];

    return (
      <div className="container">
        <Row>
          <Col md={6} mdOffset={3}>
            <Panel header={intl.formatMessage(messages.reset_your_password)}>
              <Form>
                <FormGroup validationState={errors.length > 0 ? 'error' : null}>
                  <ControlLabel>
                    <FormattedMessage id="Enter_Your_Email" defaultMessage="Enter Your Email" />
                  </ControlLabel>
                  <FormControl
                    type="email"
                    value={this.state.restoreEmail}
                    onChange={this.updateEmailVal}
                  />
                  <ValidationErrors errors={errors} />
                </FormGroup>

                <FormGroup>
                  <Button
                    disabled={this.state.loading}
                    bsStyle="primary"
                    onClick={this.resetPasswordStart}>
                      {intl.formatMessage(messages.reset_password)}
                  </Button>
                </FormGroup>
              </Form>
            </Panel>
          </Col>
        </Row>
      </div>
    );
  },

  renderInitiatedPage() {
    const intl  = this.props.intl;

    return (
      <div className="container">
        <Row>
          <Col md={6} mdOffset={3}>
            <Panel header={intl.formatMessage(messages.congratulations)}>
              <FormattedMessage
                id="reset_password_email_sent"
                defaultMessage="You should receive an email with password reset instructions." />
            </Panel>
          </Col>
        </Row>
      </div>
    );
  },

  render() {
    if ( this.props.isRestorePasswordInitiated === false ) {
      return this.renderResetForm();
    }

    return this.renderInitiatedPage();
  }
});

const mapStateToProps = (state) => {
  let resetPassword = state.auth.toJS().resetPassword;

  return {
    isRestorePasswordInitiated: resetPassword.isRestorePasswordInitiated,
    errors:                     resetPassword.errors,
    loading:                    resetPassword.loading
  };
};

const mapDispatchToPros = (dispatch) => {
  return {
    resetPasswordRequest: (resetEmail) => {
      dispatch(resetPasswordRequest(resetEmail));
    },

    resetPasswordPage: () => {
      dispatch(resetPasswordPage());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToPros)(injectIntl(PasswordReset));
