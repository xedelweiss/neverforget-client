import {defineMessages} from "react-intl";

const messages = defineMessages({
  reset_your_password: {
    id: 'reset_your_password',
    defaultMessage: 'Reset Your Password'
  },

  congratulations: {
    id: 'congratulations',
    defaultMessage: 'Congratulations'
  },

  set_new_password: {
    id: 'set_new_password',
    defaultMessage: 'Set New Password'
  },

  reset_password: {
    id: "Reset_Password",
    defaultMessage: "Reset Password"
  }

});

export default messages;