export default function (response) {
  let errors = [];

  if (response.data) {
    errors = response.data.result.errors;
  } else {
    errors = [response.message];
  }

  return errors;
}