function getValueIdFromImmutable(container, entityName, idFieldName) {
  return entityName
    ? container.get(entityName).get(idFieldName)
    : container.get(idFieldName)
}

function getValueIdFromObject(container, entityName, idFieldName) {
  return entityName
    ? container[entityName][idFieldName]
    : container[idFieldName]
}

export default function (entitiesList, entityName = null, id, immutableTypes = true, idFieldName = 'id') {
  let result = -1;

  entitiesList.map((container, key) => {
    let currentItemId = immutableTypes
      ? getValueIdFromImmutable(container, entityName, idFieldName)
      : getValueIdFromObject(container, entityName, idFieldName);

    if (currentItemId == id) {
      result = key;
    }

    return container;
  });

  return result;
}