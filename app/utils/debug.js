export function log(msg) {
  const enabled = __DEV__;

  if ( enabled === true ) {
    console.log(msg);
  }
}