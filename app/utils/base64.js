export function base64_encode(str) {
  return window.btoa(unescape(encodeURIComponent(str)));
}
// base64 encoded ascii to ucs-2 string
export function base64_decode(str) {
  return decodeURIComponent(escape(window.atob(str)));
}