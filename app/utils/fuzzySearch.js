export default function (haystack, needle) {
  var hay = haystack.toLowerCase(), i = 0, n = -1, l;
  var s   = needle.toLowerCase();
  for (; l = s[i++];) if (!~(n = hay.indexOf(l, n + 1))) return false;
  return true;
};