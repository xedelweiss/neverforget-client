import queryString from "query-string";

const defaultLocale = 'en-us';

export default function getCurrentLocale () {
  let location = window.location;
  let parameters = location.search;
  let parsedStr = queryString.parse(parameters);

  let locale = defaultLocale;
  if ( parsedStr.locale ) {
    locale = parsedStr.locale;
  }

  return locale;
}