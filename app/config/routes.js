import React from 'react';
import {Route, IndexRoute} from 'react-router';

import MainLayout from '../pages/MainLayout';
import Dashboard from '../pages/Dashboard';
import Login from "../pages/Login";
import LoginSocial from "../pages/LoginSocialPage";
import Register from "../pages/Register";
import SchedulePage from "../pages/SchedulePage";
import MetapersonsPage from '../pages/MetapersonsPage';
import MetapersonPage from '../pages/MetapersonPage';
import CreateMetapersonPage from '../pages/CreateMetapersonPage';
import Settings from "../pages/Settings";
import PasswordReset from "../pages/PasswordReset";
import PasswordChange from "../pages/PasswordChange";
import {log} from "../utils/debug";

const routeChanged = (prevRoute, nextRoute) => {
  //if we go from password change page then remove token GET parameter from URL
  if ( prevRoute.location.pathname == "/password/change" ) {
    let location    = window.location;
    let newLocation = `${location.protocol}//${location.host}${location.pathname}#${nextRoute.location.pathname}`;
    window.location.replace(newLocation);
  }
};

const routes = (
  <Route component={MainLayout} onChange={routeChanged}>
    <IndexRoute component={Dashboard}/>
    <Route path="/dashboard" component={Dashboard}/>
    <Route path="/login" component={Login}/>
    <Route path="/login-via-social/:provider(/:code)" component={LoginSocial}/>
    <Route path="/register" component={Register}/>
    <route path="/settings" component={Settings}/>
    <Route path="/schedule" component={SchedulePage}/>
    <Route path="/contacts" component={MetapersonsPage}/>
    <Route path="/contacts/:metapersonId" component={MetapersonPage}/>
    <Route path="/contacts/new" component={CreateMetapersonPage}/>
    <Route path="/password/reset" component={PasswordReset}/>
    <Route path="/password/change" component={PasswordChange} />
  </Route>
);

export default routes;