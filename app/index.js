import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, hashHistory, browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import axios from 'axios';

import "./styles/main.scss";

import {IntlProvider, addLocaleData} from 'react-intl';
import en from "react-intl/locale-data/en";
import uk from "react-intl/locale-data/uk";

import Application from './containers/Application';
import routes from './config/routes';
import configureStore from './store/configureStore';
import {setToken} from './actions/auth';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

import getCurrentLocale from "./config/locale";
addLocaleData([...en, ...uk]);
import localeData from './i18n/locales/data.json';
const currentLocale = getCurrentLocale();
const messages = localeData[currentLocale] || localeData.en;

function checkToken() {
    let token = localStorage.getItem('authToken');
    let dispatch = store.dispatch;

    dispatch(setToken(token));

    // redirect to login page on 401
    axios.interceptors.response.use(function (response) {
        return response;
      },
      function (error) {
        if (error.status == 401) {
            dispatch(setToken(null));
            history.push('/login');
        }

        return Promise.reject(error);
    });
}

ReactDOM.render(
  <IntlProvider locale={currentLocale} messages={messages}>
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={Application} onEnter={checkToken}>
                {routes}
            </Route>
        </Router>
    </Provider>
    </IntlProvider>,
    document.getElementById('app')
);