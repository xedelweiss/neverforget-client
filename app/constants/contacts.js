// metapersons list

export const APPLY_METAPERSONS_FILTER = 'APPLY_METAPERSONS_FILTER';
export const RESET_METAPERSONS_FILTER = 'RESET_METAPERSONS_FILTER';

export const SELECT_METAPERSON = 'SELECT_METAPERSON';
export const UNSELECT_METAPERSON = 'UNSELECT_METAPERSON';

export const MERGE_METAPERSONS = 'MERGE_METAPERSONS';
export const MERGE_METAPERSONS_SUCCESS = 'MERGE_METAPERSONS_SUCCESS';
export const MERGE_METAPERSONS_FAILURE = 'MERGE_METAPERSONS_FAILURE';

// active metaperson

export const INIT_NEW_METAPERSON = 'INIT_NEW_METAPERSON';

export const START_EDIT_ACTIVE_METAPERSON = 'START_EDIT_ACTIVE_METAPERSON';
export const CANCEL_EDIT_ACTIVE_METAPERSON = 'CANCEL_EDIT_ACTIVE_METAPERSON';
export const COMMIT_EDIT_ACTIVE_METAPERSON = 'COMMIT_EDIT_ACTIVE_METAPERSON';

// metaperson

export const CREATE_METAPERSON = 'CREATE_METAPERSON';
export const CREATE_METAPERSON_SUCCESS = 'CREATE_METAPERSON_SUCCESS';
export const CREATE_METAPERSON_FAILURE = 'CREATE_METAPERSON_FAILURE';

export const FETCH_METAPERSONS         = 'FETCH_METAPERSONS';
export const FETCH_METAPERSONS_SUCCESS = 'FETCH_METAPERSONS_SUCCESS';
export const FETCH_METAPERSONS_FAILURE = 'FETCH_METAPERSONS_FAILURE';

export const FETCH_METAPERSON         = 'FETCH_METAPERSON';
export const FETCH_METAPERSON_SUCCESS = 'FETCH_METAPERSON_SUCCESS';
export const FETCH_METAPERSON_FAILURE = 'FETCH_METAPERSON_FAILURE';

export const SAVE_METAPERSON = 'SAVE_METAPERSON';
export const SAVE_METAPERSON_SUCCESS = 'SAVE_METAPERSON_SUCCESS';
export const SAVE_METAPERSON_FAILURE = 'SAVE_METAPERSON_FAILURE';

export const SAVE_METAPERSON_WITH_RELATIONS = 'SAVE_METAPERSON_WITH_RELATIONS';
export const SAVE_METAPERSON_WITH_RELATIONS_SUCCESS = 'SAVE_METAPERSON_WITH_RELATIONS_SUCCESS';
export const SAVE_METAPERSON_WITH_RELATIONS_FAILURE = 'SAVE_METAPERSON_WITH_RELATIONS_FAILURE';

export const DELETE_METAPERSON         = 'DELETE_METAPERSON';
export const DELETE_METAPERSON_SUCCESS = 'DELETE_METAPERSON_SUCCESS';
export const DELETE_METAPERSON_FAILURE = 'DELETE_METAPERSON_FAILURE';

// persons

export const FETCH_METAPERSON_PERSONS = 'FETCH_METAPERSON_PERSONS';
export const FETCH_METAPERSON_PERSONS_SUCCESS = 'FETCH_METAPERSON_PERSONS_SUCCESS';
export const FETCH_METAPERSON_PERSONS_FAILURE = 'FETCH_METAPERSON_PERSONS_FAILURE';

export const UNMERGE_METAPERSON_PERSON = 'UNMERGE_METAPERSON_PERSON';
export const UNMERGE_METAPERSON_PERSON_SUCCESS = 'UNMERGE_METAPERSON_PERSON_SUCCESS';
export const UNMERGE_METAPERSON_PERSON_FAILURE = 'UNMERGE_METAPERSON_PERSON_FAILURE';

// contacts

export const INIT_NEW_METAPERSON_CONTACT = 'INIT_NEW_METAPERSON_CONTACT';
export const EDIT_METAPERSON_CONTACT_BY_KEY = 'EDIT_METAPERSON_CONTACT_BY_KEY';

export const CREATE_METAPERSON_CONTACT = 'CREATE_METAPERSON_CONTACT';
export const CREATE_METAPERSON_CONTACT_SUCCESS = 'CREATE_METAPERSON_CONTACT_SUCCESS';
export const CREATE_METAPERSON_CONTACT_FAILURE = 'CREATE_METAPERSON_CONTACT_FAILURE';

export const FETCH_METAPERSON_CONTACTS         = 'FETCH_METAPERSON_CONTACTS';
export const FETCH_METAPERSON_CONTACTS_SUCCESS = 'FETCH_METAPERSON_CONTACTS_SUCCESS';
export const FETCH_METAPERSON_CONTACTS_FAILURE = 'FETCH_METAPERSON_CONTACTS_FAILURE';

export const SAVE_METAPERSON_CONTACT = 'SAVE_METAPERSON_CONTACT';
export const SAVE_METAPERSON_CONTACT_SUCCESS = 'SAVE_METAPERSON_CONTACT_SUCCESS';
export const SAVE_METAPERSON_CONTACT_FAILURE = 'SAVE_METAPERSON_CONTACT_FAILURE';

export const DELETE_METAPERSON_CONTACT_BY_KEY = 'DELETE_METAPERSON_CONTACT_BY_KEY';
export const DELETE_METAPERSON_CONTACT = 'DELETE_METAPERSON_CONTACT';
export const DELETE_METAPERSON_CONTACT_SUCCESS = 'DELETE_METAPERSON_CONTACT_SUCCESS';
export const DELETE_METAPERSON_CONTACT_FAILURE = 'DELETE_METAPERSON_CONTACT_FAILURE';

// dates

export const INIT_NEW_METAPERSON_DATE = 'INIT_NEW_METAPERSON_DATE';
export const EDIT_METAPERSON_DATE_BY_KEY = 'EDIT_METAPERSON_DATE_BY_KEY';

export const CREATE_METAPERSON_DATE = 'CREATE_METAPERSON_DATE';
export const CREATE_METAPERSON_DATE_SUCCESS = 'CREATE_METAPERSON_DATE_SUCCESS';
export const CREATE_METAPERSON_DATE_FAILURE = 'CREATE_METAPERSON_DATE_FAILURE';

export const FETCH_METAPERSON_DATES         = 'FETCH_METAPERSON_DATES';
export const FETCH_METAPERSON_DATES_SUCCESS = 'FETCH_METAPERSON_DATES_SUCCESS';
export const FETCH_METAPERSON_DATES_FAILURE = 'FETCH_METAPERSON_DATES_FAILURE';

export const SAVE_METAPERSON_DATE = 'SAVE_METAPERSON_DATE';
export const SAVE_METAPERSON_DATE_SUCCESS = 'SAVE_METAPERSON_DATE_SUCCESS';
export const SAVE_METAPERSON_DATE_FAILURE = 'SAVE_METAPERSON_DATE_FAILURE';

export const DELETE_METAPERSON_DATE_BY_KEY = 'DELETE_METAPERSON_DATE_BY_KEY';
export const DELETE_METAPERSON_DATE = 'DELETE_METAPERSON_DATE';
export const DELETE_METAPERSON_DATE_SUCCESS = 'DELETE_METAPERSON_DATE_SUCCESS';
export const DELETE_METAPERSON_DATE_FAILURE = 'DELETE_METAPERSON_DATE_FAILURE';