import {fromJS} from 'immutable';
import {log} from "../utils/debug";
import {
  PASSWORD_UPDATE,
  PASSWORD_UPDATE_FAILURE,
  PASSWORD_UPDATE_SUCCESS
} from "../constants/settings";

import {
  USER_UPDATE,
  USER_UPDATE_SUCCESS
} from "../constants/settings";

const INITIAL_STATE = fromJS({
  password: {
    passwordErrors:     [],
    newPasswordErrors:  [],
    passwordUpdated:    false
  },

  userInfo: {
    isUpdated: false
  }
});

export default function (state = INITIAL_STATE, action) {
  // log(action.type);

  switch (action.type) {
    case  PASSWORD_UPDATE:
      return state
        .set('password', action.payload);

    case  PASSWORD_UPDATE_FAILURE:
      return state
        .set('password', action.payload);

    case  PASSWORD_UPDATE_SUCCESS:
      return state
        .set('password', action.payload);

    case USER_UPDATE:
      return state;

    case USER_UPDATE_SUCCESS:
      return state;

    default:
      return state;
  }
}