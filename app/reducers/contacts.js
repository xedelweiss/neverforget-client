import {combineReducers} from 'redux';

import MetapersonsListReducer from './contacts/metapersonsList';
import ActiveMetapersonReducers from './contacts/activeMetaperson/';

export default combineReducers({
  metapersonsList: MetapersonsListReducer,
  activeMetaperson: combineReducers(ActiveMetapersonReducers),
});