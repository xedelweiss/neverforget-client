import {fromJS} from 'immutable';
import * as types from '../constants/socials';
import getListKeyById from '../utils/listKeyById';

const INITIAL_STATE = fromJS({
  new: {
    step: null,
    provider: null,
    authUrl: null,
    authCode: null,
    userInfo: {
      data: {},
      isNew: null,
    },
    isLoading: false,
    errors: [],
  },
  attached: [],
  socialsList: {
    socials: [], // {data = {}, isLoading = false, errors = []}
    isLoading: false,
    errors: [],
  },
});

export default function (state = INITIAL_STATE, action) {
  let key = null;

  switch (action.type) {
    case types.INIT_NEW_SOCIAL:
      return state
        .set('new', INITIAL_STATE.get('new').merge({
          step: types.SOCIAL_AUTH_STEP_INITIAL,
          provider: action.payload.provider,
        }));

    case types.INIT_SOCIALS_INFO:
      return state.set('attached', fromJS(action.payload.socials));

    case types.FETCH_AUTHORIZATION_REQUEST_URL:
      return state.mergeIn(['new'], {
        errors: [],
        isLoading: true,
      });
    case types.FETCH_AUTHORIZATION_REQUEST_URL_SUCCESS:
      return state.mergeIn(['new'], {
        step: types.SOCIAL_AUTH_STEP_HAVE_AUTH_URL,
        authUrl: action.payload.url,
        isLoading: false,
      });
    case types.FETCH_AUTHORIZATION_REQUEST_URL_FAILURE:
      return state.mergeIn(['new'], {
        errors: action.payload.errors,
        isLoading: false,
      });

    case types.RESUME_AUTHORIZATION_FLOW:
      return state.mergeIn(['new'], {
        step: types.SOCIAL_AUTH_STEP_HAVE_AUTH_CODE,
        authCode: action.payload.code,
        provider: action.payload.provider,
      });

    case types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE:
      return state.mergeIn(['new'], {
        errors: [],
        isLoading: true,
      });
    case types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE_SUCCESS:
      return state.mergeIn(['new'], {
        step: types.SOCIAL_AUTH_STEP_HAVE_USER_INFO,
        userInfo: {
          data: action.payload.userInfo,
          isNew: action.payload.isNew,
        },
        isLoading: false,
      });
    case types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE_FAILURE:
      return state.mergeIn(['new'], {
        errors: action.payload.errors,
        isLoading: false,
      });

    case types.FETCH_SOCIALS:
      return state.mergeIn(['socialsList'], {
        socials: [],
        errors: [],
        isLoading: true,
      });
    case types.FETCH_SOCIALS_SUCCESS:
      return state.mergeIn(['socialsList'], {
        socials: action.payload.socials.map(item => {
          return {
            data: item,
            isLoading: false,
            errors: [],
          }
        }),
        errors: [],
        isLoading: false,
      });
    case types.FETCH_SOCIALS_FAILURE:
      return state.mergeIn(['socialsList'], {
        errors: action.payload.errors,
        isLoading: false,
      });

    case types.DELETE_SOCIAL:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      return state
        .setIn(['socialsList', 'socials', key, 'isLoading'], true);
    case types.DELETE_SOCIAL_SUCCESS:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      return state
        .deleteIn(['socialsList', 'socials', key]);
    case types.DELETE_SOCIAL_FAILURE:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      return state
        .mergeIn(['socialsList', 'socials', key, 'social'], {
          isLoading: false,
          errors: action.payload.errors
        });

    case types.SYNC_SOCIAL:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      return state
        .setIn(['socialsList', 'socials', key, 'isLoading'], true);
    case types.SYNC_SOCIAL_SUCCESS:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      console.warn('ADD REACTION FOR SYNC SOCIAL SUCCESS');
      return state
        .setIn(['socialsList', 'socials', key, 'isLoading'], false);
    case types.SYNC_SOCIAL_FAILURE:
      key = getListKeyById(state.get('socialsList').get('socials'), 'data', action.payload.social.id);
      return state
        .mergeIn(['socialsList', 'socials', key, 'social'], {
          isLoading: false,
          errors: action.payload.errors
        });

    default:
      return state;
  }
};