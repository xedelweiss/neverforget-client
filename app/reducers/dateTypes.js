import {fromJS} from 'immutable';

import {
  FETCH_DATE_TYPES,
  FETCH_DATE_TYPES_SUCCESS,
  FETCH_DATE_TYPES_FAILURE,
} from '../constants/dateTypes';

const INITIAL_STATE = fromJS({
  dateTypes: [],
  loading: false,
  errors: []
});

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_DATE_TYPES:
      return state.merge({
        dateTypes: [],
        errors: [],
        loading: true
      });
    case FETCH_DATE_TYPES_SUCCESS:
      return state.merge({
        dateTypes: action.payload.dateTypes,
        errors: [],
        loading: false
      });
    case FETCH_DATE_TYPES_FAILURE:
      return state.merge({
        persons: [],
        errors: action.payload.errors,
        loading: false
      });

    default:
      return state;
  }
};