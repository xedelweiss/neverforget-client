import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import EventsReducer from './events';
import AuthReducer from './auth';
import DataReducer from './data';
import ContactsReducer from './contacts';
import DateTypesReducer from './dateTypes';
import Settings from './settings';
import SocialsReducer from './socials';
import GroupsReducer from './groups';

const rootReducer = combineReducers({
  routing:        routerReducer,
  events:         EventsReducer,
  auth:           AuthReducer,
  contacts:       ContactsReducer,
  data:           DataReducer,
  dateTypesList:  DateTypesReducer,
  settings:       Settings,
  socials:        SocialsReducer,
  groupsList:     GroupsReducer,
});

export default rootReducer;