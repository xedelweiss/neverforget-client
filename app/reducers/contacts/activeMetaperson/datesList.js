import {fromJS} from 'immutable';

import {
  INIT_NEW_METAPERSON_DATE,
  EDIT_METAPERSON_DATE_BY_KEY,

  FETCH_METAPERSON_DATES,
  FETCH_METAPERSON_DATES_SUCCESS,
  FETCH_METAPERSON_DATES_FAILURE,

  SAVE_METAPERSON_DATE,
  SAVE_METAPERSON_DATE_SUCCESS,
  SAVE_METAPERSON_DATE_FAILURE,

  SAVE_METAPERSON_WITH_RELATIONS,
  SAVE_METAPERSON_WITH_RELATIONS_SUCCESS,
  SAVE_METAPERSON_WITH_RELATIONS_FAILURE,

  DELETE_METAPERSON_DATE_BY_KEY,
  DELETE_METAPERSON_DATE,
  DELETE_METAPERSON_DATE_SUCCESS,
  DELETE_METAPERSON_DATE_FAILURE,

  INIT_NEW_METAPERSON,

  START_EDIT_ACTIVE_METAPERSON,
  CANCEL_EDIT_ACTIVE_METAPERSON,
  COMMIT_EDIT_ACTIVE_METAPERSON,
} from '../../../constants/contacts';

const INITIAL_STATE = fromJS({
  dates: [], // {data = {}, loading = false, errors = []}
  loading: false,
  errors: []
});

function getListKeyById(entitiesList, entityName, id) {
  let result = -1;

  entitiesList.map((container, key) => {
    if (container.get(entityName).get('id') == id) {
      result = key;
    }

    return container;
  });

  return result;
}

export default function (state = INITIAL_STATE, action) {
  let key = null;

  switch (action.type) {
    case INIT_NEW_METAPERSON:
      return INITIAL_STATE;

    case INIT_NEW_METAPERSON_DATE:
      return state.merge({
        dates: state.get('dates').push(fromJS({
          data: action.payload.date,
          errors: [],
          loading: false,
        }))
      });

    case EDIT_METAPERSON_DATE_BY_KEY:
      return state.mergeIn(['dates', action.payload.key, 'data'], action.payload.date);

    case FETCH_METAPERSON_DATES:
      return state.merge({
          dates: [],
          errors: [],
          loading: true
        });
    case FETCH_METAPERSON_DATES_SUCCESS:
      return state.merge({
          dates: action.payload.dates.map(item => {
            return {
              data: item,
              loading: false,
              errors: []
            }
          }),
          errors: [],
          loading: false
        });
    case FETCH_METAPERSON_DATES_FAILURE:
      return state.merge({
          dates: [],
          errors: action.payload.errors,
          loading: false
        });

    case SAVE_METAPERSON_DATE:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .mergeIn(['dates', key], {
          data: action.payload.date,
          loading: true,
        });
    case SAVE_METAPERSON_DATE_SUCCESS:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .mergeIn(['dates', key], {
          loading: false,
          errors: [],
          data: action.payload.date
        });
    case SAVE_METAPERSON_DATE_FAILURE:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .mergeIn(['dates', key], {
          loading: false,
          errors: action.payload.errors
        });

    case SAVE_METAPERSON_WITH_RELATIONS:
      return state.merge({
        dates: state.get('dates').map((date, key) => {
          let payload = action.payload.dates;

          if (typeof payload[key] == 'undefined') {
            return date;
          }

          return date.merge({
            data: payload[key],
            loading: true,
            errors: [],
          });
        })
      });
    case SAVE_METAPERSON_WITH_RELATIONS_SUCCESS:
      return state.merge({
        dates: state.get('dates').map((date, key) => {
          let payload = action.payload.dates;

          if (typeof payload[key] == 'undefined') {
            return date;
          }

          return date.merge({
            data: payload[key],
            loading: false,
          });
        })
      });
    case SAVE_METAPERSON_WITH_RELATIONS_FAILURE:
      return state.merge({
        dates: state.get('dates').map((date, key) => {
          let payload = action.payload.errors.dates;

          if (typeof payload[key] == 'undefined') {
            return date;
          }

          return date.merge({
            errors: payload[key],
            loading: false,
          });
        })
      });

    case DELETE_METAPERSON_DATE_BY_KEY:
      return state
        .deleteIn(['dates', action.payload.key]);
    case DELETE_METAPERSON_DATE:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .setIn(['dates', key, 'loading'], true);
    case DELETE_METAPERSON_DATE_SUCCESS:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .deleteIn(['dates', key]);
    case DELETE_METAPERSON_DATE_FAILURE:
      key = getListKeyById(state.get('dates'), 'data', action.payload.date.id);
      return state
        .mergeIn(['dates', key], {
          loading: false,
          errors: action.payload.errors
        });

    case START_EDIT_ACTIVE_METAPERSON:
      return state
        .set('originalData', state.get('dates'));
    case CANCEL_EDIT_ACTIVE_METAPERSON:
      return state
        .set('dates', state.get('originalData'))
        .set('errors', [])
        .set('originalData', null);
    // case COMMIT_EDIT_ACTIVE_METAPERSON:
    //   return state
    //     .set('dates', state.get('originalData'))
    //     .set('originalData', null);


    default:
      return state;
  }
};