import metaperson from './metaperson';
import contactsList from './contactsList';
import datesList from './datesList';
import personsList from './personsList';

export default {
  metaperson,
  contactsList,
  datesList,
  personsList,
}