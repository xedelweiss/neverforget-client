import {fromJS} from 'immutable';

import {
  UNMERGE_METAPERSON_PERSON,
  UNMERGE_METAPERSON_PERSON_SUCCESS,
  UNMERGE_METAPERSON_PERSON_FAILURE,

  FETCH_METAPERSON_PERSONS,
  FETCH_METAPERSON_PERSONS_SUCCESS,
  FETCH_METAPERSON_PERSONS_FAILURE,

  INIT_NEW_METAPERSON,

  START_EDIT_ACTIVE_METAPERSON,
  CANCEL_EDIT_ACTIVE_METAPERSON,
  COMMIT_EDIT_ACTIVE_METAPERSON,
} from '../../../constants/contacts';

const INITIAL_STATE = fromJS({
  persons: [], // {data = {}, loading = false, errors = []}
  loading: false,
  errors: []
});

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case INIT_NEW_METAPERSON:
      return INITIAL_STATE;

    case FETCH_METAPERSON_PERSONS:
      return state.merge({
          persons: [],
          errors: [],
          loading: true
        });
    case FETCH_METAPERSON_PERSONS_SUCCESS:
      return state.merge({
          persons: action.payload.persons.map(item => {
            return {
              data: item,
              loading: false,
              errors: []
            }
          }),
          errors: [],
          loading: false
        });
    case FETCH_METAPERSON_PERSONS_FAILURE:
      return state.merge({
          persons: [],
          errors: action.payload.errors,
          loading: false
        });

    // @todo reload after unmerge
    case UNMERGE_METAPERSON_PERSON:
      return state
        .set('loading', true);
    case UNMERGE_METAPERSON_PERSON_SUCCESS:
      return state
      // .set('metaperson', action.payload.metaperson)
        .set('errors', [])
        .set('loading', false);
    case UNMERGE_METAPERSON_PERSON_FAILURE:
      return state
        .set('loading', false)
        .set('errors', action.payload.errors);

    case START_EDIT_ACTIVE_METAPERSON:
      return state
        .set('originalData', state.get('persons'));
    case CANCEL_EDIT_ACTIVE_METAPERSON:
      return state
        .set('persons', state.get('originalData'))
        .set('errors', [])
        .set('originalData', null);
    // case COMMIT_EDIT_ACTIVE_METAPERSON:
    //   return state
    //     .set('persons', state.get('originalData'))
    //     .set('originalData', null);

    default:
      return state;
  }
};