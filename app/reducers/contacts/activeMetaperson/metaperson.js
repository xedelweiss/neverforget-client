import {fromJS} from 'immutable';

import {
  CREATE_METAPERSON,
  CREATE_METAPERSON_SUCCESS,
  CREATE_METAPERSON_FAILURE,

  FETCH_METAPERSON,
  FETCH_METAPERSON_SUCCESS,
  FETCH_METAPERSON_FAILURE,

  SAVE_METAPERSON,
  SAVE_METAPERSON_SUCCESS,
  SAVE_METAPERSON_FAILURE,

  SAVE_METAPERSON_WITH_RELATIONS,
  SAVE_METAPERSON_WITH_RELATIONS_SUCCESS,
  SAVE_METAPERSON_WITH_RELATIONS_FAILURE,

  INIT_NEW_METAPERSON,

  START_EDIT_ACTIVE_METAPERSON,
  CANCEL_EDIT_ACTIVE_METAPERSON,
  COMMIT_EDIT_ACTIVE_METAPERSON,

  MERGE_METAPERSONS,
  MERGE_METAPERSONS_SUCCESS,
  MERGE_METAPERSONS_FAILURE,
} from '../../../constants/contacts';

const INITIAL_STATE = fromJS({
  isEdited: false,
  isNew: false,

  data: {},
  originalData: null,
  loading: false,
  errors: [],
});

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case INIT_NEW_METAPERSON:
      return INITIAL_STATE
        .set('isNew', true)
        .set('isEdited', true);

    // @todo reset contacts/dates/persons?
    case FETCH_METAPERSON:
      return state.merge({
          isEdited: false,
          data: {},
          loading: true,
          errors: []
        });
    case FETCH_METAPERSON_SUCCESS:
      return state.merge({
          data: action.payload.metaperson,
          loading: false,
          errors: [],
        });
    case FETCH_METAPERSON_FAILURE:
      return state.merge({
          data: {},
          loading: false,
          errors: action.payload.errors,
        });

    case START_EDIT_ACTIVE_METAPERSON:
      return state
        .set('isEdited', true)
        .set('originalData', state.get('data'));
    case CANCEL_EDIT_ACTIVE_METAPERSON:
      return state
        .set('isEdited', false)
        .set('data', state.get('originalData'))
        .set('errors', [])
        .set('originalData', null);
    case COMMIT_EDIT_ACTIVE_METAPERSON:
      return state
        .set('isEdited', false)
        .set('data', state.get('originalData'))
        .set('originalData', null);

    case MERGE_METAPERSONS:
      if (action.payload.metapersons.filter(metaperson => metaperson.id == state.get('data').get('id')).length == 0) {
        return state;
      }

      return state.merge({
        loading: true,
        errors: [],
      });
    case MERGE_METAPERSONS_SUCCESS:
      return state
        .set('isEdited', false)
        .merge({
          data: action.payload.metaperson,
          loading: false,
          errors: [],
        });

    case SAVE_METAPERSON:
      return state.merge({
          data: action.payload.metaperson,
          loading: true,
          errors: [],
        });
    case SAVE_METAPERSON_SUCCESS:
      return state
        .set('isEdited', false)
        .merge({
          data: action.payload.metaperson,
          loading: false,
          errors: [],
        });
    case SAVE_METAPERSON_FAILURE:
      return state.merge({
          loading: false,
          errors: action.payload.errors,
        });

    case SAVE_METAPERSON_WITH_RELATIONS:
      return state.merge({
        data: action.payload.metaperson,
        loading: true,
        errors: [],
      });
    case SAVE_METAPERSON_WITH_RELATIONS_SUCCESS:
      return state
        .set('isEdited', false)
        .merge({
          data: action.payload.metaperson,
          loading: false,
          errors: [],
        });
    case SAVE_METAPERSON_WITH_RELATIONS_FAILURE:
      return state.merge({
        loading: false,
        errors: action.payload.errors.metaperson,
      });

    case CREATE_METAPERSON:
      return state.merge({
        loading: true,
        errors: [],
      });
    case CREATE_METAPERSON_SUCCESS:
      return state
        .set('isEdited', false)
        .merge({
          data: action.payload.metaperson,
          loading: false,
          errors: [],
        });
    case CREATE_METAPERSON_FAILURE:
      return state.merge({
        loading: false,
        errors: action.payload.errors,
      });

    default:
      return state;
  }
};