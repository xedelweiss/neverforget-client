import {fromJS} from 'immutable';

import {
  INIT_NEW_METAPERSON_CONTACT,
  EDIT_METAPERSON_CONTACT_BY_KEY,

  FETCH_METAPERSON_CONTACTS,
  FETCH_METAPERSON_CONTACTS_SUCCESS,
  FETCH_METAPERSON_CONTACTS_FAILURE,

  SAVE_METAPERSON_CONTACT,
  SAVE_METAPERSON_CONTACT_SUCCESS,
  SAVE_METAPERSON_CONTACT_FAILURE,

  SAVE_METAPERSON_WITH_RELATIONS,
  SAVE_METAPERSON_WITH_RELATIONS_SUCCESS,
  SAVE_METAPERSON_WITH_RELATIONS_FAILURE,

  DELETE_METAPERSON_CONTACT_BY_KEY,
  DELETE_METAPERSON_CONTACT,
  DELETE_METAPERSON_CONTACT_SUCCESS,
  DELETE_METAPERSON_CONTACT_FAILURE,

  INIT_NEW_METAPERSON,

  START_EDIT_ACTIVE_METAPERSON,
  CANCEL_EDIT_ACTIVE_METAPERSON,
  COMMIT_EDIT_ACTIVE_METAPERSON,
} from '../../../constants/contacts';

const INITIAL_STATE = fromJS({
  contacts: [], // {data = {}, loading = false, errors = []}
  loading: false,
  errors: []
});

function getListKeyById(entitiesList, entityName, id) {
  let result = -1;

  entitiesList.map((container, key) => {
    if (container.get(entityName).get('id') == id) {
      result = key;
    }

    return container;
  });

  return result;
}

/**
 * @param state
 * @param action
 * @returns {*}
 */
export default function (state = INITIAL_STATE, action) {
  let key = null;

  switch (action.type) {
    case INIT_NEW_METAPERSON:
      return INITIAL_STATE;

    case INIT_NEW_METAPERSON_CONTACT:
      return state.merge({
        contacts: state.get('contacts').push(fromJS({
          data: action.payload.contact,
          errors: [],
          loading: false,
        }))
      });

    case EDIT_METAPERSON_CONTACT_BY_KEY:
      return state.mergeIn(['contacts', action.payload.key, 'data'], action.payload.contact);

    case FETCH_METAPERSON_CONTACTS:
      return state.merge({
          contacts: [],
          errors: [],
          loading: true,
        });
    case FETCH_METAPERSON_CONTACTS_SUCCESS:
      return state.merge({
          contacts: action.payload.contacts.map(item => {
            return {
              data: item,
              loading: false,
              errors: []
            }
          }),
          errors: [],
          loading: false
        });
    case FETCH_METAPERSON_CONTACTS_FAILURE:
      return state.merge({
          contacts: [],
          errors: action.payload.errors,
          loading: false
        });

    case SAVE_METAPERSON_CONTACT:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .mergeIn(['contacts', key], {
          data: action.payload.contact,
          loading: true,
        });
    case SAVE_METAPERSON_CONTACT_SUCCESS:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .mergeIn(['contacts', key], {
          loading: false,
          errors: [],
          data: action.payload.contact
        });
    case SAVE_METAPERSON_CONTACT_FAILURE:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .mergeIn(['contacts', key], {
          loading: false,
          errors: action.payload.errors
        });

    case SAVE_METAPERSON_WITH_RELATIONS:
      return state.merge({
        contacts: state.get('contacts').map((contact, key) => {
          let payload = action.payload.contacts;

          if (typeof payload[key] == 'undefined') {
            return contact;
          }

          return contact.merge({
            data: payload[key],
            loading: true,
            errors: [],
          });
        })
      });
    case SAVE_METAPERSON_WITH_RELATIONS_SUCCESS:
      return state.merge({
        contacts: state.get('contacts').map((contact, key) => {
          let payload = action.payload.contacts;

          if (typeof payload[key] == 'undefined') {
            return contact;
          }

          return contact.merge({
            data: payload[key],
            loading: false,
          });
        })
      });
    case SAVE_METAPERSON_WITH_RELATIONS_FAILURE:
      return state.merge({
        contacts: state.get('contacts').map((contact, key) => {
          let payload = action.payload.errors.contacts;

          if (typeof payload[key] == 'undefined') {
            return contact;
          }

          return contact.merge({
            errors: payload[key],
            loading: false,
          });
        })
      });

    case DELETE_METAPERSON_CONTACT_BY_KEY:
      return state
        .deleteIn(['contacts', action.payload.key]);
    case DELETE_METAPERSON_CONTACT:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .setIn(['contacts', key, 'loading'], true);
    case DELETE_METAPERSON_CONTACT_SUCCESS:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .deleteIn(['contacts', key]);
    case DELETE_METAPERSON_CONTACT_FAILURE:
      key = getListKeyById(state.get('contacts'), 'data', action.payload.contact.id);
      return state
        .mergeIn(['contacts', key], {
          loading: false,
          errors: action.payload.errors
        });

    case START_EDIT_ACTIVE_METAPERSON:
      return state
        .set('originalData', state.get('contacts'));
    case CANCEL_EDIT_ACTIVE_METAPERSON:
      return state
        .set('contacts', state.get('originalData'))
        .set('errors', [])
        .set('originalData', null);
    // case COMMIT_EDIT_ACTIVE_METAPERSON:
    //   return state
    //     .set('contacts', state.get('originalData'))
    //     .set('originalData', null);

    default:
      return state;
  }
};