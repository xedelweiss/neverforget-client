import {fromJS} from 'immutable';

import {
  FETCH_METAPERSONS,
  FETCH_METAPERSONS_SUCCESS,
  FETCH_METAPERSONS_FAILURE,

  DELETE_METAPERSON,
  DELETE_METAPERSON_SUCCESS,
  DELETE_METAPERSON_FAILURE,

  APPLY_METAPERSONS_FILTER,
  RESET_METAPERSONS_FILTER,

  SELECT_METAPERSON,
  UNSELECT_METAPERSON,

  MERGE_METAPERSONS,
  MERGE_METAPERSONS_SUCCESS,
  MERGE_METAPERSONS_FAILURE
} from '../../constants/contacts';

const INITIAL_STATE = fromJS({
  metapersons: [],
  errors: [],
  loading: false,
  filter: {
    text: ''
  },
  metapersonsLoaded: false
});

function getListKeyById(entitiesList, entityName, id) {
  let result = -1;

  entitiesList.map((container, key) => {
    if (container.get(entityName).get('id') == id) {
      result = key;
    }

    return container;
  });

  return result;
}

export default function (state = INITIAL_STATE, action) {
  let key = null;

  switch (action.type) {
    case FETCH_METAPERSONS:
      return state.set('loading', true);
    case FETCH_METAPERSONS_SUCCESS:
      return state
        .set('metapersons', fromJS(action.payload).map((metaperson) => {
          return fromJS({
            metaperson: metaperson,
            errors: [],
            loading: false,
            selected: false,
          })
        }))
        .set('loading', false);
    case FETCH_METAPERSONS_FAILURE:
      return state
        .set('loading', false)
        .set('errors', action.payload);

    case DELETE_METAPERSON:
      key = getListKeyById(state.get('metapersons'), 'metaperson', action.payload.metaperson.id);
      return state
        .setIn(['metapersons', key, 'loading'], true);
    case DELETE_METAPERSON_SUCCESS:
      key = getListKeyById(state.get('metapersons'), 'metaperson', action.payload.metaperson.id);
      return state
        .deleteIn(['metapersons', key]);
    case DELETE_METAPERSON_FAILURE:
      key = getListKeyById(state.get('metapersons'), 'metaperson', action.payload.metaperson.id);
      return state
        .mergeIn(['metapersons', key, 'metaperson'], {
          loading: false,
          errors: action.payload.errors
        });

    case APPLY_METAPERSONS_FILTER:
      return state
        .mergeIn(['filter'], fromJS(action.payload));
    case RESET_METAPERSONS_FILTER:
      return state
        .set('filter', fromJS({
          text: '',
          group: null,
          destination: null,
        }));

    case SELECT_METAPERSON:
      key = getListKeyById(state.get('metapersons'), 'metaperson', action.payload.metaperson.id);
      return state
        .setIn(['metapersons', key, 'selected'], true);
    case UNSELECT_METAPERSON:
      key = getListKeyById(state.get('metapersons'), 'metaperson', action.payload.metaperson.id);
      return state
        .setIn(['metapersons', key, 'selected'], false);

    case MERGE_METAPERSONS:
      key = action.payload.metapersons.map(metaperson => metaperson.id); // keyS actually
      return state
        .update('metapersons', list => list.map(metaperson => key.indexOf(metaperson.get('metaperson').get('id')) < 0 ? metaperson : metaperson.set('loading', true)));
    case MERGE_METAPERSONS_SUCCESS:
      key = action.payload.originalMetapersons.map(metaperson => metaperson.id); // keyS actually

      return state
        .set('loading', false)
        .update('metapersons', list => list.filter(metaperson => {return key.indexOf(metaperson.get('metaperson').get('id')) < 0;}))
        .update('metapersons', list => list.push(fromJS({
          metaperson: action.payload.metaperson,
          errors: [],
          loading: false,
          selected: false,
        })));
    case MERGE_METAPERSONS_FAILURE:
      key = action.payload.metapersons.map(metaperson => metaperson.id); // keyS actually
      return state
        .update('metapersons', list => list.map(metaperson => key.indexOf(metaperson.get('metaperson').get('id')) < 0 ? metaperson : metaperson.set('loading', false)));
      // todo: handle errors; which? how?
    default:
      return state;
  }
};