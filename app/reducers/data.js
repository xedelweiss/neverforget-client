import {fromJS} from "immutable";
import {log} from "../utils/debug";

import {
  GET_DATA_COMMON,
  GET_DATA_COMMON_SUCCESS,
  FETCH_USER_CONTACTS,
  FETCH_USER_CONTACTS_SUCCESS
} from "../constants/data";

const INITIAL_STATE = fromJS({
  userContacts: [],
  'timeZones':  []
});

export default function (state=INITIAL_STATE, action) {

  switch (action.type) {
    case  GET_DATA_COMMON:
      return state;

    case  GET_DATA_COMMON_SUCCESS:
      return state.set('timeZones', action.payload.data.timeZones);

    case  FETCH_USER_CONTACTS:
      return state;

    case FETCH_USER_CONTACTS_SUCCESS:
      return state
        .set('userContacts', action.payload);

    default:
      return state;
  }

  return state;
}