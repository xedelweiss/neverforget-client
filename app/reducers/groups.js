import {fromJS} from 'immutable';

import * as constants from '../constants/groups';

const INITIAL_STATE = fromJS({
  groups: [], // {data = {}, isLoading = false, errors = []}
  isLoading: false,
  errors: []
});

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case constants.FETCH_GROUPS:
      return state.merge({
        groups: [],
        errors: [],
        isLoading: true
      });
    case constants.FETCH_GROUPS_SUCCESS:
      return state.merge({
        groups: action.payload.groups.map(item => {
          return {
            data: item,
            isLoading: false,
            errors: []
          }
        }),
        errors: [],
        isLoading: false
      });
    case constants.FETCH_GROUPS_FAILURE:
      return state.merge({
        groups: [],
        errors: action.payload.errors,
        isLoading: false
      });

    default:
      return state;
  }
};