import {fromJS} from 'immutable';
import {log} from "../utils/debug";
import {
  SET_TOKEN,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  NEW_PASSWORD_REQUEST,
  NEW_PASSWORD_SUCCESS,
  RESET_PASSWORD_PAGE,
  NEW_PASSWORD_PAGE
} from '../constants/auth';

import {
  USER_UPDATE,
  USER_UPDATE_SUCCESS
} from "../constants/settings";

const INITIAL_STATE = fromJS({
  token: null,

  login: {
    errors: [],
    loading: false
  },

  register: {
    errors: [],
    loading: false
  },

  user: {},

  resetPassword: {
    isRestorePasswordInitiated: false,
    restorePasswordError:       false,
    passwordChanged:            false,
    errors:   [],
    loading:  false
  }
});

export default function (state = INITIAL_STATE, action) {
  // log(action.type);

  switch (action.type) {
    case SET_TOKEN:
      return state.set('token', action.payload);

    case LOGIN_REQUEST:
      return state.merge({
        token: null,
        login: {
          errors: [],
          loading: true
        }
      });

    case LOGIN_SUCCESS:
      return state.merge({
        login: {
          errors: [],
          loading: false
        }
      });

    case LOGIN_FAILURE:
      return state.merge({
        login: {
          errors: action.payload,
          loading: false
        }
      });

    case REGISTER_REQUEST:
      return state.merge({
        token: null,
        register: {
          errors: [],
          loading: true
        }
      });

    case REGISTER_SUCCESS:
      return state
        .set('token', action.payload.token)
        .set('register', {
          errors: [],
          loading: false
        });

    case REGISTER_FAILURE:
      return state.merge({
        register: {
          errors: action.payload,
          loading: false
        }
      });

    case RESET_PASSWORD_REQUEST:
      return state;

    case RESET_PASSWORD_SUCCESS:
      return state
        .setIn(['resetPassword', 'isRestorePasswordInitiated'], true);

    case RESET_PASSWORD_FAILURE:
      return state
        .setIn(['resetPassword', 'errors'], action.payload);

    case NEW_PASSWORD_REQUEST:
      return state;

    case NEW_PASSWORD_SUCCESS:
      return state
        .setIn(['resetPassword', 'passwordChanged'], true);

    case RESET_PASSWORD_PAGE:
      return state
        .setIn(['resetPassword', 'errors'], [])
        .setIn(['resetPassword', 'isRestorePasswordInitiated'], false);

    case NEW_PASSWORD_PAGE:
      return state
        .setIn(['resetPassword', 'passwordChanged'], false);

    default:
      return state;
  }
}