import {fromJS} from 'immutable';
import {log} from "../utils/debug";

import {
  FETCH_EVENTS,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE,
  CHANGE_EVENT_STATUS,
  CHANGE_EVENT_STATUS_SUCCESS,
  CHANGE_EVENT_STATUS_FAILURE,
  SAVE_EVENT,
  SAVE_EVENT_SUCCESS,
  SAVE_EVENT_FAILURE,
  DELETE_EVENT,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_FAILURE,
  SET_ACTIVE_DATE,
  CREATE_NEW_EVENT,
  CREATE_NEW_EVENT_SUCCESS,
  CREATE_NEW_EVENT_FAILURE
} from '../constants/events';

const INITIAL_STATE = fromJS({
  events:           [],
  errors:           [],
  // timezones:        [],
  activeDate:       {},
  newEvent:         {
    errors:     [],
    event:      {},
    showModal:  false
  }
});

function getListKeyById(list, id) {
  let result = -1;

  list.map((eventContainer, key) => {
    if (eventContainer.toJS().event.id == id) {
      result = key;
    }

    return eventContainer;
  });

  return result;
}

export default function (state = INITIAL_STATE, action) {
  let key = null;
  //log(action.type);
  switch (action.type) {
    case FETCH_EVENTS:
      return state.set("loadingEvents", true);

    case FETCH_EVENTS_SUCCESS:
      return state
        .set("events", fromJS(action.payload).map((event) => {
          return fromJS({
            event: event,
            errors: [],
            loading: false
          })}))
        .set("loadingEvents", false);

    case FETCH_EVENTS_FAILURE:
      return state
        .set("loadingEvents", false)
        .set("errors", action.payload);
    //--------------------------------------------------------------------------------------

    case CHANGE_EVENT_STATUS:
      key = getListKeyById(state.get('events'), action.payload.event.id);
      
      return state
        .setIn(['events', key, 'loading'], true);

    case CHANGE_EVENT_STATUS_SUCCESS:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .setIn(['events', key, 'event'], action.payload.event)
        .setIn(['events', key, 'loading'], false);

    case CHANGE_EVENT_STATUS_FAILURE:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .set("errors", action.payload.errors)
        .setIn(['events', key, 'loading'], false);
    //--------------------------------------------------------------------------------------

    case SAVE_EVENT:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        // .setIn(['events', key, 'loading'], false)
        .setIn(['events', key, 'showModal'], true);

    case SAVE_EVENT_SUCCESS:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .setIn(['events', key, 'event'], action.payload.event)
        .setIn(['events', key, 'errors'], [])
        .setIn(['events', key, 'loading'], false)
        .setIn(['events', key, 'showModal'], false);

    case SAVE_EVENT_FAILURE:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .setIn(['events', key, 'event'], action.payload.event)
        .setIn(['events', key, 'errors'], action.payload.errors)
        .setIn(['events', key, 'showModal'], true);
    //--------------------------------------------------------------------------------------

    case  DELETE_EVENT:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .setIn(['events', key, 'loading'], true);
    
    case  DELETE_EVENT_SUCCESS:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .deleteIn(['events', key]);

    case  DELETE_EVENT_FAILURE:
      key = getListKeyById(state.get('events'), action.payload.event.id);

      return state
        .set("errors", action.payload.errors)
        .setIn(['events', key, 'loading'], false);
    //--------------------------------------------------------------------------------------

    case  SET_ACTIVE_DATE:
      return state
        .set("activeDate", action.payload.activeDate);
    //--------------------------------------------------------------------------------------

    case  CREATE_NEW_EVENT:
      return state
        .setIn(['newEvent', 'showModal'], true);

    case  CREATE_NEW_EVENT_SUCCESS:
      return state
        .setIn(['newEvent', 'showModal'], false)
        .setIn(['newEvent', 'errors'], []);

    case CREATE_NEW_EVENT_FAILURE:
      return state.set('newEvent', fromJS({
        errors:     action.payload.errors,
        event:      action.payload.event,
        showModal:  true
      }));
    //--------------------------------------------------------------------------------------

    default:
      return state;
  }
}