import axios from "axios";
import {log} from "../../utils/debug";
import {
  GET_DATA_COMMON,
  GET_DATA_COMMON_SUCCESS,
  FETCH_USER_CONTACTS,
  FETCH_USER_CONTACTS_SUCCESS
} from "../../constants/data";

const GET_COMMON_DATA_PATH = '/data/common';

export function loadDefaultData() {
  return function (dispatch) {
    const getDataURL = `${API_ROOT}${GET_COMMON_DATA_PATH}`;

    dispatch({
      type:     GET_DATA_COMMON,
      payload:  {}
    });
    
    axios
      .get(getDataURL)
      .then((response) => {
        // log(response);
        dispatch(defaultDataLoaded(response.data.result.data));
      })
      .catch((response) => {
        // log(response);
      });
  };
}

export function defaultDataLoaded(data) {
  return {
    type:     GET_DATA_COMMON_SUCCESS,
    payload:  {data}
  }
}

export function fetchUserContacts() {
  return function(dispatch) {
    dispatch({
      type:     FETCH_USER_CONTACTS,
      payload:  {}
    });

    axios.get(`${API_ROOT}/data/user-contacts`)
      .then((response) => {
        dispatch(fetchUserContactsSuccess(response.data.result.data));
      })
      .catch((response) => {

      });
  };
}

export function fetchUserContactsSuccess(contacts) {
  return {
    type:     FETCH_USER_CONTACTS_SUCCESS,
    payload:  contacts
  }
}