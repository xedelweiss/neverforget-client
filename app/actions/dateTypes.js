import * as axios from 'axios';
import {
  FETCH_DATE_TYPES,
  FETCH_DATE_TYPES_SUCCESS,
  FETCH_DATE_TYPES_FAILURE,
} from '../constants/dateTypes';
import processErrorResponse from '../utils/processErrorResponse';

export function fetchDateTypes() {
  return function (dispatch) {
    dispatch({
      type: FETCH_DATE_TYPES,
      payload: {}
    });

    axios.get(`${API_ROOT}/data/date-types`)
      .then((response) => {
        dispatch(fetchDateTypesSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchDateTypesFailure(response));
      });
  }
}

export function fetchDateTypesSuccess(dateTypes) {
  return {
    type: FETCH_DATE_TYPES_SUCCESS,
    payload: {
      dateTypes,
    }
  }
}

export function fetchDateTypesFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_DATE_TYPES_FAILURE,
    payload: {
      errors,
    }
  }
}