import * as axios from 'axios';
import {
  INIT_NEW_METAPERSON_DATE,
  EDIT_METAPERSON_DATE_BY_KEY,

  // create
  CREATE_METAPERSON_DATE,
  CREATE_METAPERSON_DATE_SUCCESS,
  CREATE_METAPERSON_DATE_FAILURE,

  // read
  FETCH_METAPERSON_DATES,
  FETCH_METAPERSON_DATES_SUCCESS,
  FETCH_METAPERSON_DATES_FAILURE,

  // update
  SAVE_METAPERSON_DATE,
  SAVE_METAPERSON_DATE_SUCCESS,
  SAVE_METAPERSON_DATE_FAILURE,

  // delete
  DELETE_METAPERSON_DATE_BY_KEY,
  DELETE_METAPERSON_DATE,
  DELETE_METAPERSON_DATE_SUCCESS,
  DELETE_METAPERSON_DATE_FAILURE,
} from '../../constants/contacts';
import processErrorResponse from '../../utils/processErrorResponse';

export function initNewMetapersonDate(date) {
  return {
    type: INIT_NEW_METAPERSON_DATE,
    payload: {
      date,
    }
  }
}

export function editMetapersonDateByKey(key, date) {
  return {
    type: EDIT_METAPERSON_DATE_BY_KEY,
    payload: {
      key,
      date,
    }
  }
}

export function deleteMetapersonDateByKey(key, date) {
  return function (dispatch) {
    if (!date.id) {
      dispatch({
        type: DELETE_METAPERSON_DATE_BY_KEY,
        payload: {
          key,
        }
      });

      return;
    }

    dispatch(deleteMetapersonDate(date));
  };
}

export function createMetapersonDate(metapersonId, date) {
  return function (dispatch) {
    dispatch({
      type: CREATE_METAPERSON_DATE,
      payload: {
        metapersonId,
        date,
      }
    });

    axios.post(`${API_ROOT}/metapersons/${metapersonId}/contacts`, {date})
      .then((response) => {
        dispatch(createMetapersonDateSuccess(metapersonId, response.data.result.data));
      })
      .catch((response) => {
        dispatch(createMetapersonDateFailure(metapersonId, date, response));
      });
  }
}

export function createMetapersonDateSuccess(metapersonId, date) {
  return {
    type: CREATE_METAPERSON_DATE_SUCCESS,
    payload: {
      metapersonId,
      date,
    }
  }
}

export function createMetapersonDateFailure(metapersonId, date, response) {
  let errors = processErrorResponse(response);

  return {
    type: CREATE_METAPERSON_DATE_FAILURE,
    payload: {
      metapersonId,
      date,
      errors,
    }
  }
}

export function fetchMetapersonDates(metapersonId) {
  return function (dispatch) {
    dispatch({
      type: FETCH_METAPERSON_DATES,
      payload: {
        metapersonId: metapersonId
      }
    });

    axios.get(`${API_ROOT}/metapersons/${metapersonId}/dates`)
      .then((response) => {
        dispatch(fetchMetapersonDatesSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchMetapersonDatesFailure(response));
      });
  }
}

export function fetchMetapersonDatesSuccess(dates) {
  return {
    type: FETCH_METAPERSON_DATES_SUCCESS,
    payload: {
      dates: dates
    }
  }
}

export function fetchMetapersonDatesFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_METAPERSON_DATES_FAILURE,
    payload: {
      errors: errors
    }
  }
}

export function saveMetapersonDate(date, updateStateOnly = true) {
  return function (dispatch) {
    let metapersonId = date.person_id;

    dispatch({
      type: SAVE_METAPERSON_DATE,
      payload: {
        metapersonId: metapersonId,
        date: date,
      }
    });

    if (updateStateOnly) {
      return;
    }

    axios.put(`${API_ROOT}/metapersons/${metapersonId}/dates/${date.id}`, date)
      .then((response) => {
        dispatch(saveMetapersonDateSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(saveMetapersonDateFailure(date, response));
      });
  }
}

export function saveMetapersonDateSuccess(date) {
  let metapersonId = date.person_id;

  return {
    type: SAVE_METAPERSON_DATE_SUCCESS,
    payload: {
      metapersonId,
      date
    }
  }
}

export function saveMetapersonDateFailure(date, errors) {
  let metapersonId = date.person_id;

  return {
    type: SAVE_METAPERSON_DATE_FAILURE,
    payload: {
      metapersonId,
      date,
      errors
    }
  }
}

export function deleteMetapersonDate(date) {
  return function (dispatch) {
    dispatch({
      type: DELETE_METAPERSON_DATE,
      payload: {
        date,
      }
    });

    axios.delete(`${API_ROOT}/metapersons/${date.person_id}/dates/${date.id}`)
      .then(() => {
        dispatch(deleteMetapersonDateSuccess(date))
      })
      .catch((response) => {
        dispatch(deleteMetapersonDateFailure(date, response));
      })
  };
}

export function deleteMetapersonDateSuccess(date) {
  return {
    type: DELETE_METAPERSON_DATE_SUCCESS,
    payload: {
      date,
    }
  }
}

export function deleteMetapersonDateFailure(date, response) {
  let errors = processErrorResponse(response);

  return {
    type: DELETE_METAPERSON_DATE_FAILURE,
    payload: {
      date,
      errors,
    }
  }
}