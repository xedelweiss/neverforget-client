import * as axios from 'axios';
import {
  // read
  FETCH_METAPERSON_PERSONS,
  FETCH_METAPERSON_PERSONS_SUCCESS,
  FETCH_METAPERSON_PERSONS_FAILURE,

  // other
  UNMERGE_METAPERSON_PERSON,
  UNMERGE_METAPERSON_PERSON_SUCCESS,
  UNMERGE_METAPERSON_PERSON_FAILURE,
} from '../../constants/contacts';
import processErrorResponse from '../../utils/processErrorResponse';

export function fetchMetapersonPersons(metapersonId) {
  return function (dispatch) {
    dispatch({
      type: FETCH_METAPERSON_PERSONS,
      payload: {
        metapersonId: metapersonId
      }
    });

    axios.get(`${API_ROOT}/metapersons/${metapersonId}/persons`)
      .then((response) => {
        dispatch(fetchMetapersonPersonsSuccess(metapersonId, response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchMetapersonPersonsFailure(metapersonId, response));
      });
  }
}

export function fetchMetapersonPersonsSuccess(metapersonId, persons) {
  return {
    type: FETCH_METAPERSON_PERSONS_SUCCESS,
    payload: {
      metaperson: metapersonId,
      persons: persons
    }
  }
}

export function fetchMetapersonPersonsFailure(metapersonId, response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_METAPERSON_PERSONS_FAILURE,
    payload: {
      metaperson: metapersonId,
      errors: errors
    }
  }
}

export function unmergeMetapersonPerson(metaperson, person) {
  return function (dispatch) {
    dispatch({
      type: UNMERGE_METAPERSON_PERSON,
      payload: {
        metaperson: metaperson,
        person: person
      }
    });

    axios.delete(`${API_ROOT}/metapersons/${metaperson.id}/persons/${person.id}`)
      .then((response) => {
        dispatch(unmergeMetapersonPersonSuccess(metaperson, person));
      })
      .catch((response) => {
        dispatch(unmergeMetapersonPersonFailure(metaperson, person, response));
      });
  }
}

export function unmergeMetapersonPersonSuccess(metaperson, person) {
  return {
    type: UNMERGE_METAPERSON_PERSON_SUCCESS,
    payload: {
      metaperson: metaperson,
      person: person
    }
  }
}

export function unmergeMetapersonPersonFailure(metaperson, person, response) {
  let errors = processErrorResponse(response);

  return {
    type: UNMERGE_METAPERSON_PERSON_FAILURE,
    payload: {
      metaperson: metaperson,
      person: person,
      errors: errors
    }
  }
}