import * as axios from 'axios';
import {
  // create
  CREATE_METAPERSON,
  CREATE_METAPERSON_SUCCESS,
  CREATE_METAPERSON_FAILURE,

  // read
  FETCH_METAPERSONS,
  FETCH_METAPERSONS_SUCCESS,
  FETCH_METAPERSONS_FAILURE,

  FETCH_METAPERSON,
  FETCH_METAPERSON_SUCCESS,
  FETCH_METAPERSON_FAILURE,

  // update
  SAVE_METAPERSON,
  SAVE_METAPERSON_SUCCESS,
  SAVE_METAPERSON_FAILURE,

  SAVE_METAPERSON_WITH_RELATIONS,
  SAVE_METAPERSON_WITH_RELATIONS_SUCCESS,
  SAVE_METAPERSON_WITH_RELATIONS_FAILURE,

  // delete
  DELETE_METAPERSON,
  DELETE_METAPERSON_SUCCESS,
  DELETE_METAPERSON_FAILURE,
} from '../../constants/contacts';
import {saveMetapersonContact, saveMetapersonContactSuccess, saveMetapersonContactFailure} from './contacts';
import {saveMetapersonDate, saveMetapersonDateSuccess, saveMetapersonDateFailure} from './dates';
import processErrorResponse from '../../utils/processErrorResponse';

function objectEntries(obj) {
  let index = 0;

  // In ES6, you can use strings or symbols as property keys,
  // Reflect.ownKeys() retrieves both
  let propKeys = Reflect.ownKeys(obj);

  return {
    [Symbol.iterator]() {
      return this;
    },
    next() {
      if (index < propKeys.length) {
        let key = propKeys[index];
        index++;
        return {value: [key, obj[key]]};
      } else {
        return {done: true};
      }
    }
  };
}

export function createMetaperson(metaperson) {
  return function (dispatch) {
    dispatch({
      type: CREATE_METAPERSON,
      payload: {
        metaperson: metaperson,
      }
    });

    axios.post(`${API_ROOT}/metapersons`, metaperson)
      .then((response) => {
        dispatch(createMetapersonSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(createMetapersonFailure(metaperson, response));
      });
  }
}

export function createMetapersonSuccess(metaperson) {
  return {
    type: CREATE_METAPERSON_SUCCESS,
    payload: {
      metaperson: metaperson
    }
  }
}

export function createMetapersonFailure(metaperson, response) {
  let errors = processErrorResponse(response);

  return {
    type: CREATE_METAPERSON_FAILURE,
    payload: {
      metaperson: metaperson,
      errors: errors
    }
  }
}

export function fetchMetapersons() {
  return function (dispatch) {
    dispatch({
      type: FETCH_METAPERSONS,
      payload: {}
    });

    axios.get(`${API_ROOT}/metapersons`)
      .then((response) => {
        dispatch(fetchMetapersonsSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchMetapersonsFailure(response));
      });
  }
}

export function fetchMetapersonsSuccess(metapersons) {
  return {
    type: FETCH_METAPERSONS_SUCCESS,
    payload: metapersons
  }
}

export function fetchMetapersonsFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_METAPERSONS_FAILURE,
    payload: errors
  }
}

export function fetchMetaperson(metapersonId) {
  return function (dispatch) {
    dispatch({
      type: FETCH_METAPERSON,
      payload: {
        metapersonId: metapersonId
      }
    });

    axios.get(`${API_ROOT}/metapersons/${metapersonId}`)
      .then((response) => {
        dispatch(fetchMetapersonSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchMetapersonFailure(response));
      });
  }
}

export function fetchMetapersonSuccess(metaperson) {
  return {
    type: FETCH_METAPERSON_SUCCESS,
    payload: {
      metaperson: metaperson
    }
  }
}

export function fetchMetapersonFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_METAPERSON_FAILURE,
    payload: errors
  }
}

export function saveMetaperson(metaperson) {
  return {
    type: SAVE_METAPERSON,
    payload: {
      metaperson
    }
  }
}

export function saveMetapersonSuccess(metaperson) {
  return {
    type: SAVE_METAPERSON_SUCCESS,
    payload: {
      metaperson: metaperson
    }
  }
}

export function saveMetapersonFailure(metaperson, errors) {
  return {
    type: SAVE_METAPERSON_FAILURE,
    payload: {
      metaperson: metaperson,
      errors: errors
    }
  }
}

export function saveMetapersonWithRelations(metaperson, contacts, dates) {
  return function (dispatch) {
    dispatch({
      type: SAVE_METAPERSON_WITH_RELATIONS,
      payload: {
        metaperson,
        contacts,
        dates,
      }
    });

    axios.put(`${API_ROOT}/metapersons/${metaperson.id}`, {
      metaperson,
      contacts,
      dates
    })
      .then((response) => {
        let dataMetaperson = response.data.result.data.metaperson;
        let dataContacts = response.data.result.data.contacts;
        let dataDates = response.data.result.data.dates;

        dispatch(saveMetapersonWithRelationsSuccess(dataMetaperson, dataContacts, dataDates));
      })
      .catch((response) => {
        let errorMetaperson = response.data.result.errors.metaperson || {};
        let errorContacts    = response.data.result.errors.contacts || [];
        let errorDates       = response.data.result.errors.dates || [];

        dispatch(saveMetapersonWithRelationsFailure(metaperson, contacts, dates, {metaperson: errorMetaperson, contacts: errorContacts, dates: errorDates}));

        // for (let [key, contactResponse] of objectEntries(errorContacts)) {
        //   if (key == 'length') continue;
        //   dispatch(saveMetapersonContactFailure(contacts[key], contactResponse));
        // }
      });
  }
}

export function saveMetapersonWithRelationsSuccess(metaperson, contacts, dates) {
  return {
    type: SAVE_METAPERSON_WITH_RELATIONS_SUCCESS,
    payload: {
      metaperson,
      contacts,
      dates,
    }
  }
}

export function saveMetapersonWithRelationsFailure(metaperson, contacts, dates, errors) {
  return {
    type: SAVE_METAPERSON_WITH_RELATIONS_FAILURE,
    payload: {
      metaperson,
      contacts,
      dates,
      errors
    }
  }
}

export function deleteMetaperson(metaperson) {
  return function (dispatch) {
    dispatch({
      type: DELETE_METAPERSON,
      payload: {
        metaperson: metaperson
      }
    });

    axios.delete(`${API_ROOT}/metapersons/${metaperson.id}`)
      .then(() => {
        dispatch(deleteMetapersonSuccess(metaperson))
      })
      .catch((response) => {
        dispatch(deleteMetapersonFailure(metaperson, response));
      })
  };
}

export function deleteMetapersonSuccess(metaperson) {
  return {
    type: DELETE_METAPERSON_SUCCESS,
    payload: {
      metaperson: metaperson
    }
  }
}

export function deleteMetapersonFailure(metaperson, response) {
  let errors = processErrorResponse(response);

  return {
    type: DELETE_METAPERSON_FAILURE,
    payload: {
      metaperson: metaperson,
      errors: errors
    }
  }
}