import * as axios from 'axios';
import {
  INIT_NEW_METAPERSON_CONTACT,

  EDIT_METAPERSON_CONTACT_BY_KEY,

  // create
  CREATE_METAPERSON_CONTACT,
  CREATE_METAPERSON_CONTACT_SUCCESS,
  CREATE_METAPERSON_CONTACT_FAILURE,

  // read
  FETCH_METAPERSON_CONTACTS,
  FETCH_METAPERSON_CONTACTS_SUCCESS,
  FETCH_METAPERSON_CONTACTS_FAILURE,

  // update
  SAVE_METAPERSON_CONTACT,
  SAVE_METAPERSON_CONTACT_SUCCESS,
  SAVE_METAPERSON_CONTACT_FAILURE,

  // delete
  DELETE_METAPERSON_CONTACT_BY_KEY,
  DELETE_METAPERSON_CONTACT,
  DELETE_METAPERSON_CONTACT_SUCCESS,
  DELETE_METAPERSON_CONTACT_FAILURE,
} from '../../constants/contacts';
import processErrorResponse from '../../utils/processErrorResponse';

export function initNewMetapersonContact(contact) {
  return {
    type: INIT_NEW_METAPERSON_CONTACT,
    payload: {
      contact,
    }
  }
}

export function editMetapersonContactByKey(key, contact) {
  return {
    type: EDIT_METAPERSON_CONTACT_BY_KEY,
    payload: {
      key,
      contact,
    }
  }
}

export function deleteMetapersonContactByKey(key, contact) {
  return function (dispatch) {
    if (!contact.id) {
      dispatch({
        type: DELETE_METAPERSON_CONTACT_BY_KEY,
        payload: {
          key,
        }
      });

      return;
    }

    dispatch(deleteMetapersonContact(contact));
  };
}

export function createMetapersonContact(metapersonId, contact) {
  return function (dispatch) {
    dispatch({
      type: CREATE_METAPERSON_CONTACT,
      payload: {
        metapersonId: metapersonId,
        contact: contact,
      }
    });

    axios.post(`${API_ROOT}/metapersons/${metapersonId}/contacts`, {contact: contact})
      .then((response) => {
        dispatch(createMetapersonContactSuccess(metapersonId, response.data.result.data));
      })
      .catch((response) => {
        dispatch(createMetapersonContactFailure(metapersonId, contact, response));
      });
  }
}

export function createMetapersonContactSuccess(metapersonId, contact) {
  return {
    type: CREATE_METAPERSON_CONTACT_SUCCESS,
    payload: {
      metapersonId: metapersonId,
      contact: contact
    }
  }
}

export function createMetapersonContactFailure(metapersonId, contact, response) {
  let errors = processErrorResponse(response);

  return {
    type: CREATE_METAPERSON_CONTACT_FAILURE,
    payload: {
      metapersonId: metapersonId,
      contact: contact,
      errors: errors
    }
  }
}

export function fetchMetapersonContacts(metapersonId) {
  return function (dispatch) {
    dispatch({
      type: FETCH_METAPERSON_CONTACTS,
      payload: {
        metapersonId: metapersonId
      }
    });

    axios.get(`${API_ROOT}/metapersons/${metapersonId}/contacts`)
      .then((response) => {
        dispatch(fetchMetapersonContactsSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchMetapersonContactsFailure(response));
      });
  }
}

export function fetchMetapersonContactsSuccess(contacts) {
  return {
    type: FETCH_METAPERSON_CONTACTS_SUCCESS,
    payload: {
      contacts: contacts
    }
  }
}

export function fetchMetapersonContactsFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_METAPERSON_CONTACTS_FAILURE,
    payload: {
      errors: errors
    }
  }
}

export function saveMetapersonContact(contact, updateStateOnly = true) {
  return function (dispatch) {
    let metapersonId = contact.person_id;

    dispatch({
      type: SAVE_METAPERSON_CONTACT,
      payload: {
        metapersonId: metapersonId,
        contact: contact,
      }
    });

    if (updateStateOnly) {
      return;
    }

    axios.put(`${API_ROOT}/metapersons/${metapersonId}/contacts/${contact.id}`, contact)
      .then((response) => {
        dispatch(saveMetapersonContactSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(saveMetapersonContactFailure(contact, response));
      });
  }
}

export function saveMetapersonContactSuccess(contact) {
  let metapersonId = contact.person_id;

  return {
    type: SAVE_METAPERSON_CONTACT_SUCCESS,
    payload: {
      metapersonId: metapersonId,
      contact: contact
    }
  }
}

export function saveMetapersonContactFailure(contact, errors) {
  let metapersonId = contact.person_id;

  return {
    type: SAVE_METAPERSON_CONTACT_FAILURE,
    payload: {
      metapersonId: metapersonId,
      contact: contact,
      errors: errors
    }
  }
}

export function deleteMetapersonContact(contact) {
  return function (dispatch) {
    dispatch({
      type: DELETE_METAPERSON_CONTACT,
      payload: {
        contact,
      }
    });

    axios.delete(`${API_ROOT}/metapersons/${contact.person_id}/contacts/${contact.id}`)
      .then(() => {
        dispatch(deleteMetapersonContactSuccess(contact))
      })
      .catch((response) => {
        dispatch(deleteMetapersonContactFailure(contact, response));
      })
  };
}

export function deleteMetapersonContactSuccess(contact) {
  return {
    type: DELETE_METAPERSON_CONTACT_SUCCESS,
    payload: {
      contact,
    }
  }
}

export function deleteMetapersonContactFailure(contact, response) {
  let errors = processErrorResponse(response);

  return {
    type: DELETE_METAPERSON_CONTACT_FAILURE,
    payload: {
      contact,
      errors,
    }
  }
}