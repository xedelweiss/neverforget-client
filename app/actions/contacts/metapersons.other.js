import * as axios from 'axios';
import {
  INIT_NEW_METAPERSON,

  APPLY_METAPERSONS_FILTER,
  RESET_METAPERSONS_FILTER,

  SELECT_METAPERSON,
  UNSELECT_METAPERSON,

  START_EDIT_ACTIVE_METAPERSON,
  CANCEL_EDIT_ACTIVE_METAPERSON,
  COMMIT_EDIT_ACTIVE_METAPERSON,

  MERGE_METAPERSONS,
  MERGE_METAPERSONS_SUCCESS,
  MERGE_METAPERSONS_FAILURE,
} from '../../constants/contacts';
import processErrorResponse from '../../utils/processErrorResponse';

export function applyMetapersonsFilter(filter) {
  return {
    type: APPLY_METAPERSONS_FILTER,
    payload: filter
  }
}

export function resetMetapersonsFilter() {
  return {
    type: RESET_METAPERSONS_FILTER,
    payload: null
  }
}

export function initNewMetaperson() {
  return {
    type: INIT_NEW_METAPERSON
  }
}

export function selectMetaperson(metaperson) {
  return {
    type: SELECT_METAPERSON,
    payload: {
      metaperson: metaperson
    }
  }
}

export function unselectMetaperson(metaperson) {
  return {
    type: UNSELECT_METAPERSON,
    payload: {
      metaperson: metaperson
    }
  }
}

export function mergeMetapersons(metapersons) {
  return function (dispatch) {
    dispatch({
      type: MERGE_METAPERSONS,
      payload: {
        metapersons: metapersons
      }
    });

    axios.post(`${API_ROOT}/metapersons/merge`, {
      metapersons: metapersons.map(metaperson => metaperson.id)
    })
      .then((response) => {
        dispatch(mergeMetapersonsSuccess(metapersons, response.data.result.data));
      })
      .catch((response) => {
        dispatch(mergeMetapersonsFailure(metapersons, response));
      });
  }
}

export function mergeMetapersonsSuccess(originalMetapersons, metaperson) {
  return {
    type: MERGE_METAPERSONS_SUCCESS,
    payload: {
      originalMetapersons: originalMetapersons,
      metaperson: metaperson
    }
  }
}

export function mergeMetapersonsFailure(originalMetapersons, response) {
  let errors = processErrorResponse(response);

  return {
    type: MERGE_METAPERSONS_FAILURE,
    payload: {
      originalMetapersons: originalMetapersons,
      errors: errors
    }
  }
}

export function startEditActiveMetaperson() {
  return {
    type: START_EDIT_ACTIVE_METAPERSON,
    payload: {}
  }
}

export function cancelEditActiveMetaperson() {
  return {
    type: CANCEL_EDIT_ACTIVE_METAPERSON,
    payload: {}
  }
}

export function commitEditActiveMetaperson() {
  return {
    type: COMMIT_EDIT_ACTIVE_METAPERSON,
    payload: {}
  }
}