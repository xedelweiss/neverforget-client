import * as axios from 'axios';
import * as constants from '../constants/groups';
import {saveGroupContact, saveGroupContactSuccess, saveGroupContactFailure} from './contacts/contacts';
import {saveGroupDate, saveGroupDateSuccess, saveGroupDateFailure} from './contacts/dates';
import processErrorResponse from '../utils/processErrorResponse';

export function createGroup(group) {
  return function (dispatch) {
    dispatch({
      type: constants.CREATE_GROUP,
      payload: {
        group: group,
      }
    });

    axios.post(`${API_ROOT}/groups`, group)
      .then((response) => {
        dispatch(createGroupSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(createGroupFailure(group, response));
      });
  }
}

export function createGroupSuccess(group) {
  return {
    type: constants.CREATE_GROUP_SUCCESS,
    payload: {
      group: group
    }
  }
}

export function createGroupFailure(group, response) {
  let errors = processErrorResponse(response);

  return {
    type: constants.CREATE_GROUP_FAILURE,
    payload: {
      group: group,
      errors: errors
    }
  }
}

export function fetchGroups() {
  return function (dispatch) {
    dispatch({
      type: constants.FETCH_GROUPS,
      payload: {}
    });

    axios.get(`${API_ROOT}/groups`)
      .then((response) => {
        dispatch(fetchGroupsSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchGroupsFailure(response));
      });
  }
}

export function fetchGroupsSuccess(groups) {
  return {
    type: constants.FETCH_GROUPS_SUCCESS,
    payload: {
      groups,
    }
  }
}

export function fetchGroupsFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: constants.FETCH_GROUPS_FAILURE,
    payload: errors
  }
}

export function fetchGroup(groupId) {
  return function (dispatch) {
    dispatch({
      type: constants.FETCH_GROUP,
      payload: {
        groupId: groupId
      }
    });

    axios.get(`${API_ROOT}/groups/${groupId}`)
      .then((response) => {
        dispatch(fetchGroupSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchGroupFailure(response));
      });
  }
}

export function fetchGroupSuccess(group) {
  return {
    type: constants.FETCH_GROUP_SUCCESS,
    payload: {
      group: group
    }
  }
}

export function fetchGroupFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: constants.FETCH_GROUP_FAILURE,
    payload: errors
  }
}

export function saveGroup(group) {
  return {
    type: constants.SAVE_GROUP,
    payload: {
      group
    }
  }
}

export function saveGroupSuccess(group) {
  return {
    type: constants.SAVE_GROUP_SUCCESS,
    payload: {
      group: group
    }
  }
}

export function saveGroupFailure(group, errors) {
  return {
    type: constants.SAVE_GROUP_FAILURE,
    payload: {
      group: group,
      errors: errors
    }
  }
}

export function deleteGroup(group) {
  return function (dispatch) {
    dispatch({
      type: constants.DELETE_GROUP,
      payload: {
        group: group
      }
    });

    axios.delete(`${API_ROOT}/groups/${group.id}`)
      .then(() => {
        dispatch(deleteGroupSuccess(group))
      })
      .catch((response) => {
        dispatch(deleteGroupFailure(group, response));
      })
  };
}

export function deleteGroupSuccess(group) {
  return {
    type: constants.DELETE_GROUP_SUCCESS,
    payload: {
      group: group
    }
  }
}

export function deleteGroupFailure(group, response) {
  let errors = processErrorResponse(response);

  return {
    type: constants.DELETE_GROUP_FAILURE,
    payload: {
      group: group,
      errors: errors
    }
  }
}