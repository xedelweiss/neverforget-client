import * as axios from 'axios';
import * as types from '../constants/socials';
import processErrorResponse from '../utils/processErrorResponse';
import {base64_encode} from '../utils/base64';

export function startSocialAuthorizationProcess(attached, provider, returnUrl, jwt) {
  return (dispatch) => {
    let newLocation = `${API_ROOT}/socials/${provider}/attach?returnUrl=${encodeURIComponent(returnUrl)}`;

    if (jwt) {
      newLocation += `&jwt=${jwt}`;
    }

    if (attached) {
      attached = base64_encode(JSON.stringify(attached));
      newLocation += '&socials=' + encodeURIComponent(attached);
    }

    window.location = newLocation;
  }
}

function fetchNewSocialAuthorizationCode(provider, redirectUrl) {
  return (dispatch) => {
    dispatch(initNewSocial(provider));
    dispatch(getAuthorizationRequestUrl(provider, redirectUrl));
  }
}

function initNewSocial(provider) {
  return {
    type: types.INIT_NEW_SOCIAL,
    payload: {
      provider,
    }
  };
}

function getAuthorizationRequestUrl(provider, redirectUrl) {
  return (dispatch) => {
    dispatch({
      type: types.FETCH_AUTHORIZATION_REQUEST_URL,
      payload: {}
    });

    axios
      .get(`${API_ROOT}/socials/get-auth-url`, {
        params: {
          provider,
          redirectUrl,
        }
      })
      .then((response) => {
        dispatch(getAuthorizationRequestUrlSuccess(response.data.result.data.url));
      })
      .catch((response) => {
        dispatch(getAuthorizationRequestUrlFailure(response));
      })
  }
}

function getAuthorizationRequestUrlSuccess(url) {
  return (dispatch) => {
    // mark fetch completed
    dispatch({
      type: types.FETCH_AUTHORIZATION_REQUEST_URL_SUCCESS,
      payload: {
        url,
      }
    });

    // @fixme maybe it's not the best idea
    window.location = url;
  }
}

function getAuthorizationRequestUrlFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: types.FETCH_AUTHORIZATION_REQUEST_URL_FAILURE,
    payload: {
      errors,
    }
  }
}

export function initSocialsInfo(socials) {
  return {
    type: types.INIT_SOCIALS_INFO,
    payload: {
      socials
    }
  }
}

export function fetchNewSocialUserInfo(provider, authorizationCode, redirectUrl) {
  return (dispatch) => {
    dispatch(resumeAuthorizationFlow(provider, authorizationCode));
    dispatch(fetchUserInfoByAuthorizationCode(provider, authorizationCode, redirectUrl));
  }
}

function resumeAuthorizationFlow(provider, code) {
  return {
    type: types.RESUME_AUTHORIZATION_FLOW,
    payload: {
      provider,
      code,
    }
  };
}

function fetchUserInfoByAuthorizationCode(provider, code, redirectUrl) {
  return (dispatch) => {
    dispatch({
      type: types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE,
      payload: {
        provider,
        code,
        redirectUrl
      }
    });

    axios
      .get(`${API_ROOT}/socials/get-user-info`, {
        params: {
          provider,
          code,
          redirectUrl,
        }
      })
      .then((response) => {
        dispatch(fetchUserInfoByAuthorizationCodeSuccess(response.data.result.data.isNew, response.data.result.data.userInfo));
      })
      .catch((response) => {
        dispatch(fetchUserInfoByAuthorizationCodeFailure(response));
      })
  }
}

function fetchUserInfoByAuthorizationCodeSuccess(isNew, userInfo) {
  return {
    type: types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE_SUCCESS,
    payload: {
      isNew,
      userInfo,
    }
  }
}

function fetchUserInfoByAuthorizationCodeFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: types.FETCH_USER_INFO_BY_AUTHORIZATION_CODE_FAILURE,
    payload: {
      errors,
    }
  }
}

export function fetchSocials() {
  return (dispatch) => {
    dispatch({
      type: types.FETCH_SOCIALS,
      payload: {}
    });

    axios
      .get(`${API_ROOT}/socials`)
      .then((response) => {
        dispatch(fetchSocialsSuccess(response.data.result.data));
      })
      .catch((response) => {
        dispatch(fetchSocialsFailure(response));
      })
  }
}

function fetchSocialsSuccess(socials) {
  return {
    type: types.FETCH_SOCIALS_SUCCESS,
    payload: {
      socials,
    }
  }
}

function fetchSocialsFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: types.FETCH_SOCIALS_FAILURE,
    payload: {
      errors,
    }
  }
}

export function deleteSocial(social) {
  return (dispatch) => {
    dispatch({
      type: types.DELETE_SOCIAL,
      payload: {
        social
      }
    });

    axios
      .delete(`${API_ROOT}/socials/${social.id}`)
      .then((response) => {
        dispatch(deleteSocialSuccess(social));
      })
      .catch((response) => {
        dispatch(deleteSocialFailure(social, response));
      })
  }
}

function deleteSocialSuccess(social) {
  return {
    type: types.DELETE_SOCIAL_SUCCESS,
    payload: {
      social,
    }
  }
}

function deleteSocialFailure(social, response) {
  let errors = processErrorResponse(response);

  return {
    type: types.DELETE_SOCIAL_FAILURE,
    payload: {
      social,
      errors,
    }
  }
}

export function syncSocial(social) {
  return (dispatch) => {
    dispatch({
      type: types.SYNC_SOCIAL,
      payload: {
        social
      }
    });

    axios
      .post(`${API_ROOT}/socials/${social.id}/sync`)
      .then((response) => {
        dispatch(syncSocialSuccess(social));
      })
      .catch((response) => {
        dispatch(syncSocialFailure(social, response));
      })
  }
}

function syncSocialSuccess(social) {
  return {
    type: types.SYNC_SOCIAL_SUCCESS,
    payload: {
      social,
    }
  }
}

function syncSocialFailure(social, response) {
  let errors = processErrorResponse(response);

  return {
    type: types.SYNC_SOCIAL_FAILURE,
    payload: {
      social,
      errors,
    }
  }
}
