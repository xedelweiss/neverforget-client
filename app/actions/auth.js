import * as axios from 'axios';
import {
  SET_TOKEN,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  GET_CURRENT_USER_SUCCESS,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  NEW_PASSWORD_REQUEST,
  NEW_PASSWORD_SUCCESS,
  NEW_PASSWORD_FAILURE,
  RESET_PASSWORD_PAGE,
  NEW_PASSWORD_PAGE
} from "../constants/auth";
import {log} from "../utils/debug";


export function setToken(token) {
  return {
    type: SET_TOKEN,
    payload: token
  }
}

export function logout() {
  return setToken(null); // @todo is it ok?
}

export function login(email, password, remember) {
  return function (dispatch) {
    dispatch({
      type: LOGIN_REQUEST,
      payload: {}
    });

    const loginURL = API_ROOT + '/auth/login';

    axios
      .post(loginURL, {
        email: email,
        password: password,
        remember: remember
      })
      .then((response) => {
        dispatch(loginSuccess(response.data.result.data));
        dispatch(setToken(response.data.result.data.token));
      })
      .catch((response) => {
        let errors = typeof response.status == 'undefined'
          ? {'general': [response.message]}
          : response.data.result.errors;

        dispatch(loginFailure(errors));
        dispatch(setToken(null));
    })
  }
}

export function register(first_name, email, password, socialTokens) {
  return function (dispatch) {
    dispatch({
      type: REGISTER_REQUEST,
      payload: {}
    });

    axios.post(API_ROOT + '/auth/register', {
      first_name: first_name,
      email: email,
      password: password,
      social_tokens: socialTokens,
    }).then((response) => {
      dispatch(registerSuccess(response.data.result.data));
      dispatch(setToken(response.data.result.data.token));
    }).catch((response) => {
      let errors = typeof response.status == 'undefined'
        ? {'general': [response.message]}
        : response.data.result.errors;

      dispatch(registerFailure(errors));
      dispatch(setToken(null));
    })
  }
}

export function loginSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    payload: data
  }
}

export function loginFailure(errors) {
  return {
    type: LOGIN_FAILURE,
    payload: errors
  }
}

export function registerSuccess(data) {
  return {
    type: REGISTER_SUCCESS,
    payload: data
  }
}

export function registerFailure(errors) {
  return {
    type: REGISTER_FAILURE,
    payload: errors
  }
}

export function resetPasswordRequest(resetEmail) {
  return function (dispatch) {
    dispatch({
      type:     RESET_PASSWORD_REQUEST,
      payload:  {}
    });

    axios.post(`${API_ROOT}/password/reset-link`, {
      resetEmail
    })
      .then((response) => {
        let responseInfo = response.data.result.data;

        if ( responseInfo.success === true ) {
          dispatch(resetPasswordSuccess());
        } else {
          dispatch(resetPasswordFailure(responseInfo));
        }
      })
      .catch((response) => {
        dispatch(resetPasswordFailure(response));
      });
  }
}

export function resetPasswordSuccess() {
  return {
    type:     RESET_PASSWORD_SUCCESS,
    payload:  {}
  };
}

export function resetPasswordFailure(response) {
  if ( response.data.result.errors ) {
    return {
      type:   RESET_PASSWORD_FAILURE,
      payload:  {
        errors: response.data.result.errors
      }
    }
  }
}

export function newPasswordRequest(token, newPassword) {
  return function (dispatch) {
    dispatch({
      type:     NEW_PASSWORD_REQUEST,
      payload:  {}
    });

    axios.put(`${API_ROOT}/password/set-new-password`, {
      token,
      password: newPassword
    })
      .then((response) => {
        let responseInfo = response.data.result.data;

        if ( responseInfo.success === true ) {
          dispatch(newPasswordSuccess());
        } else {
          dispatch(newPasswordFailure(responseInfo));
        }
      })
      .catch((response) => {
        let responseInfo = response.data.result.data;
        dispatch(newPasswordFailure(responseInfo));
      });
  }
}

export function newPasswordSuccess() {
  return {
    type:     NEW_PASSWORD_SUCCESS,
    payload:  {}
  };
}

export function newPasswordFailure() {
  return {
    type:     NEW_PASSWORD_FAILURE,
    payload:  {}
  };
}

export function resetPasswordPage() {
  return {
    type:     RESET_PASSWORD_PAGE,
    payload:  {}
  };
}

export function changePasswordPage() {
  return {
    type:     NEW_PASSWORD_PAGE,
    payload:  {}
  };
}