import * as axios from 'axios';
import {
  FETCH_EVENTS,
  FETCH_EVENTS_SUCCESS,
  FETCH_EVENTS_FAILURE,
  CHANGE_EVENT_STATUS,
  CHANGE_EVENT_STATUS_SUCCESS,
  CHANGE_EVENT_STATUS_FAILURE,
  SAVE_EVENT,
  SAVE_EVENT_SUCCESS,
  SAVE_EVENT_FAILURE,
  DELETE_EVENT,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_FAILURE,
  CREATE_NEW_EVENT,
  CREATE_NEW_EVENT_SUCCESS,
  CREATE_NEW_EVENT_FAILURE,
  SET_ACTIVE_DATE
} from "../constants/events";
import {log} from "../utils/debug";

function processErrorResponse(response) {
  let errors = [];
  
  if ( response.data ) {
    errors = [response.data.result.errors];
  } else {
    errors = [response.message];
  }

  return errors;
}

export function fetchEvents(activeDate, period) {
  return function (dispatch) {
    dispatch({
      type: FETCH_EVENTS,
      payload: {}
    });

    axios.get(`${API_ROOT}/events`, {
      params: {
        period,
        activeDate
      }
    }).then((response) => {
      dispatch(fetchEventsSuccess(response.data.result.data));
    }).catch((response) => {
      log("Error in fetchEvents action");
      dispatch(fetchEventsFailure(response));
    });
  }
}

export function fetchEventsSuccess(events) {
  return {
    type: FETCH_EVENTS_SUCCESS,
    payload: events
  }
}

export function fetchEventsFailure(response) {
  let errors = processErrorResponse(response);

  return {
    type: FETCH_EVENTS_FAILURE,
    payload: errors
  }
}

export function changeEventStatus(event) {
  return function (dispatch) {
    dispatch({
      type: CHANGE_EVENT_STATUS,
      payload: {
        event:    event
      }
    });

    axios.put(API_ROOT + `/events/${event.id}?field=status`, {
      is_active: event.is_active
    }).then((response) => {
      dispatch(changeEventStatusSuccess(event, response));
    }).catch((response) => {
      dispatch(changeEventStatusFailure(event, response));
    })
  }
}

export function changeEventStatusSuccess(event, response) {
  return {
    type: CHANGE_EVENT_STATUS_SUCCESS,
    payload: {
      event:    event
    }
  }
}

export function changeEventStatusFailure(event, response) {
  let errors = processErrorResponse(response);

  return {
    type: CHANGE_EVENT_STATUS_FAILURE,
    payload: {
      event:  event,
      errors: errors
    }
  }
}

export function createNewEvent(event, period, activeDate) {
  return function(dispatch) {
    dispatch({
      type:   CREATE_NEW_EVENT,
      payload: {

      }
    });

    axios.post(`${API_ROOT}/events`, event)
      .then((response) => {
        dispatch(createNewEventSuccess(period, activeDate));
      })
      .catch((response) => {
        dispatch(createNewEventFailure(event, response));
      });
  };
}

export function createNewEventSuccess(period, activeDate) {
  return function (dispatch) {
    dispatch({
      type:     CREATE_NEW_EVENT_SUCCESS,
      payload: {
      }
    });

    dispatch(fetchEvents(activeDate, period));
  };
}

export function createNewEventFailure(event, response) {
  let errors = processErrorResponse(response);

  return {
    type:     CREATE_NEW_EVENT_FAILURE,
    payload: {
      event:  event,
      errors: errors
    }
  };
}

export function saveEvent(event) {
  return function (dispatch) {
    dispatch({
      type: SAVE_EVENT,
      payload: {
        event: event
      }
    });

    axios.put(API_ROOT + `/events/${event.id}`,
      event
    ).then((response) => {
      dispatch(saveEventSuccess(event));
    }).catch((response) => {
      dispatch(saveEventFailure(event, response));
    })
  }
}

export function saveEventSuccess(event) {
  return {
    type: SAVE_EVENT_SUCCESS,
    payload: {
      event: event
    }
  }
}

export function saveEventFailure(event, response) {
  let errors = processErrorResponse(response);

  return {
    type: SAVE_EVENT_FAILURE,
    payload: {
      event:  event,
      errors: errors
    }
  }
}

export function deleteEvent(event) {
  return function(dispatch) {
    dispatch({
      type: DELETE_EVENT,
      payload: {
        event: event
      }
    });

    axios.delete(`${API_ROOT}/events/${event.id}`)
      .then(() => {
        dispatch(deleteEventSuccess(event));
      })
      .catch((response) => {
        dispatch(deleteEventFailure(event, response));
      });
  };
}

export function deleteEventSuccess(event) {
  return function(dispatch) {
    dispatch({
      type: DELETE_EVENT_SUCCESS,
      payload: {
        event
      }
    });
  }
}

export function deleteEventFailure(event, response) {
  let errors = processErrorResponse(response);

  return {
    type:     DELETE_EVENT_FAILURE,
    payload:  {
      event:  event,
      errors: errors
    }
  }
}

export function setActiveDate(activeDate) {
  return {
    type:     SET_ACTIVE_DATE,
    payload: {
      activeDate
    }
  }
}
