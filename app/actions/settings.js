import * as axios from 'axios';
import {log} from "../utils/debug";
import {
  USER_UPDATE,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE,
  PASSWORD_UPDATE,
  PASSWORD_UPDATE_SUCCESS,
  PASSWORD_UPDATE_FAILURE
} from "../constants/settings";
import {
  setToken
} from "./auth";


export function updateUser(user) {
  return function(dispatch) {
    dispatch({
      type:     USER_UPDATE,
      payload:  {}
    });

    axios
      .put(`${API_ROOT}/users/${user.id}`, {user})
      .then((response) => {
        dispatch(updateUserSuccess(response.data.result.data));
      })
      .catch((response) => {
        // log(response);
      });
  };
}

export function updateUserSuccess(token) {
  return function(dispatch) {
    dispatch({
      type:     USER_UPDATE_SUCCESS,
      payload:  {}
    });

    dispatch(setToken(token));
  };



}

export function updateUserFailure() {
  return {
    type:     USER_UPDATE_FAILURE,
    payload:  {}
  }
}

export function updatePassword(user, password, newPassword) {
  return function (dispatch) {
    dispatch({
      type:   PASSWORD_UPDATE,
      payload: {
        passwordErrors: [],
        newPasswordErrors: [],
        passwordUpdated: false
      }
    });

    axios
      .put(`${API_ROOT}/users/${user.id}/password`, {password, newPassword})
      .then((response) => {
        dispatch(updatePasswordSuccess());
      })
      .catch((response) => {
        dispatch(updatePasswordFailure(response));
      });
  }
}

export function updatePasswordSuccess() {
  return {
    type:     PASSWORD_UPDATE_SUCCESS,
    payload:  {
      passwordErrors: [],
      newPasswordErrors: [],
      passwordUpdated:  true
    }
  };
}

export function updatePasswordFailure(response) {
  let knownErrors = response.data.result.errors ? response.data.result.errors : null;
  let passwordErrors = [];
  let newPasswordErrors = [];

  if ( knownErrors ) {
    if ( knownErrors.password ) {
      passwordErrors = knownErrors.password;
    }

    if ( knownErrors.newPassword ) {
      newPasswordErrors = knownErrors.newPassword;
    }
  }

  return {
    type:   PASSWORD_UPDATE_FAILURE,
    payload: {
      passwordErrors,
      newPasswordErrors,
      passwordUpdated: false
    }
  };
}