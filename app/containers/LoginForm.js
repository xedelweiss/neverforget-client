import React from 'react';
import {connect} from 'react-redux';
import update from 'react-addons-update';
import {LinkContainer} from 'react-router-bootstrap';
import {FormGroup, Form, ControlLabel, Checkbox, FormControl, Button, Col} from 'react-bootstrap';
import LoadingGlyphicon from '../components/LoadingGlyphicon';
import {login} from '../actions/auth';
import ValidationErrors from '../components/ValidationErrors';
import SocialAddButton from './Social/AddButton';

const LoginForm = React.createClass({
  contextTypes: {
    router: React.PropTypes.object
  },
  getInitialState() {
    return {
      email: '',
      password: '',
      remember: true
    }
  },
  onAuthCompleteRedirect(){
    this.context.router.push('/dashboard')
  },
  componentWillReceiveProps(nextProps){
    if (nextProps.token) {
      this.onAuthCompleteRedirect();
    }
  },
  onSubmit(e) {
    e.preventDefault();

    this.props.login(
      this.state.email,
      this.state.password,
      this.state.remember
    );
  },
  onEmailChange(e) {
    this.setState(update(this.state, {
      email: {$set: e.target.value}
    }));
  },
  onPasswordChange(e) {
    this.setState(update(this.state, {
      password: {$set: e.target.value}
    }));
  },
  onRememberChange(e) {
    this.setState(update(this.state, {
      remember: {$set: e.target.checked}
    }));
  },
  render: function () {
    let emailErrors = this.props.errors ? this.props.errors.email || [] : [];
    let passwordErrors = this.props.errors ? this.props.errors.password || [] : [];
    let generalErrors = this.props.errors ? this.props.errors.general || [] : [];
    let loading = this.props.loading;

    if (this.props.token) {
      this.onAuthCompleteRedirect();
    }

    return (
      <Form horizontal role="form" method="POST" onSubmit={this.onSubmit}>
        <FormGroup validationState={emailErrors.length > 0 ? 'error' : null}>
          <Col componentClass={ControlLabel} xs={12} md={4}>E-Mail Address</Col>
          <Col xs={12} md={6}>
            <FormControl type="email" name="email" value={this.state.email}
                         onChange={this.onEmailChange}/>
            <ValidationErrors errors={emailErrors}/>
          </Col>
        </FormGroup>

        <FormGroup validationState={passwordErrors.length > 0 ? 'error' : null}>
          <Col componentClass={ControlLabel} xs={12} md={4}>Password</Col>
          <Col xs={12} md={6}>
            <FormControl type="password" name="password" value={this.state.password}
                         onChange={this.onPasswordChange}/>
            <ValidationErrors errors={passwordErrors}/>
          </Col>
        </FormGroup>

        <FormGroup validationState={generalErrors.length > 0 ? 'error' : null}>
          <Col xs={12} md={6} mdOffset={4}>
            <ValidationErrors errors={generalErrors}/>
          </Col>
        </FormGroup>

        {
          false &&
          <FormGroup>
            <Col md={8} mdOffset={4}>
              <Checkbox name="remember" checked={this.state.remember}
                        onChange={this.onRememberChange}>
                Remember Me
              </Checkbox>
            </Col>
          </FormGroup>
        }

        <FormGroup>
          <Col xs={12} md={8} mdOffset={4}>
            <Button type="submit" bsStyle="success" disabled={loading}>
              <LoadingGlyphicon glyph="log-in" loading={loading}/> Login
            </Button>

            <LinkContainer to="/password/reset">
              <Button bsStyle="link">Forgot Your Password?</Button>
            </LinkContainer>
          </Col>
        </FormGroup>

        <hr />

        <FormGroup>
          <Col md={6} mdOffset={4}>
            <SocialAddButton provider="vkontakte" title="VK" />
            <SocialAddButton provider="facebook" title="Facebook" />
            <SocialAddButton provider="google" title="Google" />
          </Col>
        </FormGroup>
      </Form>
    )
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  return {
    token: auth.token,
    loading: auth.login.loading,
    errors: auth.login.errors
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password, remember) => {
      dispatch(login(email, password, remember));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);