import React from 'react';
import {connect} from 'react-redux';
import * as types from '../constants/socials'

import * as socialsAction from '../actions/socials';
// import VkontakteLogin from './LoginSocial/VkontakteLogin';

const LoginSocialForm = React.createClass({
  propTypes: {
    provider: React.PropTypes.string.isRequired,
  },

  getDefaultProps(){
    return {
      code: null,
    }
  },

  componentWillMount() {
    let provider = this.props.provider;
    let code = this.props.code;

    if (!code) {
      this.props.getAuthorizationCode(provider);
    } else {
      this.props.getUserInfo(provider, code);
    }
  },

  render() {
    if (this.props.errors) {
      return <div>
        {this.props.errors.constructor === Array ? this.props.errors.join(' ') : this.props.errors}
      </div>
    }

    if (this.props.step == types.SOCIAL_AUTH_STEP_HAVE_USER_INFO) {
      return <pre>
        {JSON.stringify(this.props.userInfo)}
      </pre>
    }

    return <h4 className="text-center"><LoadingGlyphicon loading={true}/> Please wait..</h4>;
  },

  // render() {
  //   switch (this.props.provider) {
  //     case 'vkontakte':
  //       return <VkontakteLogin/>;
  //     default:
  //       return <div>
  //         Sorry, we do not support <strong>{this.props.provider}</strong> social provider
  //       </div>;
  //   }
  // }
});

const mapStateToProps = (state) => {
  let {step, isLoading, errors, userInfo} = state.socials.get('new').toJS();

  return {
    step,
    isLoading,
    errors,
    userInfo
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAuthorizationCode: (provider) => {
      let redirectUrl = window.location.origin + window.location.pathname;
      dispatch(socialsAction.fetchNewSocialAuthorizationCode(provider, redirectUrl));
    },
    getUserInfo: (provider, authorizationCode) => {
      dispatch(socialsAction.fetchNewSocialUserInfo(provider, authorizationCode));
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginSocialForm);