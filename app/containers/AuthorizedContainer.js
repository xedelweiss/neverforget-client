import React from 'react';
import {connect} from 'react-redux';

const AuthorizedContainer = React.createClass({
  render() {
    let isInverted = this.props['data-invert'];

    let {isAuthorized, ...rest} = this.props;

    if (!isAuthorized && !isInverted) {
      return null;
    }

    if (isAuthorized && isInverted) {
      return null;
    }

    if (this.props.children.length > 1) {
      throw 'Only one child allowed';
    }

    return rest.children
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  return {
    isAuthorized: !!auth.token
  }
};

export default connect(
  mapStateToProps
)(AuthorizedContainer);