import React from "react";
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import moment from "moment";
import update from "react-addons-update";

import EventModal from "../../components/Event/EventModal";
import {
  createNewEvent,
} from "../../actions/events";

const NewEventContainer = React.createClass({
  getInitialState() {

    return {
      event: {
        event_name:       '',
        message_subject:  '',
        message_body:     '',
        recipient:        '',
        message_signature:'',
        datetime_to_send: moment().format("YYYY-MM-DD HH:mm:ss"),
        timezone:         'Europe/Kiev',
        is_active:        true,
        comment:          ''
      },
      showModal:  false,
      errors:     [],
      timezones:  []
    };
  },

  componentWillReceiveProps(nextProps) {
    let date = nextProps.activeDate;
    let newActiveDate = moment({year: date.year, month: date.month-1, date: date.day});

    this.setState(update(this.state, {
      event: {
        datetime_to_send: {$set: newActiveDate.format("YYYY-MM-DD HH:mm:ss")}
      },
      showModal:        {$set: nextProps.showModal},
      errors:           {$set: nextProps.errors}
    }));
  },

  openNewEventWindow() {
    this.setState({showModal: true});
  },

  closeNewEventWindow() {
    this.setState({showModal: false});
  },

  createNewEvent(event) {
    this.props.createNewEvent(event, this.props.period, this.props.activeDate);
  },

  render() {
    return (
      <div className="pull-right">
        <Button onClick={this.openNewEventWindow}>New Event</Button>
        <EventModal
          userContacts={this.props.userContacts}
          isShown={this.state.showModal}
          onHide={this.closeNewEventWindow}
          event={this.state.event}
          errors={this.state.errors}
          timezones={this.props.timezones}
          onSave={this.createNewEvent}
        />
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  let tmpEventsState = state.events.toJS();

  return {
    activeDate: tmpEventsState.activeDate,
    showModal:  tmpEventsState.newEvent.showModal,
    errors:     tmpEventsState.newEvent.errors
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createNewEvent: (event, period, activeDate) => {
      dispatch(createNewEvent(event, period, activeDate));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewEventContainer);