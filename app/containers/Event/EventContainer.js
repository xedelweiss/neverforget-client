import React, {PropTypes} from "react";
import {Row, Col, Panel, Button} from "react-bootstrap";
import {connect} from "react-redux";
import update from "react-addons-update";
import moment from "moment";
import {LinkContainer} from "react-router-bootstrap";

import EventList from "../../components/Event/EventList";
import DatePeriod from "../../components/Event/DatePeriod";
import {
  fetchEvents,
  changeEventStatus, 
  saveEvent,
  deleteEvent,
  setActiveDate
} from '../../actions/events';

const EventContainer = React.createClass({
  getInitialState() {
    let initialDate = new Date();

    let year = parseInt(this.props.location.query.year) || initialDate.getFullYear();
    let month = parseInt(this.props.location.query.month) || (initialDate.getMonth() + 1);
    let day = parseInt(this.props.location.query.day) || initialDate.getDate();

    let activeDate = { year, month, day };
    this.props.setActiveDate(activeDate);

    return {
      activeDate: activeDate,
      timezones:  []
    };
  },

  /**
   * Update state of activeDate and get new filtered events.
   *
   * @param period
   * @param sign
   */
  prevNextPeriodChange(period, sign) {
    let activeDate =
      moment({
        year: this.state.activeDate.year,
        month: this.state.activeDate.month - 1,
        day: this.state.activeDate.day
      });

    switch (sign) {
      case    "-":
        activeDate.subtract(1, `${period}s`);
        break;
      case    "+":
        activeDate.add(1, `${period}s`);
        break;
      default:
        break;
    }

    this.setState(update(this.state, {
      activeDate: {
        day:    { $set: activeDate.format("D") },
        year:   { $set: activeDate.format("YYYY") },
        month:  { $set: activeDate.format("M") }
      }
    }), () => {
      this.setNewPeriodFilter(this.props.period);
    });
    
  },

  setActiveDateToday() {
    let activeDate = moment();

    this.setState({
      activeDate: {
        day: activeDate.format("D"),
        year: activeDate.format("YYYY"),
        month: activeDate.format("M")
      }
    }, () => {
      this.setNewPeriodFilter(this.props.period)
    });
  },

  componentWillMount() {
    this.props.fetchEvents(this.state.activeDate, this.props.period);
  },

  setNewPeriodFilter(filterValue) {
    this.props.setActiveDate(this.state.activeDate);
    this.props.fetchEvents(this.state.activeDate, filterValue);
  },

  saveEvent(event) {
    this.props.saveEvent(event);
  },

  deleteEvent(event) {
    this.props.deleteEvent(event);
  },

  renderState() {
    switch (true) {
      case (this.props.loadingEvents):
        return this.renderLoading();

      case (this.props.errors.length > 0):
        return this.renderErrors();

      case (this.props.events.length == 0):
        return this.renderEmptyList();

      case (this.props.events.length > 0):
        return this.renderList();
    }
  },

  renderErrors() {
    return (
      <div>Oops! Some error occurred. Please try again later.</div>
    );
  },

  renderLoading() {
    return <div>Loading</div>
  },

  renderList() {
    return (
      <div>
        <EventList events={this.props.events}
                   saveEvent={this.saveEvent}
                   deleteEvent={this.deleteEvent}
                   timezones={this.props.timezones}
                   userContacts={this.props.userContacts}
                   changeEventStatus={this.props.changeEventStatus} />
      </div>
    );
  },

  renderEmptyList() {
    return (
      <div>
        I am empty list.
      </div>
    );
  },

  render() {
    return (
      <div>
        <Row>
          <Col md={3}>
            <Panel className="pull-left">
              <Button
                onClick={ () => { this.prevNextPeriodChange(this.props.location.query.period, "-") } }>Prev</Button>
              <Button
                onClick={ () => { this.prevNextPeriodChange(this.props.location.query.period, "+") } }>Next</Button>
              <Button onClick={ () => { this.setActiveDateToday() } }>Today</Button>
            </Panel>
          </Col>

          <Col md={5} className="active-date-container">
            <DatePeriod activeDate={this.state.activeDate} period={this.props.period}/>
          </Col>

          <Col md={4}>
            <Panel className="pull-right">
              <LinkContainer to={{ pathname: 'schedule', query: { period: 'day'} }}>
                <Button onClick={ () => {this.setNewPeriodFilter('day');} }>Daily</Button>
              </LinkContainer>
              <LinkContainer to={{ pathname: 'schedule', query: { period: 'week'} }}>
                <Button onClick={ () => {this.setNewPeriodFilter('week');} }>Weekly</Button>
              </LinkContainer>
              <LinkContainer to={{ pathname: 'schedule', query: { period: 'month'} }}>
                <Button onClick={ () => {this.setNewPeriodFilter('month');} }>Monthly</Button>
              </LinkContainer>
              <LinkContainer to={{ pathname: 'schedule', query: { period: 'year'} }}>
                <Button onClick={ () => {this.setNewPeriodFilter('year');} }>Yearly</Button>
              </LinkContainer>
            </Panel>
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            {this.renderState()}
          </Col>
        </Row>
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  let tmpState = state.events.toJS();

  return {
    events:           tmpState.events,
    loadingEvents:    tmpState.loadingEvents,
    errors:           tmpState.errors
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchEvents: (activeDate, period) => {
      dispatch(fetchEvents(activeDate, period));
    },

    changeEventStatus: (event) => {
      event.is_active = !event.is_active;
      dispatch(changeEventStatus(event));
    },
    
    saveEvent: (event) => {
      dispatch(saveEvent(event));
    },
    
    deleteEvent: (event, period, activeDate) => {
      dispatch(deleteEvent(event, period, activeDate));
    },
    
    setActiveDate: (activeDate) => {
      dispatch(setActiveDate(activeDate));
    }
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventContainer);
