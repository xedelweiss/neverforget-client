import React from 'react';
import {connect} from 'react-redux';
import jwtDecode from 'jwt-decode';

const TokenInfo = React.createClass({
    render() {
        let {field, token} = this.props;

        let tokenData = jwtDecode(token);

        let info = tokenData[field] ? tokenData[field] : null;

        return (
            <span>{info}</span>
        )
    }
});

const mapStateToProps = (state) => {
    let auth = state.auth.toJS();
    return {
        token: auth.token
    }
};

export default connect(
    mapStateToProps
)(TokenInfo);