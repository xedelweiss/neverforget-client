import React from 'react';
import {injectIntl} from 'react-intl';
import Select from 'react-select';
import "./styles/style.scss";

const LanguageSelector = React.createClass({
  getInitialState() {
    let state = {
      locales: [
        {value: 'en-us', label: 'English (US)'},
        {value: 'uk-ua', label: 'Ukrainian'}
      ]
    };

    state.currentLocale = state.locales[0];
    for(let i=0; i<state.locales.length; i++) {
      if ( state.locales[i].value === this.props.intl.locale ) {
        state.currentLocale = state.locales[i];
        break;
      }
    }

    return state;
  },

  updateCurrentLocale(locale) {

  },

  componentWillReceiveProps(nextProps) {
  },

  onLanguageSelected(item) {
    window.location = '/?locale=' + item.value + window.location.hash;
  },

  render() {

    return (
      <Select
        className="language-select"
        name="language_selector"
        clearable={false}
        value={this.state.currentLocale}
        options={this.state.locales}
        onChange={this.onLanguageSelected}
      />
    );
  }
});

export default injectIntl(LanguageSelector);