import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import {loadDefaultData} from "../actions/data/default";

const Application = React.createClass({
  saveAuthToken(authToken) {
    if (authToken) {
      axios.defaults.headers.common['Authorization'] = `Bearer ${authToken}`;
      localStorage.setItem('authToken', authToken);
      this.props.loadDefaultData();
    } else {
      delete axios.defaults.headers.common['Authorization'];
      localStorage.removeItem('authToken');
    }
  },
  componentWillMount() {
    this.saveAuthToken(this.props.authToken);
  },
  componentWillReceiveProps(nextProps){
    this.saveAuthToken(nextProps.authToken);
  },
  render: function () {
    return (
      <div>
        {this.props.children}
      </div>
    )
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  return {
    authToken: auth.token
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadDefaultData: function() {
      dispatch(loadDefaultData());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Application);