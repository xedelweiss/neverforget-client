import React from 'react';
import {connect} from 'react-redux';
import {logout} from '../actions/auth';

const LogoutContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object
  },
  onClick(event) {
    if (this.props.children.props.onClick) {
      this.props.children.props.onClick(event);
    }

    this.props.logout();
    this.context.router.push('/login');
  },
  render() {
    //react 15.2 recomentation to not propagate props to children:
    // https://facebook.github.io/react/warnings/unknown-prop.html
    const {children, isAuthorized, logout, ...rest} = this.props;

    rest.onClick = this.onClick;

    return React.cloneElement(React.Children.only(children), rest);
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  return {
    isAuthorized: !!auth.token
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      dispatch(logout());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutContainer);