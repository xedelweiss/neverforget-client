import React from 'react';
import {connect} from 'react-redux';
import {FormGroup, Form, ControlLabel, FormControl, Button, Col} from 'react-bootstrap';
import LoadingGlyphicon from '../components/LoadingGlyphicon';
import {register} from '../actions/auth';
import ValidationErrors from '../components/ValidationErrors';
import SocialAddButton from './Social/AddButton';

const RegisterForm = React.createClass({
  contextTypes: {
    router: React.PropTypes.object
  },
  getInitialState() {
    return {
      first_name: [this.props.initialUserInfo.first_name, this.props.initialUserInfo.last_name].filter(item => item).join(' '),
      email:      this.props.initialUserInfo.email ? this.props.initialUserInfo.email : '',
      password:   '',
      isModified: false,
    }
  },
  onAuthCompleteRedirect(){
    this.context.router.push('/dashboard')
  },
  componentWillReceiveProps(nextProps){
    if (nextProps.token) {
      this.onAuthCompleteRedirect();
    }

    if (!this.state.isModified) {
      this.setState({
        first_name: nextProps.initialUserInfo.first_name,
        last_name: nextProps.initialUserInfo.last_name,
        email: nextProps.initialUserInfo.email,
      });
    }
  },
  onSubmit(e) {
    e.preventDefault();

    this.props.register(
      this.state.first_name,
      this.state.email,
      this.state.password,
      this.props.tokens,
    );
  },
  onEmailChange(e) {
    this.setState({
      isModified: true,
      email: e.target.value
    });
  },
  onNameChange(e) {
    this.setState({
      isModified: true,
      first_name: e.target.value
    });
  },
  onPasswordChange(e) {
    this.setState({
      isModified: true,
      password: e.target.value
    });
  },
  render: function () {
    let nameErrors = this.props.errors ? this.props.errors.first_name || [] : [];
    let emailErrors = this.props.errors ? this.props.errors.email || [] : [];
    let passwordErrors = this.props.errors ? this.props.errors.password || [] : [];
    let generalErrors = this.props.errors ? this.props.errors.general || [] : [];
    let loading = this.props.loading;

    if (this.props.token) {
      this.onAuthCompleteRedirect();
    }

    return (
      <Form horizontal role="form" method="POST" onSubmit={this.onSubmit}>
        <FormGroup validationState={nameErrors.length > 0 ? 'error' : null}>
          <ControlLabel className="col-md-4 control-label">Name</ControlLabel>

          <Col md={6}>
            <FormControl type="text"
                         name="first_name"
                         value={this.state.first_name}
                         onChange={this.onNameChange}/>
            <ValidationErrors errors={nameErrors}/>
          </Col>
        </FormGroup>

        <FormGroup validationState={emailErrors.length > 0 ? 'error' : null}>
          <ControlLabel className="col-md-4 control-label">E-Mail Address</ControlLabel>

          <Col md={6}>
            <FormControl type="email" name="email" value={this.state.email}
                         onChange={this.onEmailChange}/>
            <ValidationErrors errors={emailErrors}/>
          </Col>
        </FormGroup>

        <FormGroup validationState={passwordErrors.length > 0 ? 'error' : null}>
          <ControlLabel className="col-md-4 control-label">Password</ControlLabel>

          <Col md={6} className="col-md-6">
            <FormControl type="password" name="password" value={this.state.password}
                         onChange={this.onPasswordChange}/>
            <ValidationErrors errors={passwordErrors}/>
          </Col>
        </FormGroup>

        <FormGroup validationState={generalErrors.length > 0 ? 'error' : null}>
          <Col md={6} mdOffset={4}>
            <ValidationErrors errors={generalErrors}/>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col md={6} mdOffset={4}>
            <Button type="submit" bsStyle="primary" disabled={loading}>
              <LoadingGlyphicon glyph="user" loading={loading}/> Register
            </Button>
          </Col>
        </FormGroup>

        <hr />

        <FormGroup>
          <Col md={6} mdOffset={4}>
            <SocialAddButton provider="vkontakte" title="VK" />
            <SocialAddButton provider="facebook" title="Facebook" />
            <SocialAddButton provider="google" title="Google" />
          </Col>
        </FormGroup>
      </Form>
    )
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  let {userInfo, tokens} = state.socials.get('attached').toJS();
  // initialUserInfo = initialUserInfo ? initialUserInfo.toJS() : {};

  return {
    token: auth.token,
    loading: auth.register.loading,
    errors: auth.register.errors,
    initialUserInfo: userInfo ? userInfo : {},
    tokens,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    register: (name, email, password, socialTokens) => {
      dispatch(register(name, email, password, socialTokens));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterForm);