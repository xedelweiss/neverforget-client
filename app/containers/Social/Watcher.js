import React from 'react';
import {connect} from 'react-redux';
import * as socialsAction from '../../actions/socials';
import * as authAction from '../../actions/auth';
import {base64_decode} from '../../utils/base64';

const SocialWatcher = React.createClass({
  render() {
    let {socials, jwt} = this.props;

    if (socials) {
      socials = JSON.parse(base64_decode(socials));
      this.props.initSocialsInfo(socials);
    }

    if (jwt) {
      this.props.setToken(jwt);
    }

    return null;
  },

  componentWillReceiveProps(newProps){
    if (newProps.attached) {
      console.warn(newProps.attached);
    }
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  let attached = state.socials.get('attached').toJS();

  return {}
};

const mapDispatchToProps = (dispatch) => {
  return {
    setToken: (token) => {
      dispatch(authAction.setToken(token));
    },
    initSocialsInfo: (socials) => {
      dispatch(socialsAction.initSocialsInfo(socials))
    },
    fetchNewSocialUserInfo: (provider, code) => {
      let redirectUrl = window.location.origin + window.location.pathname;
      dispatch(socialsAction.fetchNewSocialUserInfo(provider, code, redirectUrl));
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialWatcher);