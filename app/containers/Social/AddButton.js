import React from 'react';
import {connect} from 'react-redux';
import {Button} from 'react-bootstrap';
import * as socialsAction from '../../actions/socials';
import LoadingGlyphicon from '../../components/LoadingGlyphicon';

const AddButton = React.createClass({
  getInitialState() {
    return {
      isLoading: false,
    }
  },
  propTypes: {
    provider: React.PropTypes.string.isRequired,
    title: React.PropTypes.string,
  },
  onClick() {
    this.setState({
      isLoading: true,
    });
    this.props.startSocialAuthorizationProcess(this.props.attached, this.props.provider, this.props.jwt);
  },
  render() {
    let buttonProvider = this.props.provider;
    let isLoading = this.state.isLoading;
    let isDone = this.props.attached.tokens && this.props.attached.tokens.filter(({provider}) => {
      return provider == buttonProvider;
    }).length > 0;

    return <Button title={isDone ? 'You will be able to add more social providers after sign up' : null} disabled={isLoading || isDone} bsStyle={isDone ? 'primary' : 'default'} onClick={isDone ? () => {} : this.onClick}>
      {isLoading && <LoadingGlyphicon loading={isLoading}/>} {this.props.title}
    </Button>
  }
});

const mapStateToProps = (state) => {
  let auth = state.auth.toJS();
  let attached = state.socials.get('attached').toJS();

  return {
    jwt: auth ? auth.token : null,
    attached,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    startSocialAuthorizationProcess: (attached, provider, jwt) => {
      let returnUrl = window.location.origin + window.location.pathname;
      dispatch(socialsAction.startSocialAuthorizationProcess(attached, provider, returnUrl, jwt));
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddButton);