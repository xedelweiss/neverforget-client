import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../actions/socials';
import LoadingGlyphicon from '../../components/LoadingGlyphicon';
import SocialsList from '../../components/Socials/SocialsList';

const SocialsContainer = React.createClass({
  componentWillMount() {
    this.props.fetchSocials();
  },
  onDelete(social) {
    this.props.deleteSocial(social);
  },
  onSync(social) {
    this.props.syncSocial(social);
  },
  render() {
    let isLoading = this.props.isLoading;
    let socials = this.props.socials;

    let handlers = {
      onDelete: this.onDelete,
      onSync: this.onSync,
    };

    if (this.props.children) {
      return React.cloneElement(this.props.children, {socials, isLoading, ...handlers});
    }

    return (
      isLoading
        ? <h4 className="text-center"><LoadingGlyphicon loading={true}/></h4>
        : <SocialsList
            socials={socials}
            {...handlers}
          />
    )
  }
});

const mapStateToProps = (state) => {
  let socialsList = state.socials.get('socialsList').toJS(); // @todo socials.socialsList.socials - think about this
  // console.info(socialsList);
  return {
    socials: socialsList.socials,
    isLoading: socialsList.isLoading,
    errors: socialsList.errors,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSocials: () => {
      dispatch(actions.fetchSocials());
    },
    deleteSocial: (social) => {
      dispatch(actions.deleteSocial(social));
    },
    syncSocial: (social) => {
      dispatch(actions.syncSocial(social));
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialsContainer);