import React from 'react';
import {connect} from 'react-redux';
import * as groupsAction from './../../actions/groups';
import EditableTagFormGroup from '../../components/form/EditableTagFormGroup';

const EditableGroupsFormGroup = React.createClass({
  getInitialState(){
    return {
      value: [],
      newGroups: [],
    }
  },

  componentWillMount() {
    this.props.fetchGroups();
  },

  componentWillReceiveProps(nextProps){
    if (nextProps.value) {
      this.setState({
        value: nextProps.value
      })
    }
  },

  render() {
    return (
      <EditableTagFormGroup
        type="text"
        label="Group"
        placeholder="Group"
        value={this.state.value.join(';')}
        options={[...this.state.newGroups.map(title => {
          return {
            value: title,
            label: title
          }
        }), ...this.props.groups.map(({data}) => {
          return {
            value: data.title,
            label: data.title
          }
        })]}
        onChange={(values) => {
          this.setState({
            value: values,
            newGroups: [
              ...this.state.newGroups,
              ...values.map(({value}) => value)
            ].filter((value, index, self) => self.indexOf(value) === index)
          });

          return this.props.onChange({
            target: {
              value: values.map(({value}) => {
                return {
                  title: value
                };
              })
            }
          })
        }}
        editable={this.props.editable}
      />
    )
  }
});

const mapStateToProps = (state) => {
  let groupsList = state.groupsList.toJS();

  return {
    groups: groupsList.groups,
    isLoading: groupsList.isLoading
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGroups: () => {
      dispatch(groupsAction.fetchGroups());
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditableGroupsFormGroup);