import React from "react";
import {log} from "../../utils/debug";
import update from "react-addons-update";
import {
  Row, Col, Form, Button,
  FormGroup, ControlLabel, FormControl
} from "react-bootstrap";
import jwtDecode from 'jwt-decode';
import {defineMessages, injectIntl, FormattedMessage, FormattedHTMLMessage} from "react-intl";
import {connect} from 'react-redux';
import {updatePassword} from "../../actions/settings";
import ValidationErrors from "../../components/ValidationErrors";
import "./styles/styles.scss";

const messages = defineMessages({
  new_password_not_confirmed: {
    id: 'new_password_not_confirmed',
    defaultMessage: 'New Password and New Password Confirm must be the same'
  }
});

const PasswordInformation = React.createClass({
  getInitialState() {
    const token = jwtDecode(this.props.token);

    let userInfo = {
      id:           token.id,
      first_name:   token.first_name,
      last_name:    token.last_name,
      middle_name:  token.middle_name,
      email:        token.email,
      timeZone:     token.timezone
    };

    return {
      user:         userInfo,
      password:     '',
      newPassword:  '',
      newPasswordConfirm: '',
      newPasswordNotConfirmed: false,
      passwordUpdated: false
    };
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.passwordState.passwordUpdated === true) {
      this.setState(update(this.state, {
        passwordUpdated: {$set: true}
      }))
    }
  },

  onFieldUpdated(field, e) {
    this.setState(update(this.state, {
      [field]: {
          $set: e.target.value
        }
    }), () => {
      if ( field === 'newPassword' || field === 'newPasswordConfirm' ) {
        if ( this.state.newPassword === this.state.newPasswordConfirm ) {
          this.setState(update(this.state, {
            newPasswordNotConfirmed: { $set: false }
          }));
        }
      }

      if ( field === 'password' ) {
        this.setState(update(this.state, {
          newPasswordNotConfirmed: { $set: false }
        }));
      }
    });


  },

  onUpdate() {
    if ( this.state.newPassword !== this.state.newPasswordConfirm ) {
      this.setState(update(this.state, {
        newPasswordNotConfirmed: { $set: true }
      }));
      return false;
    }

    this.props.updatePassword(this.state.user, this.state.password, this.state.newPassword);

    this.setState(update(this.state, {
      password: {$set: ''},
      newPassword: {$set: ''},
      newPasswordConfirm: {$set: ''}
    }));
  },

  render() {
    const intl  = this.props.intl;
    const newPasswordNotConfirmed = [intl.formatMessage(messages.new_password_not_confirmed)];

    let passwordErrors
      = this.props.passwordState.passwordErrors ? this.props.passwordState.passwordErrors : [];

    let newPasswordErrors
      = this.props.passwordState.newPasswordErrors ? this.props.passwordState.newPasswordErrors : [];

    return (
      <div className="container">
        <Form>
          <Row>
            <Col md={4}>
              <div className="settings-contact-info-title">
                <FormattedHTMLMessage
                  id="change_password"
                  defaultMessage="Change Password" />
              </div>

              {this.state.passwordUpdated &&
                <div className="bg-success">
                  <FormattedMessage
                    id="password_changed"
                    defaultMessage="The password has been changed" />
                </div>
              }
            </Col>
          </Row>

          <Row>
            <Col md={4}>
                <FormGroup controlId="current-password"
                           validationState={passwordErrors.length > 0 ? 'error' : null}>
                  <ControlLabel>
                    <FormattedMessage id="current_password" defaultMessage="Current Password" />
                  </ControlLabel>
                  <FormControl
                    type="password"
                    value={this.state.password}
                    onChange={this.onFieldUpdated.bind(this, 'password')}
                  />
                  <ValidationErrors errors={passwordErrors} />
                </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md={4}>
                <FormGroup controlId="new-password"
                           validationState={newPasswordErrors.length > 0 ? 'error' : null}>
                  <ControlLabel>
                    <FormattedMessage id="new_password" defaultMessage="New Password" />
                  </ControlLabel>
                  <FormControl
                    type="password"
                    value={this.state.newPassword}
                    onChange={this.onFieldUpdated.bind(this, 'newPassword')}
                  />
                  <ValidationErrors errors={newPasswordErrors} />
                </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md={4}>
              <FormGroup controlId="new-password-confirm"
                         validationState={this.state.newPasswordNotConfirmed ? 'error' : null}>
                <ControlLabel>
                  <FormattedMessage id="new_password_confirm" defaultMessage="New Password Confirm" />
                </ControlLabel>
                <FormControl
                  type="password"
                  value={this.state.newPasswordConfirm}
                  onChange={this.onFieldUpdated.bind(this, 'newPasswordConfirm')}
                />
                {this.state.newPasswordNotConfirmed && <ValidationErrors errors={newPasswordNotConfirmed} />}
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md={4}>
              <Button bsStyle="primary"
                      onClick={this.onUpdate}>
                <FormattedMessage id="update_password" defaultMessage="Update Password" />
              </Button>
            </Col>
          </Row>

        </Form>

      </div>
    );
  }
});

const mapStateToProps = (state) => {
  let auth          = state.auth.toJS();
  let passwordState = state.settings.toJS().password;

  return {
    token:          auth.token,
    passwordState:  passwordState
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePassword: function(user, password, newPassword) {
      dispatch(updatePassword(user, password, newPassword));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(PasswordInformation));