import React from 'react';
import {connect} from 'react-redux';
import {
  Row, Col
} from "react-bootstrap";
import {FormattedMessage, FormattedHTMLMessage} from "react-intl";
import SocialsContainer from "../Social/SocialsContainer";
import SocialsSettingsList from "../../components/Socials/SocialsSettingsList";
import SocialAddButton from '../../containers/Social/AddButton';

const SocialsInformation = React.createClass({
  render() {
    return <div className="container">
      <Row>
        <Col md={4}>

        </Col>
      </Row>
      <Row>
        <Col md={4}>
          <div className="settings-contact-info-title">
            <FormattedHTMLMessage
              id="Social_Information"
              defaultMessage="Social Information"/>
          </div>
        </Col>
      </Row>

      <Row>
        <Col md={4}>
          <SocialsContainer>
            <SocialsSettingsList />
          </SocialsContainer>
        </Col>
      </Row>

      <Row>
        <Col md={4}>
          <SocialAddButton provider="vkontakte" title="VK"/>
          <SocialAddButton provider="facebook" title="Facebook"/>
          <SocialAddButton provider="google" title="Google"/>
        </Col>
      </Row>
    </div>

  }
});

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => {
  return {}
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocialsInformation);