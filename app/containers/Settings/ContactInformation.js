import React from "react";
import Select from 'react-select';
import {log} from "../../utils/debug";
import {fromJS} from 'immutable';

import {
  Row, Col, Form, Button,
  FormGroup, ControlLabel, FormControl
} from "react-bootstrap";

import {FormattedMessage, FormattedHTMLMessage} from "react-intl";
import update from "react-addons-update";
import {connect} from 'react-redux';
import jwtDecode from 'jwt-decode';
import {updateUser} from "../../actions/settings";
import "./styles/styles.scss";

const ContactInformation = React.createClass({
  getInitialState() {
    let userInfo = this.extractUserInfoFromToken(this.props.token);

    return {
      userInfo: userInfo,
      userInfoUpdated: false
    };
  },

  extractUserInfoFromToken(token) {
    let decodedToken = jwtDecode(token);

    return {
      id: decodedToken.id,
      first_name: decodedToken.first_name,
      last_name: decodedToken.last_name,
      middle_name: decodedToken.middle_name,
      email: decodedToken.email,
      timeZone: decodedToken.timezone
    };
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.token && this.props.token && nextProps.token != this.props.token) {

      let currentUserInfo = fromJS(this.extractUserInfoFromToken(this.props.token));
      let nextUserInfo    = fromJS(this.extractUserInfoFromToken(nextProps.token));

      if (!nextUserInfo.equals(currentUserInfo)) {
        this.setState({
          userInfoUpdated: true
        });
      }
    }
  },

  onTimeZoneSelect(val) {
    this.setState(update(this.state, {
      userInfo: {
        timeZone: {$set: val.value}
      },
      userInfoUpdated: {$set: false},
    }));
  },

  onFieldUpdated(field, e) {
    this.setState(update(this.state, {
      userInfo: {
        [field]: {
          $set: e.target.value
        }
      },
      userInfoUpdated: {$set: false},
    }));
  },

  onSave() {
    this.setState({
      userInfoUpdated: false
    });

    this.props.updateUser(this.state.userInfo);
  },

  render() {
    let timeZones = this.props.timeZones.map((val) => {
      return {
        'value': val,
        'label': val
      };
    });

    return (
      <div className="container">
        <Row>
          <Col md={4}>
            <div className="settings-contact-info-title">
              <FormattedHTMLMessage
                id="Contact_Information"
                defaultMessage="Contact Information"/>
            </div>
            {this.state.userInfoUpdated &&
            <div className="bg-success">
              <FormattedMessage
                id="userinfo_updated"
                defaultMessage="Your information has been updated"/>
            </div>
            }
          </Col>
        </Row>

        <Row>
          <Col md={4}>
            <Form>
              <FormGroup controlId="user-first-name">
                <ControlLabel>
                  <FormattedMessage id="First_Name" defaultMessage="First Name"/>
                </ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.userInfo.first_name}
                  onChange={this.onFieldUpdated.bind(this, 'first_name')}
                />
              </FormGroup>

              <FormGroup controlId="user-last-name">
                <ControlLabel>
                  <FormattedMessage id="Last_Name" defaultMessage="Last Name"/>
                </ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.userInfo.last_name}
                  onChange={this.onFieldUpdated.bind(this, 'last_name')}
                />
              </FormGroup>

              <FormGroup controlId="user-middle-name">
                <ControlLabel>
                  <FormattedMessage id="Middle_Name" defaultMessage="Middle Name"/>
                </ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.userInfo.middle_name}
                  onChange={this.onFieldUpdated.bind(this, 'middle_name')}
                />
              </FormGroup>

              <FormGroup controlId="user-email">
                <ControlLabel>
                  <FormattedMessage id="Email" defaultMessage="Email"/>
                </ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.userInfo.email}
                  onChange={this.onFieldUpdated.bind(this, 'email')}
                />
              </FormGroup>

              <FormGroup controlId="user_timezone">
                <ControlLabel>
                  <FormattedMessage id="TimeZone" defaultMessage="Time Zone"/>
                </ControlLabel>
                <Select
                  name="timezone"
                  clearable={false}
                  value={{
                    value: this.state.userInfo.timeZone,
                    label: this.state.userInfo.timeZone
                  }}
                  options={timeZones}
                  onChange={this.onTimeZoneSelect}
                />
              </FormGroup>

              <Button bsStyle="primary" onClick={this.onSave}>Save changes</Button>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  const timeZones = state.data.toJS().timeZones;

  return {
    timeZones: timeZones
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUser: function (user) {
      dispatch(updateUser(user));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps)(ContactInformation);