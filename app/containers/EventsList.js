import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'react-bootstrap';
import Event from '../components/Event/EventItem';
import {fetchEvents, changeEventStatus} from '../actions/events';
import LoadingGlyphicon from '../components/LoadingGlyphicon';

const EventList = React.createClass({
    propTypes: {
        year: PropTypes.number.isRequired
    },
    componentWillMount() {
        this.props.fetchEvents(this.props.year);
    },
    componentDidUpdate(prevProps) { // @todo think about it: throws invariant Violation: findComponentRoot if componentWillReceiveProps
        if (this.props.year == prevProps.year) {
            return;
        }

        this.props.fetchEvents(this.props.year);
    },
    renderState() {
        switch (true) {
            case (this.props.loading):
                return this.renderLoading();
            case (this.props.errors.length > 0):
                return this.renderErrors();
            case (this.props.events.length == 0):
                return this.renderEmptyTable();
            case (this.props.events.length > 0):
                return this.renderTable();
        }
    },
    renderLoading() {
        return <h4 className="text-center"><LoadingGlyphicon loading={true} /> Loading..</h4>
    },
    renderErrors() {
        return <Row>
            <Col md={12} style={{color: 'red'}}>
                {this.props.errors.map((error, index) => <p key={index}>{error}</p>)}
            </Col>
        </Row>;
    },
    renderTable() {
        return (
            <div>
                <Row>
                    <Col md={3} className="event_table_header"><b>Event Name</b></Col>
                    <Col md={3} className="event_table_header"><b>Event Recipient</b></Col>
                    <Col md={2} className="event_table_header"><b>Event Date</b></Col>
                </Row>
                {this.props.events.map(({event, loading}) => <Event
                    key={event.id}
                    event={event}
                    loading={loading}
                    onToggleStatusClick={this.props.changeEventStatus.bind(this, event.id, !event.is_active)}
                />)}
            </div>
        );
    },
    renderEmptyTable() {
        return (
            <p>No Events</p>
        );
    },
    render() {
        return (
            <div>
                {this.renderState()}
            </div>
        )
    }
});

const mapStateToProps = (state) => {
    let events = state.events.toJS();
    return {
        events: events.events,
        loading: events.loading,
        errors: events.errors
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchEvents: (year) => {
            dispatch(fetchEvents(year));
        },
        changeEventStatus: (eventId, isActive) => {
            dispatch(changeEventStatus(eventId, isActive));
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EventList);