import React from 'react';
import {connect} from 'react-redux';
import {LinkContainer} from 'react-router-bootstrap';
import {ButtonToolbar, ButtonGroup, DropdownButton, MenuItem, Button, Glyphicon} from 'react-bootstrap';
import {mergeMetapersons} from "../../actions/contacts";
import SocialsContainer from '../../containers/Social/SocialsContainer';
import SocialsDropdownButton from '../../components/Socials/SocialsDropdownButton';

const MetapersonsToolbar = React.createClass({
  getSelectedMetapersons() {
    return this.props.loading
      ? []
      : this.props.metapersons.filter(function (metaperson) {
        return metaperson.selected;
      });
  },
  mergeSelectedMetapersons() {
    let selectedMetapersons = this.getSelectedMetapersons().map(metaperson => metaperson.metaperson);
    this.props.mergeMetapersons(selectedMetapersons);
  },
  render() {
    let loading = this.props.loading;
    let selectedContacts = this.getSelectedMetapersons();
    let toolbarDisabled = loading;

    return (
      <div>
        <ButtonToolbar>
          <ButtonGroup bsSize="small" className="pull-right">
            <LinkContainer to={{pathname: '/contacts/new'}}>
              <Button><Glyphicon glyph="plus"/> New Contact</Button>
            </LinkContainer>
          </ButtonGroup>
          <ButtonGroup bsSize="small" className="pull-right">
            <DropdownButton bsSize="small" title={<span><Glyphicon glyph="pencil"/> Edit</span>} id="bg-nested-dropdown"
                            disabled={toolbarDisabled || true}>
              <MenuItem eventKey={1}>Merge duplicates</MenuItem>
              <MenuItem eventKey={2}>Add new group</MenuItem>
              <MenuItem eventKey={3}>Edit groups</MenuItem>
            </DropdownButton>
          </ButtonGroup>
          <ButtonGroup bsSize="small" className="pull-right">
            <SocialsContainer>
              <SocialsDropdownButton disabled={toolbarDisabled} />
            </SocialsContainer>
          </ButtonGroup>
          <ButtonGroup bsSize="small" className="pull-right">
            <Button disabled={toolbarDisabled || true}><Glyphicon glyph="refresh"/> Sync Now</Button>
          </ButtonGroup>
          <ButtonGroup bsSize="small" className="pull-right">
            <Button disabled={toolbarDisabled || selectedContacts.length < 1}
                    onClick={() => this.mergeSelectedMetapersons()}><Glyphicon glyph="resize-small"/> Merge
              selected</Button>
          </ButtonGroup>
        </ButtonToolbar>
      </div>
    )
  }
})

const mapStateToProps = (state) => {
  let metapersonsList = state.contacts.metapersonsList.toJS();
  return {
    metapersons: metapersonsList.metapersons,
    loading: metapersonsList.loading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    mergeMetapersons: (metapersons) => {
      dispatch(mergeMetapersons(metapersons));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonsToolbar);