import React from 'react';
import {connect} from 'react-redux';
import {applyMetapersonsFilter} from '../../actions/contacts';

const MetapersonsDestinationFilter = React.createClass({
  getInitialState(){
    return {
      destinations: [
        {
          name: null,
          label: 'All Destinations',
        },
        {
          name: 'phone',
          label: 'Phone',
        },
        {
          name: 'email',
          label: 'Email',
        },
        {
          name: 'vkontakte',
          label: 'VKontakte',
        },
        {
          name: 'facebook',
          label: 'Facebook',
        },
        {
          name: 'skype',
          label: 'Skype',
        },
        {
          name: 'viber',
          label: 'Viber',
        },
        {
          name: 'telegram',
          label: 'Telegram',
        },
        {
          name: 'whatsapp',
          label: 'WhatsApp',
        },
      ]
    }
  },

  render() {
    console.info(this.state);
    return (
      <ul className="list-unstyled">
        <li><a href="#">By Destination</a></li>
        <ul>
          {this.state.destinations.map(({name, label}, key) => <li key={key}><a href="#" onClick={() => this.props.applyFilter(name)}>{label} {this.props.activeDestination == name ? '(active)' : null}</a></li>)}
        </ul>
      </ul>
    )
  }
});

const mapStateToProps = (state) => {
  let metapersonsList = state.contacts.metapersonsList.toJS();
  return {
    activeDestination: metapersonsList.filter.destination
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    applyFilter: (destination) => {
      dispatch(applyMetapersonsFilter({destination}));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonsDestinationFilter);