import React from 'react';
import {connect} from 'react-redux';
import {FormGroup, InputGroup, Button, FormControl, Glyphicon} from "react-bootstrap";
import {applyMetapersonsFilter} from '../../actions/contacts';

const MetapersonTextFilter = React.createClass({
  handleChange: function(event) {
    this.props.applyFilter(event.target.value);
  },
  render() {
    return (
      <FormGroup>
        <InputGroup bsSize="small">
          <FormControl value={this.props.text} placeholder='search..' ref="filterTextInput" onChange={this.handleChange} />
          <InputGroup.Button>
            <Button><Glyphicon glyph="search" /> Search</Button>
          </InputGroup.Button>
        </InputGroup>
      </FormGroup>
    )
  }
});

const mapStateToProps = (state) => {
  let metapersonsList = state.contacts.metapersonsList.toJS();

  return {
    text: metapersonsList.filter.text
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    applyFilter: (text) => {
      dispatch(applyMetapersonsFilter({text}));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonTextFilter);