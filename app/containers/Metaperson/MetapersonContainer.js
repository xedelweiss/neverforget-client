import React from 'react';
import update from 'react-addons-update';
import {connect} from 'react-redux';
import {
  Row,
  Col,
  Button,
  Glyphicon,
  Nav,
  NavItem
} from 'react-bootstrap';
import * as contactsAction from "../../actions/contacts";
import * as dateTypesAction from '../../actions/dateTypes';
import LoadingGlyphicon from '../../components/LoadingGlyphicon';
import MetapersonInfo from '../../components/Metaperson/MetapersonInfo';
import MetapersonContacts from '../../components/Metaperson/MetapersonContacts';
import MetapersonDates from '../../components/Metaperson/MetapersonDates';
import MetapersonToolbar from '../../components/Metaperson/MetapersonToolbar';

const MetapersonContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object
  },

  getInitialState(){
    return {
      metaperson: {},
      contacts: [],
      dates: [],
      persons: [],
      activePersonId: 0
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.metaperson) {
      if (nextProps.metaperson.id && this.props.metapersonId != nextProps.metaperson.id) {
        this.context.router.push(`/contacts/${nextProps.metaperson.id}`);
      }

      this.setState({
        metaperson: nextProps.metaperson
      });
    }
    if (nextProps.contactsList.contacts) {
      this.setState({
        contacts: nextProps.contactsList.contacts
          .map(({...contact}) => {
            return {...contact, isModified: !!contact.id}
          })
      });
    }
    if (nextProps.datesList.dates) {
      this.setState({
        dates: nextProps.datesList.dates
          .map(({...date}) => {
            return {...date, isModified: !!date.id}
          })
      });
    }
    if (nextProps.personsList.persons) {
      this.setState({
        persons: nextProps.personsList.persons
      });
    }
  },

  componentWillMount() {
    let metapersonId = this.props.metapersonId;

    if (metapersonId) {
      this.props.fetchMetaperson(metapersonId);
      this.props.fetchMetapersonContacts(metapersonId);
      this.props.fetchMetapersonDates(metapersonId);
      this.props.fetchMetapersonPersons(metapersonId);
      this.props.fetchDateTypes();
    } else {
      this.props.initNewMetaperson();
    }
  },

  changeActivePerson(eventKey) {
    event.preventDefault();
    this.setState({
      activePersonId: eventKey
    });
  },

  onMetapersonStartEdit() {
    this.setState({
      activePersonId: 0,
    });

    this.props.startEditMetaperson();
  },

  onMetapersonCancelEdit() {
    if (!this.props.metapersonId) {
      this.context.router.push(`/contacts`);
    }

    this.props.cancelEditMetaperson();
  },

  onMetapersonSave() {
    // detect diffs
    let contacts = this.state.contacts
      // .filter(({isModified}) => isModified)
      .map(({data}) => data);
    let dates = this.state.dates
      // .filter(({isModified}) => isModified)
      .map(({data}) => data);

    if (this.props.metapersonId) {
      this.props.saveMetapersonWithRelations(this.state.metaperson, contacts, dates); // @fixme how to
    } else {
      this.props.createMetaperson(this.state.metaperson);
    }
  },

  onMetapersonChange(field, event) {
    let value = event.target.value;

    this.setState(update(this.state, {
      metaperson: {
        [field]: {
          $set: value
        }
      }
    }));
  },

  onContactChange(key, field, value){
    if (field == 'type') {
      field = 'contact_type';
    }

    this.props.editMetapersonContactByKey(key, {
      [field]: value,
    });
  },

  onDataChange(key, field, value){
    if (field == 'type') {
      field = 'date_type_id';
    }

    if (field == 'value') {
      field = 'date'; // @todo think about this
    }

    this.props.editMetapersonDateByKey(key, {
      [field]: value,
    });
  },

  onContactAdded(contactType){
    this.props.initNewMetapersonContact({
      contact_type: contactType,
      value: '',
    });
  },

  onContactDelete(key, contact){
    this.props.deleteMetapersonContactByKey(key, contact);
  },

  onDataAdded(){
    this.props.initNewMetapersonDate({
      date: '',
    });
  },

  onDateDelete(key, date){
    this.props.deleteMetapersonDateByKey(key, date);
  },

  renderLoading(){
    return <h4 className="text-center"><LoadingGlyphicon loading={true}/> Loading..</h4>;
  },

  render() {
    let {errors, loading, editable} = this.props;
    let {metaperson, dates, persons, contacts} = this.state;
    let activePerson = persons.filter(person => person.id == this.state.activePersonId)[0];

    let personFilter = function (item) {
      return this.state.activePersonId > 0 ? item.data.person_id == this.state.activePersonId : true;
    }.bind(this);

    contacts = contacts.filter(personFilter);
    dates = dates.filter(personFilter);

    if (loading || metaperson == null) {
      return this.renderLoading();
    }

    return (
      <div>
        <MetapersonToolbar
          metaperson={metaperson}
          loading={loading}
          isInEditMode={editable}
          onMerge={() => this.props.convertToLocal(this.state.metaperson)}
          onEdit={() => this.onMetapersonStartEdit()}
          onSave={() => this.onMetapersonSave()}
          onCancel={() => this.onMetapersonCancelEdit()}
        >
          {
            !activePerson ? null :
              <Button bsStyle="danger"
                      onClick={() => this.props.unmergeMetapersonPerson(metaperson, activePerson)}
                      disabled={activePerson.id == metaperson.id}
                      title={activePerson.id == metaperson.id ? 'You can\'t disconnect main person yet' : null}
              ><Glyphicon glyph="resize-full"/> Disconnect</Button>
          }
        </MetapersonToolbar>

        <MetapersonInfo
          metaperson={metaperson}
          onFieldChange={this.onMetapersonChange}
          editable={editable}
          errors={errors}
        />
        {
          persons.length < 2 || editable ? null :
            <Row style={{margin: '10px 0'}}>
              <Col md={12}>
                <Nav bsStyle="tabs" activeKey={this.state.activePersonId} onSelect={this.changeActivePerson}>
                  <NavItem eventKey={0}>All</NavItem>
                  {persons.map((person) => <NavItem key={person.data.id}
                                                    eventKey={person.data.id}>{person.data.sync_source ? person.data.sync_source.social_type : 'local'}</NavItem>)}
                </Nav>
              </Col>
            </Row>
        }

        {
          !this.props.metapersonId ? null :
            <MetapersonContacts
              editable={editable}
              contacts={contacts}
              onContactChange={this.onContactChange}
              onAdd={this.onContactAdded}
              onDelete={this.onContactDelete}
            />
        }

        {
          !this.props.metapersonId ? null :
            <MetapersonDates
              loading={loading || this.props.dateTypesList.loading}
              editable={editable}
              dates={dates}
              onDateChange={this.onDataChange}
              onAdd={this.onDataAdded}
              onDelete={this.onDateDelete}
              types={this.props.dateTypesList.dateTypes}
            />
        }
      </div>
    )
  }
});

const mapStateToProps = (state) => {
  let activeMetaperson   = state.contacts.activeMetaperson.metaperson.toJS();
  let activeContactsList = state.contacts.activeMetaperson.contactsList.toJS();
  let activeDatesList    = state.contacts.activeMetaperson.datesList.toJS();
  let activePersonsList  = state.contacts.activeMetaperson.personsList.toJS();

  let dateTypesList = state.dateTypesList.toJS();

  return {
    editable: activeMetaperson.isEdited,
    isNew: activeMetaperson.isNew,

    metaperson: activeMetaperson.data,
    loading: activeMetaperson.loading,
    errors: activeMetaperson.errors,

    contactsList: activeContactsList,
    datesList: activeDatesList,
    personsList: activePersonsList,

    dateTypesList: dateTypesList,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    initNewMetaperson: () => {
      dispatch(contactsAction.initNewMetaperson());
    },
    initNewMetapersonContact: (contact) => {
      dispatch(contactsAction.initNewMetapersonContact(contact))
    },
    deleteMetapersonContactByKey: (key, contact) => {
      dispatch(contactsAction.deleteMetapersonContactByKey(key, contact))
    },
    deleteMetapersonDateByKey: (key, date) => {
      dispatch(contactsAction.deleteMetapersonDateByKey(key, date))
    },
    editMetapersonContactByKey: (key, contact) => {
      dispatch(contactsAction.editMetapersonContactByKey(key, contact))
    },
    initNewMetapersonDate: (date) => {
      dispatch(contactsAction.initNewMetapersonDate(date))
    },
    editMetapersonDateByKey: (key, date) => {
      dispatch(contactsAction.editMetapersonDateByKey(key, date))
    },
    fetchMetaperson: (metapersonId) => {
      dispatch(contactsAction.fetchMetaperson(metapersonId));
    },
    fetchMetapersonContacts: (metapersonId) => {
      dispatch(contactsAction.fetchMetapersonContacts(metapersonId));
    },
    fetchMetapersonDates: (metapersonId) => {
      dispatch(contactsAction.fetchMetapersonDates(metapersonId));
    },
    fetchDateTypes: () => {
      dispatch(dateTypesAction.fetchDateTypes());
    },
    fetchMetapersonPersons: (metapersonId) => {
      dispatch(contactsAction.fetchMetapersonPersons(metapersonId));
    },
    unmergeMetapersonPerson: (metaperson, person) => {
      dispatch(contactsAction.unmergeMetapersonPerson(metaperson, person));
    },
    saveMetaperson: (metaperson) => {
      dispatch(contactsAction.saveMetaperson(metaperson))
    },
    saveMetapersonWithRelations: (metaperson, contacts, dates) => {
      dispatch(contactsAction.saveMetapersonWithRelations(metaperson, contacts, dates))
    },
    createMetaperson: (metaperson) => {
      dispatch(contactsAction.createMetaperson(metaperson))
    },
    startEditMetaperson: () => {
      dispatch(contactsAction.startEditActiveMetaperson())
    },
    cancelEditMetaperson: () => {
      dispatch(contactsAction.cancelEditActiveMetaperson())
    },
    commitEditMetaperson: () => {
      dispatch(contactsAction.commitEditActiveMetaperson())
    },
    convertToLocal: (metaperson) => {
      dispatch(contactsAction.mergeMetapersons([metaperson]));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonContainer);