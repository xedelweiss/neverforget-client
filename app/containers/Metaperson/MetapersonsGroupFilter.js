import React from 'react';
import {connect} from 'react-redux';

import LoadingGlyphicon from '../../components/LoadingGlyphicon';
import * as groupsAction from './../../actions/groups';
import {applyMetapersonsFilter} from '../../actions/contacts';

const MetapersonsGroupFilter = React.createClass({
  componentWillMount() {
    this.props.fetchGroups();
  },

  render() {
    let isLoading = this.props.isLoading;
    let groups = this.props.groups;

    return (
        <ul className="list-unstyled">
          <li>By Group</li>
          <ul>
            <li><a href="#" onClick={() => this.props.applyFilter(null)}>All Groups {this.props.activeGroup == null ? '(active)' : null}</a></li>
            {
              isLoading
                ? <li className="list-unstyled"><LoadingGlyphicon loading={true}/> Loading..</li>
                : groups.map(({data}, key) => <li key={key}><a href="#" onClick={() => this.props.applyFilter(data.title)}>{data.title} {this.props.activeGroup == data.title ? '(active)' : null}</a></li>)
            }
          </ul>
        </ul>
    )

  }
});

const mapStateToProps = (state) => {
  let metapersonsList = state.contacts.metapersonsList.toJS();
  let groupsList = state.groupsList.toJS();
  return {
    groups: groupsList.groups,
    isLoading: groupsList.isLoading,
    errors: groupsList.errors,
    activeGroup: metapersonsList.filter.group
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchGroups: () => {
      dispatch(groupsAction.fetchGroups());
    },
    applyFilter: (group) => {
      dispatch(applyMetapersonsFilter({group}));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonsGroupFilter);