import React from 'react';
import {connect} from 'react-redux';
import {ButtonToolbar, ButtonGroup, Button, MenuItem, Glyphicon, DropdownButton} from 'react-bootstrap';
import {startEditActiveMetaperson, cancelEditActiveMetaperson, commitEditActiveMetaperson, saveMetaperson} from '../../actions/contacts';

const MetapersonToolbar = React.createClass({
    render() {
      let {loading, editable, metaperson} = this.props;
      let toolbarDisabled = loading;

      return (
          <div>
            <ButtonToolbar>
              <ButtonGroup className="pull-right">
                {!editable ? null :
                   <ButtonGroup>
                     <Button onClick={() => this.props.saveMetaperson()} bsStyle="primary" disabled={toolbarDisabled}><Glyphicon glyph="save" /> Save</Button>
                     <Button onClick={() => this.props.cancelEditMetaperson()} bsStyle="primary" disabled={toolbarDisabled}>Cancel</Button>
                   </ButtonGroup>
                }
                {editable ? null:
                  <Button onClick={() => this.props.startEditMetaperson()} bsStyle="primary" disabled={toolbarDisabled}><Glyphicon glyph="pencil" /> Edit</Button>
                }
              </ButtonGroup>
              <ButtonGroup className="pull-right">
                {
                  !editable ? null : <DropdownButton disabled={toolbarDisabled || true} title={<span><Glyphicon glyph='plus' /> Add..</span>} id="bg-nested-dropdown">
                    <MenuItem eventKey={1}>Phone</MenuItem>
                    <MenuItem eventKey={2}>Email</MenuItem>
                    <MenuItem eventKey={3}>VKontakte</MenuItem>
                    <MenuItem eventKey={4}>Facebook</MenuItem>
                    <MenuItem eventKey={5}>Skype</MenuItem>
                    <MenuItem eventKey={6}>Viber</MenuItem>
                    <MenuItem eventKey={7}>Telegram</MenuItem>
                    <MenuItem eventKey={8}>WhatsApp</MenuItem>
                  </DropdownButton>
                }
              </ButtonGroup>
            </ButtonToolbar>
          </div>
        )
    }
})

const mapStateToProps = (state) => {
  let activeMetaperson = activeMetaperson.metaperson.toJS();
  return {
    metaperson: activeMetaperson.data,
    loading: activeMetaperson.loading,
    editable: activeMetaperson.isEdited
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    startEditMetaperson: () => {
      dispatch(startEditActiveMetaperson());
    },
    cancelEditMetaperson: () => {
      dispatch(cancelEditActiveMetaperson());
    },
    commitEditMetaperson: () => {
      dispatch(commitEditActiveMetaperson());
    },
    saveMetaperson: (metaperson) => {
      dispatch(saveMetaperson(metaperson));
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonToolbar);