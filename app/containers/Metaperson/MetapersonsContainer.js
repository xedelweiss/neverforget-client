import React from 'react';
import {connect} from 'react-redux';
import LoadingGlyphicon from '../../components/LoadingGlyphicon';
import {fetchMetapersons, deleteMetaperson, selectMetaperson, unselectMetaperson, resetMetapersonsFilter}
  from "../../actions/contacts";
import fuzzySearch from '../../utils/fuzzySearch';
import MetapersonsList from '../../components/Metaperson/MetapersonsList';

const MetapersonsContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object
  },
  getDefaultProps() {
    return {
      metapersons: []
    }
  },
  componentWillMount() {
    this.props.fetchMetapersons()
  },
  deleteMetaperson(metaperson) {
    this.props.deleteMetaperson(metaperson);
  },
  onMetapersonSelectChange(metaperson, e) {
    if (e.target.checked) {
      this.props.selectMetaperson(metaperson);
    } else {
      this.props.unselectMetaperson(metaperson);
    }
  },
  openMetapersonDetails(metaperson) {
    this.context.router.push('/contacts/' + metaperson.id);
  },
  getFilteredMetapersons: function () {
    return this.props.metapersons.filter(metapersonEntity => {
      let metaperson = metapersonEntity.metaperson;
      let filterText  = this.props.filter.text;
      let filterGroup  = this.props.filter.group;
      let filterDestination  = this.props.filter.destination;

      if (filterGroup && metaperson.groups.filter(({title}) => title == filterGroup).length == 0) {
        return false;
      }

      if (filterDestination && metaperson.contacts.filter(({contact_type}) => contact_type == filterDestination).length == 0) {
        return false;
      }

      let firstName = metaperson.first_name;
      let lastName  = metaperson.last_name;
      let nickName  = metaperson.nick_name;

      return (
        !filterText
        || fuzzySearch((firstName + ' ' + lastName), filterText)
        || fuzzySearch((lastName + ' ' + firstName), filterText)
        || fuzzySearch(nickName, filterText)
      );
    });
  },
  render() {
    let loading = this.props.loading;
    let metapersons = this.getFilteredMetapersons();

    if (!this.props.loading && this.props.metapersons.length == 0) {
      return (
        <p>You have no contacts. Try to import some ;)</p>
      )
    }

    if (!this.props.loading && metapersons.length == 0) {
      return (
        <p>No persons found. Try to <a style={{color: 'brown'}} href="#" onClick={this.props.resetFilter}>reset</a> filter</p>
      );
    }

    return (
      loading
        ? <h4 className="text-center"><LoadingGlyphicon loading={true}/> Loading..</h4>
        : <MetapersonsList
            metapersons={metapersons}
            deleteMetaperson={this.deleteMetaperson}
            openMetapersonDetails={this.openMetapersonDetails}
            onMetapersonSelectChange={this.onMetapersonSelectChange}
          />
    )
  }
});

const mapStateToProps = (state) => {
  let metapersonsList = state.contacts.metapersonsList.toJS();
  return {
    metapersons: metapersonsList.metapersons,
    loading: metapersonsList.loading,
    errors: metapersonsList.errors,
    filter: metapersonsList.filter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMetapersons: () => {
      dispatch(fetchMetapersons());
    },
    deleteMetaperson: (metaperson) => {
      dispatch(deleteMetaperson(metaperson))
    },
    selectMetaperson: (metaperson) => {
      dispatch(selectMetaperson(metaperson));
    },
    unselectMetaperson: (metaperson) => {
      dispatch(unselectMetaperson(metaperson));
    },
    resetFilter: () => {
      dispatch(resetMetapersonsFilter());
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MetapersonsContainer);