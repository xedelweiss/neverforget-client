import React from 'react';
import LoadingGlyphicon from '../../components/LoadingGlyphicon';

const VkontakteLogin = React.createClass({
  render() {
    return (
      <h4 className="text-center"><LoadingGlyphicon loading={true}/> Please wait..</h4>
    )
  }
});

export default VkontakteLogin;