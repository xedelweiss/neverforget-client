import React from 'react';
import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Glyphicon} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import AuthorizedContainer from '../../containers/AuthorizedContainer';
import LogoutContainer from '../../containers/LogoutContainer';
import TokenInfo from '../../containers/TokenInfo';
import {FormattedMessage, injectIntl} from 'react-intl';
import LanguageDropDown from '../../containers/i18n/LanguageDropDown';

let mainMenu = React.createClass({
  render: function () {
    return (
      <Navbar className="top-bar">
        <Navbar.Header>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <AuthorizedContainer data-invert={false}>
            <Nav>
              <LinkContainer to="/dashboard">
                <NavItem eventKey={2}>
                  <FormattedMessage id="Dashboard" defaultMessage="Dashboard" />
                </NavItem>
              </LinkContainer>

              <LinkContainer to="/contacts">
                <NavItem eventKey={3}>
                  <FormattedMessage id="Contacts" defaultMessage="Contacts" />
                </NavItem>
              </LinkContainer>

              <LinkContainer to="/schedule" className="main_menu_item">
                <NavItem eventKey={4}>
                  <FormattedMessage id="Events" defaultMessage="Events" />
                </NavItem>
              </LinkContainer>
            </Nav>
          </AuthorizedContainer>

          <Nav pullRight>
            <LanguageDropDown/>
          </Nav>

          <AuthorizedContainer data-invert={true}>
            <Nav pullRight>
              <LinkContainer to="/dashboard">
                <NavItem eventKey={2}>
                  <FormattedMessage id="Dashboard" defaultMessage="Dashboard" />
                </NavItem>
              </LinkContainer>

              <LinkContainer to="/login">
                <NavItem eventKey={4}>
                  <FormattedMessage id="Login" defaultMessage="Login" />
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/register">
                <NavItem eventKey={5}>
                  <FormattedMessage id="Register" defaultMessage="Register" />
                </NavItem>
              </LinkContainer>
            </Nav>
          </AuthorizedContainer>

          <AuthorizedContainer data-invert={false}>
            <Nav pullRight>
              <NavDropdown eventKey={6} title={<TokenInfo field="first_name" />} id="basic-nav-dropdown">
                <LinkContainer to="/settings">
                  <MenuItem eventKey={6.1}>
                    <FormattedMessage id="Settings" defaultMessage="Settings" />
                    <Glyphicon glyph="wrench" className="pull-right" />
                  </MenuItem>
                </LinkContainer>

                <MenuItem divider/>

                <LogoutContainer>
                  <MenuItem eventKey={6.3}>
                    <FormattedMessage id="Logout" defaultMessage="Logout" />
                    <Glyphicon glyph="log-out" className="pull-right" />
                  </MenuItem>
                </LogoutContainer>
              </NavDropdown>
            </Nav>
          </AuthorizedContainer>
        </Navbar.Collapse>
      </Navbar>
    )
  }
});

export default injectIntl(mainMenu);