import React from 'react';
import {Glyphicon} from 'react-bootstrap';

const LoadingGlyphicon = React.createClass({
  propTypes: {
    loading: React.PropTypes.bool.isRequired,
  },
  getDefaultProps() {
    return {
      glyph: 'none',
    }
  },
  render() {
    let {loading, glyph} = this.props;

    return (
      <Glyphicon glyph={loading ? 'refresh' : glyph} className={loading ? 'glyphicon-refresh-animate' : null}/>
    )
  }
});

export default LoadingGlyphicon;