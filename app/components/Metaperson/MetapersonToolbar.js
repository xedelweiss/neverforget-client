import React from 'react';
import {ButtonToolbar, ButtonGroup, Button, MenuItem, Glyphicon, DropdownButton, Row, Col, FormGroup} from 'react-bootstrap';

const MetapersonToolber = React.createClass({
  propTypes: {
    loading: React.PropTypes.bool,
    isInEditMode: React.PropTypes.bool,
    onSave: React.PropTypes.func,
    onMerge: React.PropTypes.func,
    onEdit: React.PropTypes.func,
    onCancel: React.PropTypes.func,
  },
  render() {
    let {loading, isInEditMode, metaperson} = this.props;
    let toolbarDisabled = loading;

    return (
      <Row>
        <Col md={12}>
          <FormGroup>
            <ButtonToolbar>
              <ButtonGroup className="pull-right">
                { isInEditMode &&
                  <ButtonGroup>
                    <Button onClick={() => this.props.onSave()} bsStyle="primary" disabled={toolbarDisabled}><Glyphicon glyph="save" /> Save</Button>
                    <Button onClick={() => this.props.onCancel()} bsStyle="primary" disabled={toolbarDisabled}>Cancel</Button>
                  </ButtonGroup>
                }
                { !isInEditMode && !metaperson.social_source_id && <Button onClick={() => this.props.onEdit()} bsStyle="primary" disabled={toolbarDisabled}><Glyphicon glyph="pencil" /> Edit</Button> }
                { !isInEditMode && metaperson.social_source_id && <Button onClick={() => this.props.onMerge()} bsStyle="primary" disabled={toolbarDisabled}><Glyphicon glyph="lock" /> Unlock</Button> }
              </ButtonGroup>
              <ButtonGroup className="pull-right">{this.props.children}</ButtonGroup>
            </ButtonToolbar>

          </FormGroup>
        </Col>
      </Row>
    )
  }
});

export default MetapersonToolber;