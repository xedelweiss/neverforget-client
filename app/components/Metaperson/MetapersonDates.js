import React from 'react';
import {Row, Col, Button, Glyphicon} from 'react-bootstrap';
import GenericDate from './Date/GenericDate'

const MetapersonDates = React.createClass({
  propTypes: {
    editable: React.PropTypes.bool,
    types: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    dates: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    onDateChange: React.PropTypes.func.isRequired,
    onAdd: React.PropTypes.func,
    onDelete: React.PropTypes.func.isRequired,
  },

  render() {
    let {dates} = this.props;
    // date.data.date_type.title for custom

    dates = dates.map((item, key) => {
      return {
        ...item,
        _key: key,
      }
    });

    return (
      <Row>
        <Col md={12}>
          <h4>Dates:</h4>
        </Col>
        {
          dates.map(date =>
            <Col md={3} key={date._key}>
              <GenericDate value={date.data.date}
                           editable={this.props.editable}
                           types={this.props.types.map(({id, title}) => {return {
                             type: id,
                             title
                           }})}
                           type={date.data.date_type_id}
                           errors={date.errors}
                           onChange={(field, value) => this.props.onDateChange(date._key, field, value)}
                           onDelete={() => this.props.onDelete(date._key, date.data)}
              />
            </Col>
          )
        }
        {
          this.props.onAdd && this.props.editable &&
          <Col md={3}>
            <Button onClick={this.props.onAdd}><Glyphicon glyph="plus"/> Add</Button>
          </Col>
        }
      </Row>
    )
  }
});

export default MetapersonDates;