import React from 'react';
import {Row, Col, Button, Glyphicon} from 'react-bootstrap';
import PhoneContact from '../../components/Metaperson/Contact/PhoneContact';
import EmailContact from '../../components/Metaperson/Contact/EmailContact';
import MessengerContact from '../../components/Metaperson/Contact/MessengerContact';

const MetapersonContacts = React.createClass({
  propTypes: {
    contacts: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    onContactChange: React.PropTypes.func.isRequired,
    onAdd: React.PropTypes.func,
    onDelete: React.PropTypes.func.isRequired,
    // metaperson: React.PropTypes.object.isRequired
  },
  render() {
    let {contacts, editable} = this.props;

    contacts = contacts.map((item, key) => {
      return {
        ...item,
        _key: key,
      }
    });

    let emailContacts = contacts.filter((contact) => {
      return contact.data.contact_type == 'email';
    });

    let phoneContacts = contacts.filter((contact) => {
      return contact.data.contact_type == 'phone';
    });

    let messengerContacts = contacts.filter((contact) => {
      return contact.data.contact_type != 'phone'
        && contact.data.contact_type != 'email';
    }).sort((a, b) => a.contact_type > b.contact_type);

    return (
      <Row>
        <Col md={9}>
          <Row>
            <Col md={4} style={{display: phoneContacts.length > 0 || editable ? 'block' : 'none'}}>
              <h4>Phone:</h4>
              {phoneContacts.map(contact => <PhoneContact editable={editable} key={contact._key} type="phone" value={contact.data.value} errors={contact.errors} onDelete={() => this.props.onDelete(contact._key, contact.data)} onChange={(field, value) => this.props.onContactChange(contact._key, field, value)}/>)}

              {
                this.props.onAdd && this.props.editable &&
                <Button onClick={() => this.props.onAdd('phone')}><Glyphicon glyph="plus"/> Add</Button>
              }
            </Col>
            <Col md={4} style={{display: emailContacts.length > 0 || editable ? 'block' : 'none'}}>
              <h4>E-mail:</h4>
              {emailContacts.map(contact => <EmailContact editable={editable} key={contact._key} type="email" value={contact.data.value} errors={contact.errors} onDelete={() => this.props.onDelete(contact._key, contact.data)} onChange={(field, value) => this.props.onContactChange(contact._key, field, value)}/>)}

              {
                this.props.onAdd && this.props.editable &&
                <Button onClick={() => this.props.onAdd('email')}><Glyphicon glyph="plus"/> Add</Button>
              }
            </Col>
            <Col md={4} style={{display: messengerContacts.length > 0 || editable ? 'block' : 'none'}}>
              <h4>Messengers:</h4>
              {messengerContacts.map(contact => <MessengerContact editable={editable} key={contact._key} type={contact.data.contact_type} errors={contact.errors} value={contact.data.value} onDelete={() => this.props.onDelete(contact._key, contact.data)} onChange={(field, value) => this.props.onContactChange(contact._key, field, value)}/>)}

              {
                this.props.onAdd && this.props.editable &&
                <Button onClick={() => this.props.onAdd('viber')}><Glyphicon glyph="plus"/> Add</Button>
              }
            </Col>
          </Row>
        </Col>
        { true ? null : // not ready
          <Col md={2} mdOffset={1}>
            <h4>Socials:</h4>
            <p><Button bsStyle="info"><Glyphicon glyph="tower"/> Social 1</Button></p>
            <p><Button bsStyle="info"><Glyphicon glyph="road"/> Social 2</Button></p>
            <p><Button bsStyle="info"><Glyphicon glyph="send"/> Social 3</Button></p>
          </Col>
        }
      </Row>
    )
  }
});

export default MetapersonContacts;