import React from 'react';
import {Row, Col, Button, ButtonGroup, Checkbox, Glyphicon} from 'react-bootstrap';
import DeleteConfirmation from '../confirmation/DeleteConfirmation';

export default React.createClass({
  render() {
    let {metaperson, selected, loading} = this.props;

    return (
      <Row>
        <Col md={1}>
          <Checkbox disabled={loading} style={{marginTop: 0}} checked={selected} onChange={(e) => this.props.onSelectChange(metaperson, e)} />
        </Col>
        <Col md={3}>{metaperson.first_name}</Col>
        <Col md={3}>{metaperson.last_name}</Col>
        <Col md={2}>{metaperson.nick_name}</Col>
        <Col md={3}>
          <ButtonGroup className="pull-right">
            <DeleteConfirmation
              body={<span>Are you sure you want to remove <b>{metaperson.first_name} {metaperson.last_name}</b> from your contacts?</span>}
              onConfirm={() => this.props.deleteMetaperson(metaperson)}
            >
              <Button disabled={loading} bsSize="small">{metaperson.social_source_id ? <Glyphicon title="This metaperson is synced with social network" glyph="cloud" /> : <Glyphicon glyph="none" /> } Delete</Button>
            </DeleteConfirmation>

            <Button disabled={loading}
                    bsSize="small"
                    onClick={() => this.props.openMetapersonDetails(metaperson)}>Details</Button>
          </ButtonGroup>
        </Col>
      </Row>
    )
  }
});