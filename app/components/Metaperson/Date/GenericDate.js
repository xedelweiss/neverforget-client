import React from 'react';
import {Row, Col, FormGroup, FormControl, InputGroup, Button, Glyphicon} from 'react-bootstrap';
import Datetime from 'react-datetime';
import moment from 'moment';
import getListKeyById from '../../../utils/listKeyById';
import DeleteConfirmation from '../../confirmation/DeleteConfirmation';

const GenericDate = React.createClass({
  propTypes: {
    editable: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired,
  },
  getDefaultProps() {
    return {
      types: [],
      typePlaceholder: 'Date type',
      type: 'other',
      valuePlaceholder: 'Enter value',
      value: '',
      errors: []
    };
  },
  getTitleByType(id){
    let key = getListKeyById(this.props.types, null, id, false, 'type');
    if (key == -1) {
      return 'loading..';
    }
    return this.props.types[key].title;
  },
  render() {
    return (
      <Row>
        <Col md={12}>
          <FormGroup /* errors */>
            <InputGroup>
              {
                this.props.editable
                  ? <InputGroup.Button>
                      <FormControl onChange={(e) => this.props.onChange('type', e.target.value)} style={{
                        width: 'auto',
                        maxWidth: '120px',
                        borderRight: 'none',
                        borderTopLeftRadius: '4px',
                        borderBottomLeftRadius: '4px'
                      }} componentClass="select" placeholder={this.props.typePlaceholder} value={this.props.type}>
                        {this.props.types.map(({type, title}, index) => <option key={index} value={type}>{title}</option>)}
                      </FormControl>
                    </InputGroup.Button>
                  : <InputGroup.Addon>{this.getTitleByType(this.props.type)}</InputGroup.Addon>
              }
              {
                this.props.editable
                  ? <Datetime
                      inputProps={{placeholder: this.props.valuePlaceholder}}
                      value={this.props.value ? new Date(this.props.value) : ''}
                      dateFormat="YYYY-MM-DD"
                      timeFormat={false}
                      closeOnSelect={true}
                      onChange={(newDate) => this.props.onChange('value', newDate.format('YYYY-MM-DD HH:mm:ss'))}/>
                  : <FormControl type="text"
                      placeholder={this.props.valuePlaceholder}
                      value={this.props.value ? moment(this.props.value).format('YYYY-MM-DD') : ''}
                      onChange={() => {}}
                />
              }
              <InputGroup.Button>
                <DeleteConfirmation
                  body={<span>Are you sure you want to remove <b>{this.getTitleByType(this.props.type)} {this.props.value ? moment(this.props.value).format('YYYY-MM-DD') : ''}</b> from your dates?</span>}
                  onConfirm={(e) => this.props.onDelete()}
                >
                  <Button>
                    <Glyphicon glyph="remove" />
                  </Button>
                </DeleteConfirmation>
              </InputGroup.Button>

            </InputGroup>
              {this.props.errors.date ? <div className="text-danger">{this.props.errors.date[0]}</div> : null}
            </FormGroup>
        </Col>
      </Row>
    )
  }
});

export default GenericDate;