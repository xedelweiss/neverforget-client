import React from 'react';
import {Row, Col, Panel} from "react-bootstrap";
import MetapersonsListItem from "./MetapersonsListItem";

export default React.createClass({
  render() {
    let metapersons = this.props.metapersons.sort(function (a, b) {
      let first = a.metaperson;
      let second = b.metaperson;

      let firstInfo  = [first.first_name, first.last_name, first.nick_name];
      let secondInfo = [second.first_name, second.last_name, second.nick_name];

      for (let i in firstInfo) {
        if (firstInfo[i].toLowerCase() == secondInfo[i].toLowerCase()) {
          continue;
        }

        return firstInfo[i].toLowerCase() > secondInfo[i].toLowerCase() ? 1 : -1;
      }
    });

    return (
      <div>
        <Panel header={<Row>
          <Col md={1}></Col>
          <Col md={3}><small>First Name</small></Col>
          <Col md={3}><small>Last Name</small></Col>
          <Col md={2}><small>Nickname</small></Col>
          <Col md={2} mdOffset={1}><small>Manage</small></Col>
        </Row>}>
          {metapersons.map(({metaperson, selected, loading}) => {
            return (
              <MetapersonsListItem
                key={metaperson.id}
                metaperson={metaperson}
                selected={selected}
                loading={loading}
                deleteMetaperson={this.props.deleteMetaperson}
                openMetapersonDetails={this.props.openMetapersonDetails}
                onSelectChange={this.props.onMetapersonSelectChange}
              />
            )
          })}
        </Panel>
      </div>
    )
  }
});