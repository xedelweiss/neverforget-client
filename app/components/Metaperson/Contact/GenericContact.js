import React from 'react';
import {Row, Col, FormGroup, FormControl, InputGroup, Button, Glyphicon} from 'react-bootstrap';
import getListKeyById from '../../../utils/listKeyById';
import DeleteConfirmation from '../../confirmation/DeleteConfirmation';

export default React.createClass({
  getDefaultProps() {
    return {
      types: [
        {
          type: 'default',
          title: 'Default'
        },
        {
          type: 'other',
          title: 'Other'
        },
      ],
      typePlaceholder: 'Type',
      type: 'default',
      valuePlaceholder: 'Enter value',
      value: '',
      errors: []
    };
  },
  getTitleByType(id){
    let key = getListKeyById(this.props.types, null, id, false, 'type');
    if (key == -1) {
      return 'loading..';
    }
    return this.props.types[key].title;
  },
  render() {
    // const {editable} = this.props;
    return (
      <Row>
        <Col md={12}>
          <FormGroup /* errors */>
            <InputGroup>
              {
                this.props.editable
                ? <InputGroup.Button>
                    <FormControl onChange={(e) => this.props.onChange('type', e.target.value)} style={{width: 'auto', borderRight: 'none', borderTopLeftRadius: '4px', borderBottomLeftRadius: '4px'}} componentClass="select" placeholder={this.props.typePlaceholder} value={this.props.type}>
                      {this.props.types.map(({type, title}, index) => <option key={index} value={type}>{title}</option>)}
                    </FormControl>
                  </InputGroup.Button>
                : <InputGroup.Addon>{this.getTitleByType(this.props.type)}</InputGroup.Addon>
              }
              {
                this.props.editable
                  ? <FormControl type="text"
                           placeholder={this.props.valuePlaceholder}
                           value={this.props.value}
                           onChange={(e) => this.props.onChange('value', e.target.value)}
                    />
                  : <FormControl type="text"
                                 placeholder={this.props.valuePlaceholder}
                                 value={this.props.value}
                                 onChange={() => {}}
                    />
              }
              <InputGroup.Button>
                <DeleteConfirmation
                  body={<span>Are you sure you want to remove <b>{this.getTitleByType(this.props.type)} {this.props.value}</b> from your contacts?</span>}
                  onConfirm={(e) => this.props.onDelete()}
                >
                  <Button>
                    <Glyphicon glyph="remove" />
                  </Button>
                </DeleteConfirmation>
              </InputGroup.Button>

            </InputGroup>
            {this.props.errors.value ? <div className="text-danger">{this.props.errors.value[0]}</div> : null}
          </FormGroup>
        </Col>
      </Row>
    )
  }
});