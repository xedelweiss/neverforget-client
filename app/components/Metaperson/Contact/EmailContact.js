import React from 'react';
import GenericContact from './GenericContact';

export default React.createClass({
  getDefaultProps() {
    return {
      types: [
        {
          type: 'email',
          title: 'Email'
        },
      ],
    };
  },
  render() {
    return (
      <GenericContact
        typePlaceholder={'Email type'}
        valuePlaceholder={'Email'}
        {...this.props}
      />
    )
  }
});