import React from 'react';
import {Row, Col, FormGroup, FormControl} from 'react-bootstrap';
import GenericContact from './GenericContact';

export default React.createClass({
  getDefaultProps() {
    return {
      types: [
        {
          type: 'phone',
          title: 'Phone'
        },
      ],
    };
  },
  render() {
    return (
      <GenericContact
        typePlaceholder={'Phone type'}
        valuePlaceholder={'Phone number'}
        {...this.props}
      />
    )
  }
});