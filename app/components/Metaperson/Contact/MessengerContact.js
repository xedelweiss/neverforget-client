import React from 'react';
import GenericContact from './GenericContact';

export default React.createClass({
  getDefaultProps() {
    return {
      types: [
        {
          type: 'vkontakte',
          title: 'VKontakte'
        },
        {
          type: 'facebook',
          title: 'Facebook'
        },
        {
          type: 'skype',
          title: 'Skype'
        },
        {
          type: 'viber',
          title: 'Viber'
        },
        {
          type: 'telegram',
          title: 'Telegram'
        },
        {
          type: 'whatsapp',
          title: 'WhatsApp'
        }
      ]
    };
  },
  render() {
    return (
      <GenericContact
        typePlaceholder={'Messenger type'}
        valuePlaceholder={'Messenger ID'}
        {...this.props}
      />
    )
  }
});