import React from 'react';
import {Row, Col} from 'react-bootstrap';
import EditableFormGroup from '../../components/form/EditableFormGroup';
import EditableGroupsFormGroup from '../../containers/form/EditableGroupsFormGroup';

export default React.createClass({
  propTypes: {
    metaperson: React.PropTypes.object.isRequired,
    onFieldChange: React.PropTypes.func
  },
  render() {
    let {metaperson, onFieldChange, editable, errors} = this.props;

    return (
      <Row>
        <Col md={3}>
          {/*<img src="http://dummyimage.com/300x300/000/fff.png" className="img-responsive"/>*/}
        </Col>
        <Col md={9}>
          <form>
            <Row>
              <Col md={4}>
                <EditableFormGroup
                  type="text"
                  label="First Name"
                  placeholder="Enter first name"
                  value={metaperson.first_name}
                  errors={errors.first_name || []}
                  onChange={(e) => onFieldChange('first_name', e)}
                  editable={editable}
                />
              </Col>
              <Col md={4}>
                <EditableFormGroup
                  type="text"
                  label="Last Name"
                  placeholder="Enter last name"
                  value={metaperson.last_name}
                  errors={errors.last_name || []}
                  onChange={(e) => onFieldChange('last_name', e)}
                  editable={editable}
                />
              </Col>
              <Col md={4}>
                <EditableFormGroup
                  type="text"
                  label="Nick Name"
                  placeholder="Enter nick name"
                  value={metaperson.nick_name}
                  errors={errors.nick_name || []}
                  onChange={(e) => onFieldChange('nick_name', e)}
                  editable={editable}
                />
              </Col>
            </Row>
            <Row>
              <Col md={4}>
                <EditableFormGroup
                  type="text"
                  label="Job Title"
                  placeholder="Enter job title"
                  value={metaperson.job_title}
                  errors={errors.job_title || []}
                  onChange={(e) => onFieldChange('job_title', e)}
                  editable={editable}
                />
              </Col>
              <Col md={4}>
                <EditableFormGroup
                  type="text"
                  label="Company"
                  placeholder="Company"
                  value={metaperson.company}
                  errors={errors.company || []}
                  onChange={(e) => onFieldChange('company', e)}
                  editable={editable}
                />
              </Col>
              <Col md={4}>
                <EditableGroupsFormGroup
                  type="text"
                  label="Group"
                  placeholder="Group"
                  value={metaperson.groups ? metaperson.groups.map(group => group.title) : []}
                  onChange={(e) => onFieldChange('groups', e)}
                  editable={editable}
                />
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <EditableFormGroup
                  componentClass="textarea"
                  rows={4}
                  label="Notes"
                  placeholder="Enter notes"
                  value={metaperson.notes}
                  errors={errors.notes || []}
                  editable={editable}
                  onChange={(e) => onFieldChange('notes', e)}
                />
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    )
  }
});