import React from 'react';
import {DropdownButton, Glyphicon, MenuItem} from "react-bootstrap";

const SocialsDropdownButton = React.createClass({
  getName(social) {
    let {first_name, last_name, email, id} = social.info;

    let result = email ? email : social.social_id;

    if (first_name || last_name) {
      result = [first_name, last_name].filter(item => !!item).join(' ');
    }

    return result;
  },

  render() {
    let {socials, isLoading, onDelete, onSync, ...childProps} = this.props;

    return (<DropdownButton bsSize="small" title={<span><Glyphicon glyph="transfer"/> Import / Export</span>} id="bg-nested-dropdown" {...childProps}>
      {socials.map(({data, isLoading}, key) => <MenuItem key={key} eventKey={key} onClick={() => this.props.onSync(data)}>{this.getName(data)} <sub>({data.social_type})</sub></MenuItem>)}
    </DropdownButton>)
  }
});

export default SocialsDropdownButton;