import React from 'react';
import {Row, Col, Panel, Button} from "react-bootstrap";
import DeleteConfirmation from '../confirmation/DeleteConfirmation';
import LoadingGlyphicon from '../LoadingGlyphicon';

const SocialsSettingsList = React.createClass({
  getName(social) {
    let firstName = social.info && social.info.first_name ? social.info.first_name : null;
    let lastName = social.info && social.info.last_name ? social.info.last_name : null;
    let email = social.info && social.info.email ? social.info.email : null;

    let result = email ? email : social.social_id;

    if (firstName || lastName) {
      result = [firstName, lastName].filter(item => !!item).join(' ');
    }

    return result;
  },
  render() {
    let {socials, isLoading, ...childProps} = this.props;

    return <div>
        <Panel header={<Row>
          <Col md={3}><small>Provider</small></Col>
          <Col md={4}><small>Profile</small></Col>
          <Col md={1} mdOffset={1}><small>Manage</small></Col>
        </Row>}>
          { isLoading
            ? <h4 className="text-center"><LoadingGlyphicon loading={true}/></h4>
            : socials.map(({data, isLoading}, index) => {
                return (
                  <Row key={index}>
                    <Col md={3}>
                      {data.social_type}
                    </Col>
                    <Col md={4}>
                      <a href={'#' + data.social_id}>{this.getName(data)}</a>
                    </Col>
                    <Col md={1} mdOffset={1}>
                      <DeleteConfirmation
                        body={<span>Are you sure you want to remove <b>{this.getName(data)} <sub>({data.social_type})</sub></b> from your connections?</span>}
                        onConfirm={() => this.props.onDelete(data)}
                      >
                        <Button disabled={isLoading} bsSize="small">Delete</Button>
                      </DeleteConfirmation>

                    </Col>
                  </Row>
                )
              })
          }
        </Panel>
      </div>
  }
});

export default SocialsSettingsList;