import React, {PropTypes} from 'react';
import update from "react-addons-update";
import Time from 'react-time';
import {Row, Col, Button, ButtonGroup} from 'react-bootstrap';
import EventModal from './EventModal';
import DeleteConfirmation from '../confirmation/DeleteConfirmation';
import {defineMessages, FormattedMessage, FormattedHTMLMessage, injectIntl} from 'react-intl';

/**
 * Messages for i18n and L10n (to be collected by babel-react-intl plugin they are defined here)
 */
const messages = defineMessages({
  suspended: {
    id: 'suspended',
    defaultMessage: 'suspended'
  },
  Suspend: {
    id: 'Suspend',
    defaultMessage: 'Suspend'
  },
  Activate: {
    id: 'Activate',
    defaultMessage: 'Activate'
  },
  sureDelete: {
    id: 'sureDelete',
    defaultMessage: 'Are you sure you want to remove <b>{event_name}</b> from your events?'
  }
});

const eventItem = React.createClass({
  propTypes: {
    onToggleStatusClick: PropTypes.func.isRequired,
    event: PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      showModal: false,
      errors: []
    };
  },

  componentWillReceiveProps(nextProps) {
    if ( nextProps.showModal !== undefined ) {
      this.setState(update(this.state, {
        showModal:  {$set: nextProps.showModal},
        errors:     {$set: nextProps.errors}
      }));
    }
  },

  closeModal(errors) {
    if (errors.length === 0) {
      this.setState({
        showModal: false
      });
    }

    return false;
  },

  openModal() {
    this.setState({
      showModal: true
    });
  },

  saveEvent(event) {
    this.props.saveEvent(event);
  },

  render: function () {
    let {event, loading, onToggleStatusClick, timezones} = this.props;

    const intl  = this.props.intl;
    const suspended = intl.formatMessage(messages.suspended);
    const Suspend   = intl.formatMessage(messages.Suspend);
    const Activate  = intl.formatMessage(messages.Activate);

    return (
      <Row className="event_list_container">
        <Col md={2}>{ event.event_name }</Col>

        <Col md={3}>{ event.recipient }</Col>

        <Col md={2}>
          <Time value={event.datetime_to_send} format="YYYY-MM-DD"/>
          {!event.is_active ? ` (${suspended})` : ''}
        </Col>

        <Col md={2}>{event.timezone}</Col>

        <Col md={3} className="event-manage-buttons">

          <ButtonGroup className="pull-right">
            <Button disabled={loading}
                    bsStyle="link"
                    onClick={onToggleStatusClick}>{event.is_active ? Suspend : Activate}</Button>


            <DeleteConfirmation
              body={<span><FormattedHTMLMessage id="sureDelete" values={{event_name: event.event_name}} /></span>}
              onConfirm={() => {this.props.deleteEvent(event)}}
            >
              <Button disabled={loading} bsStyle="link"><FormattedMessage id="Delete" defaultMessage="Delete" /></Button>
            </DeleteConfirmation>

            <Button disabled={loading}
                    bsStyle="link"
                    onClick={this.openModal}><FormattedMessage id="Edit" defaultMessage="Edit" /></Button>
          </ButtonGroup>
        </Col>

        <EventModal event={event}
                    timezones={timezones}
                    onHide={this.closeModal}
                    onSave={this.saveEvent}
                    errors={this.state.errors}
                    userContacts={this.props.userContacts}
                    isShown={this.state.showModal}
        />
      </Row>
    )
  }
});

export default injectIntl(eventItem);