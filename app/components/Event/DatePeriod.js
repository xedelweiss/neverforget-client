import React from "react";
import moment from "moment";

export default React.createClass({

    renderPeriodDate(period, activeDate) {
        let date = moment({year: activeDate.year, month: activeDate.month-1, day: activeDate.day});

        switch (period) {
            case    "day":
                return this.renderOneDayPeriod(date);
                break;
            case    "week":
                return this.renderOneWeekPeriod(date);
                break;
            case    "month":
                return this.renderOneMonthPeriod(date);
                break;
            case    "year":
                return this.renderOneYearPeriod(date);
                break;
            default:
                break;
        }
    },

    renderOneDayPeriod(date) {
        return (
            <div>{date.format("dddd D, MMMM, YYYY")}</div>
        );
    },

    renderOneWeekPeriod(date) {
        let beginOfWeek = moment(date).startOf('week');
        let endOfWeek = moment(date).endOf('week');

        return (
            <div>{beginOfWeek.format("D, MMMM")} - {endOfWeek.format("D, MMMM, YYYY")}</div>
        );
    },

    renderOneMonthPeriod(date) {
        return (
            <div>{date.format("MMMM, YYYY")}</div>
        );
    },

    renderOneYearPeriod(date) {
        return <div>Year: {date.format("YYYY")}</div>
    },

    render() {
        return (
            <div>{this.renderPeriodDate(this.props.period, this.props.activeDate)}</div>
        );
    }
});