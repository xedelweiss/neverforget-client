import React from 'react';
import {log} from '../../utils/debug';
import update from 'react-addons-update';
import Select from 'react-select';
import {
  Button, FormGroup, ControlLabel,
  Checkbox, Modal, FormControl,
  Panel
} from 'react-bootstrap';
import Datetime from 'react-datetime';
import ContactsList from './ContactsList';
import "react-datetime/css/react-datetime.css";
import "react-select/less/default.less";
import {FormattedMessage, injectIntl} from 'react-intl';
import "./styles/styles.scss";

let eventModal = React.createClass({
  getInitialState() {
    return {
      event:    this.props.event,
      isShown:  false,
      errors:   [],
      recipientFilter: ''
    }

  },

  componentWillReceiveProps(nextProps) {
    let errors = [];
    if ( nextProps.errors !== undefined ) {
      errors = nextProps.errors;
    }

    this.setState({
      event:    nextProps.event,
      isShown:  nextProps.isShown,
      errors:   errors
    });
  },

  onDateTimeChange(newDate) {
    this._updateFieldState('datetime_to_send', newDate.format('YYYY-MM-DD HH:mm:ss'));
  },

  onFieldChange(field, event) {
    this._updateFieldState(field, event.target.value);
  },

  onCheckboxFieldChange(field, event) {
    this._updateFieldState(field, event.target.checked);
  },

  onTimeZoneSelect(val) {
    this.setState(update(this.state, {
      event: {
        timezone: {$set: val.value}
      }
    }));
  },

  onRecipientHelperClicked(val) {
    this.setState(update(this.state, {
      event: {
        recipient: {$set: val}
      },
      recipientFilter: {$set: ''}
    }));
  },

  onClose() {
    this.props.onHide(this.state.errors);

    this.setState(update(this.state, {
      recipientFilter: {$set: ''}
    }));
  },

  onSave() {
    this.props.onSave(this.state.event);
  },

  _updateFieldState(field, value) {
    this.setState(update(this.state, {
      event: {
        [field]: {
          $set: value
        }
      }
    }), () => {
      if ( field === 'recipient' ) {
        this.setState(update(this.state, {
          recipientFilter: {$set: value}
        }));
      }
    });
  },

  _hasFieldError(fieldName, errors) {
    return !!errors[fieldName];
  },

  render() {
    let event       = this.state.event;
    let isShown     = this.state.isShown;
    let onSave      = this.onSave;
    let errs        = this.state.errors[0];
    let hasErrors   = Object.keys(this.state.errors).length > 0;

    let timezones   = this.props.timezones.map((val) => {
      return {
        'value':  val,
        'label':  val
      };
    });

    return (
      <Modal show={isShown} onHide={this.onClose}>
        <Modal.Header>
          <Modal.Title>{event.event_name}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <form>
            <FormGroup controlId="event_name"
                       className={hasErrors ? (this._hasFieldError('event_name', errs) ? 'has-error' : '') : '' }>
              <ControlLabel>Event Title</ControlLabel>
              <FormControl type="text" 
                           placeholder="Enter event title" 
                           value={event.event_name}
                           onChange={this.onFieldChange.bind(this, 'event_name')}/>
              <div className="text-danger">
                {hasErrors ? (this._hasFieldError('event_name', errs) ? errs.event_name[0] : '') : ''}
              </div>
            </FormGroup>

            <FormGroup controlId="recipient"
                       className={hasErrors ? (this._hasFieldError('recipient', errs) ? 'has-error' : '') : ''}>
              <ControlLabel>Recipient's Email address</ControlLabel>
              <FormControl type="text" placeholder="Enter recipient's address"
                           value={event.recipient}
                           onChange={this.onFieldChange.bind(this, 'recipient')}
              />

              <ContactsList
                userContacts={this.props.userContacts}
                onClick={this.onRecipientHelperClicked}
                filterValue={this.state.recipientFilter} />

              <div className="text-danger">
                {hasErrors ? (this._hasFieldError('recipient', errs) ? errs.recipient[0] : '') : ''}
              </div>
            </FormGroup>

            <FormGroup controlId="message_subject"
                       className={hasErrors ? (this._hasFieldError('message_subject', errs) ? 'has-error' : '') : ''}>
              <ControlLabel>Message subject</ControlLabel>
              <FormControl type="text" placeholder="Enter text" value={event.message_subject}
                           onChange={this.onFieldChange.bind(this, 'message_subject')}/>
              <div className="text-danger">
                {hasErrors ? (this._hasFieldError('message_subject', errs) ? errs.message_subject[0] : '') : ''}
              </div>
            </FormGroup>

            <FormGroup controlId="message_body"
                       className={hasErrors ? (this._hasFieldError('message_body', errs) ? 'has-error' : '') : ''}>
              <ControlLabel>Message body</ControlLabel>
              <FormControl componentClass="textarea" placeholder="textarea"
                           value={event.message_body}
                           onChange={this.onFieldChange.bind(this, 'message_body')}/>
              <div className="text-danger">
                {hasErrors ? (this._hasFieldError('message_body', errs) ? errs.message_body[0] : '') : ''}
              </div>
            </FormGroup>

            <FormGroup controlId="message_signature"
                       className={hasErrors ? (this._hasFieldError('message_signature', errs) ? 'has-error' : '') : ''}>
              <ControlLabel>Message signature</ControlLabel>
              <FormControl type="text" placeholder="Enter text" value={event.message_signature}
                           onChange={this.onFieldChange.bind(this, 'message_signature')}/>
              <div className="text-danger">
                {hasErrors ? (this._hasFieldError('message_signature', errs) ? errs.message_signature[0] : '') : ''}
              </div>
            </FormGroup>

            <FormGroup controlId="datetime_to_send_">
              <ControlLabel>Date to send</ControlLabel>
              <div>
                <Datetime
                  value={new Date(event.datetime_to_send)}
                  dateFormat="YYYY-MM-DD"
                  timeFormat="HH:mm:ss"
                  closeOnSelect={true}
                  onChange={this.onDateTimeChange} />
              </div>
            </FormGroup>

            <Select
              name="timezone"
              clearable={false}
              value={{value: event.timezone, label: event.timezone}}
              options={timezones}
              onChange={this.onTimeZoneSelect}
            />

            <Checkbox checked={event.is_active}
                      onChange={this.onCheckboxFieldChange.bind(this, 'is_active')}>
              Is Active
            </Checkbox>

            <FormGroup controlId="comment">
              <ControlLabel>Comment</ControlLabel>
              <FormControl componentClass="textarea" placeholder="textarea" value={event.comment}
                           onChange={this.onFieldChange.bind(this, 'comment')}/>
            </FormGroup>
          </form>
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.onClose}>Close</Button>
          <Button bsStyle="primary" onClick={onSave}>Save changes</Button>
        </Modal.Footer>
      </Modal>
    )
  }
});

export default injectIntl(eventModal);