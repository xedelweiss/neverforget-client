import React from "react";
import {Row, Col} from "react-bootstrap";
import EventItem from './EventItem';

export default React.createClass({
  render() {
    return (
      <div>
        <Row>
          <Col md={2} className="event_table_header"><b>Event Name</b></Col>
          <Col md={3} className="event_table_header"><b>Event Recipient</b></Col>
          <Col md={2} className="event_table_header"><b>Event Date</b></Col>
          <Col md={2} className="event_table_header"><b>Timezone</b></Col>
          <Col md={3} className="event_table_header">
            <b className="pull-right event-manage-header">Manage Event</b>
          </Col>
        </Row>
        {this.props.events.map(({errors, event, loading, showModal}) => {
          return (
              <EventItem
                key={event.id}
                event={event}
                loading={loading}
                showModal={showModal}
                errors={errors}
                timezones={this.props.timezones}
                saveEvent={this.props.saveEvent}
                deleteEvent={this.props.deleteEvent}
                userContacts={this.props.userContacts}
                onToggleStatusClick={this.props.changeEventStatus.bind(this, event)}
              />
          );
          }

        )}
      </div>
    );
  }
});