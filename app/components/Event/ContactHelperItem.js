import {log} from '../../utils/debug';
import React from 'react';
import update from 'react-addons-update';
import {injectIntl} from 'react-intl';
import "./styles/styles.scss";

export default React.createClass({

  render() {
    return (
      <li className="event-recipient-email-helper"
          onClick={() => { this.props.onClick(this.props.contact.value);}}>{this.props.contact.value}</li>
    );

  }
});