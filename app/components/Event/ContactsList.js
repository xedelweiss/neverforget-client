import {log} from '../../utils/debug';
import React from 'react';
import update from 'react-addons-update';
import {injectIntl} from 'react-intl';
import "./styles/styles.scss";
import ContactHelperItem from "./ContactHelperItem";

const ContactsList = React.createClass({
  getInitialState() {
    return {
      filterValue:      '',
      filteredContacts: []
    };
  },

  componentWillReceiveProps(nextProps) {
    const filter = nextProps.filterValue;
    let filtered = [];

    if ( filter !== '' ) {
      filtered = nextProps.userContacts.reduce(function (acc, val) {

        if ( val.contact_type === 'email' && val.value.search(filter) !== -1) {
          if ( Array.isArray(acc) ) {
            acc.push(val);
            return acc;
          } else {
            return [val];
          }
        }

        return Array.isArray(acc) ? acc : [];
      });
    }

    this.setState({
      filterValue:  filter,
      filteredContacts: filtered
    });
  },

  render() {
    let emailHelperCounter = 0;

    return (
      <div className="event-dropdown-background">
        <ul className="event-dropdown-list-container">
          {
            this.state.filteredContacts.map((contact) => {
              if (contact.contact_type === 'email' ) {
                emailHelperCounter++;
                if ( emailHelperCounter===6 ) { return null; }

                return (
                  <ContactHelperItem
                    key={contact.id}
                    onClick={this.props.onClick}
                    contact={contact}/>
                );
              }
            })
          }
        </ul>
      </div>
    );
  }
});

export default injectIntl(ContactsList);