import React from 'react';
import {HelpBlock} from 'react-bootstrap';

export default React.createClass({
  render() {
    let errors = this.props.errors;

    if (errors.length == 0) {
      return null;
    }

    return (
      <HelpBlock>
        <strong>{errors.map((error, index) => <div key={index}>{error}</div>)}</strong>
      </HelpBlock>
    )
  }
});