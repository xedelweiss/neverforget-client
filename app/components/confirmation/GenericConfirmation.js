import React from 'react';
import Confirm from 'react-confirm-bootstrap';

export default React.createClass({
  getDefaultProps(){
    return {
      title: 'Attention required',
      body: 'Are you sure?',
      btnTitle: 'Yes'
    }
  },
  render() {
    return (
      <Confirm
        onConfirm={this.props.onConfirm}
        title={this.props.title}
        body={this.props.body}
        confirmText={this.props.btnTitle}
      >
        {this.props.children}
      </Confirm>
    )
  }
});