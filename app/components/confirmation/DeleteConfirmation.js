import React from 'react';
import GenericConfirmation from './GenericConfirmation';

export default React.createClass({
  getDefaultProps(){
    return {
      title: 'Deleting..',
      body: 'Are you sure you want to delete this?',
      btnTitle: 'Confirm Delete'
    }
  },
  render() {
    return (
      <GenericConfirmation {...this.props}>
        {this.props.children}
      </GenericConfirmation>
    )
  }
});