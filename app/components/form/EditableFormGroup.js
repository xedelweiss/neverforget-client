import React from 'react';
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap';

export default React.createClass({
  propTypes() {
    return {
      editable: React.PropTypes.bool,
      label: React.PropTypes.string,
      type: React.PropTypes.string,
      placeholder: React.PropTypes.string,
      value: React.PropTypes.string,
      errors: React.PropTypes.object,
      onChange: React.PropTypes.func,

      componentClass: React.PropTypes.string,
      rows: React.PropTypes.number,
    }
  },
  getDefaultProps() {
    return {
      editable: true,
      type: 'text',
      errors: [],
    }
  },
  render() {
    let formControlProps = {};
    Object.keys(this.props).forEach(key => {
      if (['editable', 'label', 'errors'].indexOf(key) == -1) {
        formControlProps[key] = this.props[key];
      }
    })

    return (
      <FormGroup>
        <ControlLabel>{this.props.label}</ControlLabel>
        {
          this.props.editable
            ? <FormControl {...formControlProps} />
            : <FormControl.Static style={{whiteSpace: "pre"}}>{this.props.value}</FormControl.Static>
        }
        {
          this.props.errors.length < 1 ? null : <div className="text-danger">{this.props.errors.join(' ')}</div>
        }
      </FormGroup>
    )
  }
});