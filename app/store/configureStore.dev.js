import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';

export default function configureStore(initialState) {
  const finalCreateStore = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension({
      // serialize very long structures to json
      serializeState: (key, value) => {
        switch (key) {
          case 'timeZones':
          case 'dateTypes':
            return JSON.stringify(value).replace(/"/g, "'");
          default:
            return value;
        }
      }
    }) : f => f
  )(createStore);

  const store = finalCreateStore(reducer, initialState);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
};