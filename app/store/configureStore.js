if (__DEV__ || (location && location.hostname == 'localhost')) {
    module.exports = require('./configureStore.dev.js');
} else {
    module.exports = require('./configureStore.pro.js');
}